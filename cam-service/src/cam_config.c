/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <fcntl.h>
#include <unistd.h>

#include <cam_byte_order.h>
#include <cam_config.h>
#include <cam_log.h>
#include <cam_service.h>

#include <assert.h>
#include <errno.h>
#include <string.h>

#ifdef __ZEPHYR__
#	include <ff.h>
#	include <zephyr/fs/fs.h>
#	include <zephyr/posix/fcntl.h>
#endif

static const char *const event_mode_name[] = {
	[CAM_CFG_EVENT_MODE_ONDEMAND_FROM_ARRIVAL] = "ondemand-from-arrival",
	[CAM_CFG_EVENT_MODE_ONDEMAND] = "ondemand",
	[CAM_CFG_EVENT_MODE_PERIODIC] = "periodic",
};

static char cam_cfg_dir[PATH_MAX] = { 0 };

static pthread_rwlock_t cfg_lock;

static int check_header(struct cam_csd_header *header)
{
	int ret;

	assert(header != NULL);

	ret =
		memcmp(cam_csd_signature, header->signature, sizeof(cam_csd_signature));
	if (ret != 0) {
		cam_log(
			CAM_LOG_LEVEL_ERROR, "Incorrect signature in the configuration.\n");
		return CAM_SERVICE_APP_ERROR;
	}

	if ((header->version[0] != CAM_CSD_VERSION_MAJOR) ||
		(header->version[1] != CAM_CSD_VERSION_MINOR)) {
		cam_log(
			CAM_LOG_LEVEL_ERROR, "Unsupported version in the configuration.\n");
		return CAM_SERVICE_APP_ERROR;
	}

	return CAM_SERVICE_SUCCESS;
}

static int check_metadata(struct cam_csd_metadata *metadata, cam_uuid_t uuid)
{
	int ret;

	assert(metadata != NULL);

	ret = cam_uuid_compare(metadata->uuid, uuid);
	if (ret != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Incorrect uuid in the configuration.\n");
		return CAM_SERVICE_APP_ERROR;
	}

	return CAM_SERVICE_SUCCESS;
}

static void print_cfg(struct cam_cfg_stream *cfg)
{
	unsigned int i;
	enum cam_cfg_event_mode mode;

	assert(cfg != NULL);

	cam_log(CAM_LOG_LEVEL_DEBUG, "stream name: %s\n", cfg->name);
	cam_log(
		CAM_LOG_LEVEL_DEBUG,
		"init to start timeout: %u\n",
		cfg->timeout_init_to_start);
	cam_log(
		CAM_LOG_LEVEL_DEBUG,
		"start to event timeout: %u\n",
		cfg->timeout_start_to_event);

	for (i = 0; i < cfg->event_count; i++) {
		mode = cfg->event[i].mode;
		assert(mode < CAM_CFG_EVENT_MODE_COUNT);
		cam_log(CAM_LOG_LEVEL_DEBUG, "event %d\n", i);
		cam_log(CAM_LOG_LEVEL_DEBUG, "mode %s, ", event_mode_name[mode]);
		cam_log(CAM_LOG_LEVEL_DEBUG, "timeout %u", cfg->event[i].timeout);
		cam_log(CAM_LOG_LEVEL_DEBUG, "\n");
	}
}

static bool check_file_exist(char *file_name)
{
	int fd;

	assert(file_name != NULL);

	fd = open(file_name, O_RDONLY);
	if (fd >= 0) {
		close(fd);
		return true;
	}

	return false;
}

static int cam_cfg_fs_init(const char *path)
{
#ifdef __ZEPHYR__
	static FATFS fat_fs;
	static struct fs_mount_t cam_fs_mnt = {
		.type = FS_FATFS,
		.fs_data = &fat_fs,
	};
	struct fs_statvfs sbuf;
	int ret;

	cam_fs_mnt.mnt_point = path;

	ret = fs_mount(&cam_fs_mnt);
	if (ret < 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to mount filesystem %d.\n", ret);
		return CAM_SERVICE_ERROR;
	}

	ret = fs_statvfs(cam_fs_mnt.mnt_point, &sbuf);
	if (ret < 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to retrieve fs state %d.\n", ret);
		return CAM_SERVICE_ERROR;
	}

	cam_log(CAM_LOG_LEVEL_INFO, "%s is mounted:\n", cam_fs_mnt.mnt_point);
	cam_log(
		CAM_LOG_LEVEL_INFO,
		"transfer block size: %lu, unit size = %lu, ",
		sbuf.f_bsize,
		sbuf.f_frsize);
	cam_log(
		CAM_LOG_LEVEL_INFO,
		"unit count: %lu, free unit = %lu\n",
		sbuf.f_blocks,
		sbuf.f_bfree);
#else
	(void)path;
#endif /* __ZEPHYR__ */

	return CAM_SERVICE_SUCCESS;
}

int cam_cfg_init(const char *path)
{
	int ret;

	if (path != NULL) {
		ret = snprintf(cam_cfg_dir, PATH_MAX, "%s/", path);
		if (ret < 0) {
			cam_log(
				CAM_LOG_LEVEL_ERROR,
				"Failed to set configuration directory.\n");
			return CAM_SERVICE_ERROR;
		}
	}

	ret = pthread_rwlock_init(&cfg_lock, NULL);
	if (ret != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to initialize rwlock.\n");
		return CAM_SERVICE_ERROR;
	}

	if (cam_cfg_fs_init(path) != CAM_SERVICE_SUCCESS) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to initialize file system.\n");
		return CAM_SERVICE_ERROR;
	}

	return CAM_SERVICE_SUCCESS;
}

int cam_cfg_stream_load(cam_uuid_t stream_uuid, struct cam_cfg_stream *cfg)
{
	struct cam_csd csd;
	struct cam_csd_event_alarms csd_event;
	char file_name[PATH_MAX];
	char uuid_str[CAM_UUID_STR_LEN];
	int fd;
	ssize_t read_size;
	unsigned int i = 0;
	int ret = CAM_SERVICE_SUCCESS;

	if (cfg == NULL) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Stream configuration pointer is NULL.\n");
		return CAM_SERVICE_ERROR;
	}

	if (stream_uuid == NULL) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Stream UUID pointer is NULL.\n");
		return CAM_SERVICE_ERROR;
	}

	if (cam_uuid_to_string(stream_uuid, uuid_str) != CAM_UUID_SUCCESS) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to convert uuid.\n");
		return CAM_SERVICE_ERROR;
	}

	ret = snprintf(
		file_name,
		PATH_MAX,
		"%s%s%s",
		cam_cfg_dir,
		uuid_str,
		CAM_CFG_FILE_EXTENSION);

	if (ret < 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to format filename.\n");
		return CAM_SERVICE_ERROR;
	}

	if (pthread_rwlock_rdlock(&cfg_lock) != 0) {
		return CAM_SERVICE_ERROR;
	}

	fd = open(file_name, O_RDONLY);
	if (fd < 0) {
		cam_log(
			CAM_LOG_LEVEL_ERROR, "Failed to open csd file %s.\n", file_name);
		if (pthread_rwlock_unlock(&cfg_lock) != 0) {
			return CAM_SERVICE_ERROR;
		}
		return CAM_SERVICE_APP_ERROR;
	}

	read_size = read(fd, &csd, sizeof(csd));
	if (read_size < 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to read csd file.\n");
		ret = CAM_SERVICE_ERROR;
		goto exit;
	} else if (read_size != sizeof(csd)) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Incomplete csd file.\n");
		ret = CAM_SERVICE_APP_ERROR;
		goto exit;
	}

	ret = check_header(&csd.header);
	if (ret != CAM_SERVICE_SUCCESS) {
		goto exit;
	}

	ret = check_metadata(&csd.metadata, stream_uuid);
	if (ret != CAM_SERVICE_SUCCESS) {
		goto exit;
	}

	strncpy(cfg->name, (char *)csd.metadata.name, CAM_CSD_STREAM_NAME_LEN);
	if (cfg->name[CAM_CSD_STREAM_NAME_LEN - 1] != '\0') {
		cam_log(CAM_LOG_LEVEL_ERROR, "Invalid stream name.\n");
		ret = CAM_SERVICE_APP_ERROR;
		goto exit;
	}

	cfg->timeout_init_to_start = le32toh(csd.state_ctrl.timeout_init_to_start);
	cfg->timeout_start_to_event =
		le32toh(csd.state_ctrl.timeout_start_to_event);

	if (cfg->timeout_start_to_event == 0) {
		cam_log(
			CAM_LOG_LEVEL_ERROR, "Start to event timeout can not be zero.\n");
		ret = CAM_SERVICE_APP_ERROR;
		goto exit;
	}

	while (i < CONFIG_CAM_SERVICE_MAX_EVENT_PER_STREAM) {
		read_size = read(fd, &csd_event, sizeof(csd_event));
		if (read_size < 0) {
			cam_log(CAM_LOG_LEVEL_ERROR, "Failed to read csd file.\n");
			ret = CAM_SERVICE_ERROR;
			goto exit;
		} else if (read_size != sizeof(csd_event)) {
			break;
		}

		if (csd_event.event_id != i) {
			cam_log(CAM_LOG_LEVEL_ERROR, "Invalid event ID.\n");
			ret = CAM_SERVICE_APP_ERROR;
			goto exit;
		}

		if (csd_event.mode >= CAM_CFG_EVENT_MODE_COUNT) {
			cam_log(CAM_LOG_LEVEL_ERROR, "Invalid event mode.\n");
			ret = CAM_SERVICE_APP_ERROR;
			goto exit;
		}

		cfg->event[i].mode = csd_event.mode;
		cfg->event[i].timeout = le32toh(csd_event.timeout);
		i++;
	}

	if (i == 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Csd has no event entry.\n");
		ret = CAM_SERVICE_APP_ERROR;
		goto exit;
	}

	cfg->event_count = i;

	cam_log(
		CAM_LOG_LEVEL_INFO, "Stream %s configuration is loaded.\n", uuid_str);

	print_cfg(cfg);

exit:
	if (close(fd) != 0) {
		ret = CAM_SERVICE_ERROR;
	}

	if (pthread_rwlock_unlock(&cfg_lock) != 0) {
		return CAM_SERVICE_ERROR;
	}

	return ret;
}

int cam_cfg_stream_store_init(
	cam_uuid_t stream_uuid,
	bool overwrite,
	unsigned int file_size,
	struct cam_cfg_store_ctx *ctx)
{
	int fd;
	char uuid_str[CAM_UUID_STR_LEN];
	int ret = CAM_SERVICE_SUCCESS;
	unsigned int remain_size = file_size;
	mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP;

	if (stream_uuid == NULL || ctx == NULL) {
		return CAM_SERVICE_ERROR;
	}

	memset(ctx, 0, sizeof(struct cam_cfg_store_ctx));

	if (pthread_rwlock_wrlock(&cfg_lock) != 0) {
		return CAM_SERVICE_ERROR;
	}

	if (file_size == 0) {
		ctx->status = CAM_MSG_S2A_DEPLOY_STATUS_INVALID_FILE_SIZE;
		ret = CAM_SERVICE_APP_ERROR;
		goto exit;
	}

	if (cam_uuid_to_string(stream_uuid, uuid_str) != CAM_UUID_SUCCESS) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to convert uuid.\n");
		ret = CAM_SERVICE_ERROR;
		goto exit;
	}

	if (snprintf(
			ctx->file_name,
			PATH_MAX,
			"%s%s%s",
			cam_cfg_dir,
			uuid_str,
			CAM_CFG_FILE_EXTENSION) < 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to format config filename.\n");
		ret = CAM_SERVICE_ERROR;
		goto exit;
	}

	if (snprintf(
			ctx->tmp_file_name,
			PATH_MAX,
			"%s%s",
			ctx->file_name,
			CAM_CFG_TMP_FILE_EXTENSION) < 0) {
		cam_log(
			CAM_LOG_LEVEL_ERROR,
			"Failed to format temporary config filename.\n");
		ret = CAM_SERVICE_ERROR;
		goto exit;
	}

	if (!overwrite) {
		/* Check the csd file */
		if (check_file_exist(ctx->file_name)) {
			cam_log(
				CAM_LOG_LEVEL_ERROR, "The config of %s existed.\n", uuid_str);
			ctx->status = CAM_MSG_S2A_DEPLOY_STATUS_DEPLOY_FILE_EXIST;
			ret = CAM_SERVICE_APP_ERROR;
			goto exit;
		}
	}

	/* Check the temporary csd file */
	if (check_file_exist(ctx->tmp_file_name)) {
		cam_log(
			CAM_LOG_LEVEL_ERROR,
			"The temporary csd of %s existed.\n",
			uuid_str);
		ctx->status = CAM_MSG_S2A_DEPLOY_STATUS_EXCEED_MAX_DEPLOY_SESSION;
		ret = CAM_SERVICE_APP_ERROR;
		goto exit;
	}

	fd = open(ctx->tmp_file_name, O_WRONLY | O_CREAT, mode);
	if (fd < 0) {
		cam_log(
			CAM_LOG_LEVEL_ERROR,
			"Failed to create file %s.\n",
			ctx->tmp_file_name);
		ret = CAM_SERVICE_ERROR;
		goto exit;
	}

	while (remain_size != 0) {
		ssize_t write_size;
		char data = 0;

		write_size = write(fd, &data, 1);
		if (write_size == 1) {
			remain_size--;
			continue;
		} else if (write_size < 0 && errno != ENOSPC) {
			cam_log(
				CAM_LOG_LEVEL_ERROR,
				"Failed to write file %s.\n",
				ctx->tmp_file_name);
			ret = CAM_SERVICE_ERROR;
			goto exit;
		} else {
			cam_log(
				CAM_LOG_LEVEL_ERROR,
				"%s: Invalid file size %d.\n",
				ctx->tmp_file_name,
				file_size);
			ctx->status = CAM_MSG_S2A_DEPLOY_STATUS_INVALID_FILE_SIZE;
			ret = CAM_SERVICE_APP_ERROR;
			break;
		}
	}

	if (close(fd) != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to close file.\n");
		ret = CAM_SERVICE_ERROR;
	}

exit:
	if (pthread_rwlock_unlock(&cfg_lock) != 0) {
		return CAM_SERVICE_ERROR;
	}

	return ret;
}

int cam_cfg_stream_store_append(
	struct cam_cfg_store_ctx *ctx,
	void *data,
	unsigned int size)
{
	int fd;
	ssize_t write_size;
	int ret = CAM_SERVICE_SUCCESS;

	if (ctx == NULL || data == NULL) {
		return CAM_SERVICE_ERROR;
	}

	if (pthread_rwlock_wrlock(&cfg_lock) != 0) {
		return CAM_SERVICE_ERROR;
	}

	fd = open(ctx->tmp_file_name, O_WRONLY);
	if (fd < 0) {
		cam_log(
			CAM_LOG_LEVEL_ERROR,
			"Failed to open file %s.\n",
			ctx->tmp_file_name);
		ctx->status = CAM_MSG_S2A_DEPLOY_STATUS_FAILED_TO_WRITE;
		ret = CAM_SERVICE_APP_ERROR;
		goto exit;
	}

	if (lseek(fd, ctx->offset, SEEK_SET) == ctx->offset) {
		write_size = write(fd, data, size);
		if (write_size < 0 || write_size != size) {
			cam_log(
				CAM_LOG_LEVEL_ERROR,
				"Failed to write file %s.\n",
				ctx->tmp_file_name);
			ctx->status = CAM_MSG_S2A_DEPLOY_STATUS_FAILED_TO_WRITE;
			ret = CAM_SERVICE_APP_ERROR;
		}
	} else {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to reposition current offset.\n");
		ret = CAM_SERVICE_ERROR;
	}

#ifndef __ZEPHYR__
	/* fsync is not supported in Zephyr */
	if (fsync(fd) != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to flush file.\n");
		ret = CAM_SERVICE_ERROR;
	}
#endif

	if (close(fd) != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to close file.\n");
		ret = CAM_SERVICE_ERROR;
	}

	ctx->offset += size;

exit:
	if (pthread_rwlock_unlock(&cfg_lock) != 0) {
		return CAM_SERVICE_ERROR;
	}

	return ret;
}

int cam_cfg_stream_store_end(struct cam_cfg_store_ctx *ctx)
{
	int ret = CAM_SERVICE_SUCCESS;

	if (ctx == NULL) {
		return CAM_SERVICE_ERROR;
	}

	if (pthread_rwlock_wrlock(&cfg_lock) != 0) {
		return CAM_SERVICE_ERROR;
	}

	if (rename(ctx->tmp_file_name, ctx->file_name) != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to store csd file.\n");
		(void)unlink(ctx->tmp_file_name);
		ret = CAM_SERVICE_ERROR;
	}

	if (pthread_rwlock_unlock(&cfg_lock) != 0) {
		return CAM_SERVICE_ERROR;
	}

	return ret;
}

int cam_cfg_stream_store_fail(struct cam_cfg_store_ctx *ctx)
{
	int ret = CAM_SERVICE_SUCCESS;

	if (ctx == NULL) {
		return CAM_SERVICE_ERROR;
	}

	if (pthread_rwlock_wrlock(&cfg_lock) != 0) {
		return CAM_SERVICE_ERROR;
	}

	if (check_file_exist(ctx->tmp_file_name)) {
		if (unlink(ctx->tmp_file_name) != 0) {
			cam_log(CAM_LOG_LEVEL_ERROR, "Failed to remove csd file.\n");
			ret = CAM_SERVICE_ERROR;
		}
	}

	if (pthread_rwlock_unlock(&cfg_lock) != 0) {
		return CAM_SERVICE_ERROR;
	}

	return ret;
}
