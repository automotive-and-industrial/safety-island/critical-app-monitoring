/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#define _GNU_SOURCE

#include <netinet/in.h>
#ifndef __ZEPHYR__
#	include <netinet/ip.h>
#endif
#include <netinet/tcp.h>
#include <pthread.h>
#include <unistd.h>

#include <cam_fault.h>
#include <cam_log.h>
#include <cam_service.h>
#include <cam_socket.h>
#include <cam_stream.h>

#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>

#ifdef __ZEPHYR__
#	include <zephyr/kernel.h>

#	define STACK_SIZE (1024 * 16)

/* One dispatcher thread and CONFIG_CAM_SERVICE_MAX_CONN worker threads */
K_THREAD_STACK_ARRAY_DEFINE(stack, CONFIG_CAM_SERVICE_MAX_CONN + 1, STACK_SIZE);
#endif

struct cam_socket_task {
	void (*conn_process)(int fd);
	int task_status;
	int peer_fd;
	struct cam_socket_task *next;
	pthread_t thread_id;
	struct cam_socket_ctx *socket_ctx;
};

struct thread_pool {
	struct cam_socket_task task_list[CONFIG_CAM_SERVICE_MAX_CONN];
	pthread_mutex_t mutex_pool;
	pthread_cond_t task_coming;
	struct cam_socket_task free_list;
	struct cam_socket_task wait_list;
	int is_initialised;
};

static struct thread_pool pool;

static int cam_socket_add_conn_work(
	void (*func)(int fd),
	struct cam_socket_ctx *socket_ctx,
	int peer_fd)
{
	struct cam_socket_task *t;

	pthread_mutex_lock(&pool.mutex_pool);

	if (pool.free_list.next == NULL) {
		pthread_mutex_unlock(&pool.mutex_pool);
		cam_log(CAM_LOG_LEVEL_ERROR, "Socket thread pool is full.\n");
		return CAM_SERVICE_ERROR;
	}

	t = pool.free_list.next;
	pool.free_list.next = t->next;
	t->next = pool.wait_list.next;
	pool.wait_list.next = t;

	t->conn_process = func;
	t->socket_ctx = socket_ctx;
	t->peer_fd = peer_fd;

	pthread_cond_signal(&pool.task_coming);
	pthread_mutex_unlock(&pool.mutex_pool);

	return CAM_SERVICE_SUCCESS;
}

static void *cam_socket_conn_work(void *args)
{
	struct cam_socket_task *t;
	(void)args;

	while (1) {
		pthread_mutex_lock(&pool.mutex_pool);

		while (pool.wait_list.next == NULL) {
			pthread_cond_wait(&pool.task_coming, &pool.mutex_pool);
		}

		t = pool.wait_list.next;
		pool.wait_list.next = t->next;
		t->next = NULL;
		pthread_mutex_unlock(&pool.mutex_pool);

		cam_log(
			CAM_LOG_LEVEL_DEBUG,
			"Connection thread %ld start working.\n",
			t->thread_id);

		t->conn_process(t->peer_fd);
		close(t->peer_fd);

		cam_log(
			CAM_LOG_LEVEL_DEBUG,
			"Connection thread %ld stop working.\n",
			t->thread_id);

		pthread_mutex_lock(&pool.mutex_pool);
		t->next = pool.free_list.next;
		pool.free_list.next = t;
		pthread_mutex_unlock(&pool.mutex_pool);

		if (sem_post(&t->socket_ctx->conn_cnt_sem) != 0) {
			cam_log(CAM_LOG_LEVEL_ERROR, "Error posting semaphore");
			cam_fault_service(CAM_FAULT_SOCKET_CONN_THREAD_ERROR);
		}
	}

	return NULL;
}

static int thread_pool_init(void)
{
	pthread_attr_t attr;

	if (pool.is_initialised) {
		return CAM_SERVICE_SUCCESS;
	}

	if (pthread_mutex_init(&pool.mutex_pool, NULL) != 0 ||
		pthread_cond_init(&pool.task_coming, NULL) != 0) {
		cam_log(
			CAM_LOG_LEVEL_ERROR,
			"Failed to initialize thread pool mutex and condition.\n");
		return CAM_SERVICE_ERROR;
	}

	pool.wait_list.next = NULL;

	for (int i = 0; i < CONFIG_CAM_SERVICE_MAX_CONN; ++i) {
		int res = pthread_attr_init(&attr);
		if (res != 0) {
			cam_log(CAM_LOG_LEVEL_ERROR, "Failed to init pthread_attr.\n");
			return CAM_SERVICE_ERROR;
		}

#ifdef __ZEPHYR__
		/* Zephyr requires an explicit thread stack */
		res = pthread_attr_setstack(&attr, &stack[i + 1], STACK_SIZE);
		if (res != 0) {
			cam_log(
				CAM_LOG_LEVEL_ERROR, "Failed to set pthread_attr_setstack.\n");
			return CAM_SERVICE_ERROR;
		}
#endif

		res = pthread_create(
			&pool.task_list[i].thread_id, &attr, cam_socket_conn_work, NULL);
		if (res != 0) {
			cam_log(CAM_LOG_LEVEL_ERROR, "Failed to create thread pool.\n");
			return CAM_SERVICE_ERROR;
		}

		pool.task_list[i].next = pool.free_list.next;
		pool.free_list.next = &pool.task_list[i];
	}

	pool.is_initialised = 1;

	return CAM_SERVICE_SUCCESS;
}

/*
 * This call disables TCP Delayed Ack feature, it needs to be called after every
 * send and receive because the flag resets after these events.
 */
int disable_delayed_tcp_ack(int socket_fd)
{
	int ret = 0;

#ifndef __ZEPHYR__
	const int enable = 1;

	ret = setsockopt(
		socket_fd, IPPROTO_TCP, TCP_QUICKACK, &enable, sizeof(enable));

	if (ret != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Error setting TCP_QUICKACK (%d).\n", ret);
	}
#endif

	return ret;
}

static int disable_nagle_algorithm(int socket_fd)
{
	int ret;
	const int enable = 1;

	ret = setsockopt(
		socket_fd, IPPROTO_TCP, TCP_NODELAY, &enable, sizeof(enable));

	if (ret != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Error setting TCP_NODELAY (%d).\n", ret);
	}

	return ret;
}

static int set_socket_low_delay(int socket_fd)
{
	int ret = 0;

#ifndef __ZEPHYR__
	const int tos = IPTOS_LOWDELAY;

	ret = setsockopt(socket_fd, IPPROTO_IP, IP_TOS, &tos, sizeof(tos));

	if (ret != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Error setting IP_TOS (%d).\n", ret);
	}
#endif

	return ret;
}

static int socket_create(struct cam_socket_cfg *cfg)
{
	struct sockaddr_in addr;
	int socket_fd;
	const int enable = 1;

	assert(cfg != NULL);

	addr.sin_family = AF_INET;
	addr.sin_port = htons(cfg->port);

	if (inet_pton(AF_INET, cfg->addr, &addr.sin_addr) != 1) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Invalid IP address.\n");
		return -1;
	}

	socket_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (socket_fd == -1) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Error creating socket.\n");
		return socket_fd;
	}

	if (setsockopt(
			socket_fd, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(enable)) !=
		0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Error setting SO_REUSEADDR.\n");
		goto error;
	}

	if (set_socket_low_delay(socket_fd) != 0) {
		goto error;
	}

	if (disable_nagle_algorithm(socket_fd) != 0) {
		goto error;
	}

	if (disable_delayed_tcp_ack(socket_fd) != 0) {
		goto error;
	}

	if (bind(socket_fd, (struct sockaddr *)&addr, sizeof(addr)) != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Error binding socket.\n");
		goto error;
	}

	if (listen(socket_fd, CONFIG_CAM_SERVICE_MAX_CONN) != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Error listening to server socket.\n");
		goto error;
	}

	return socket_fd;

error:
	close(socket_fd);
	return -1;
}

static void *dispatch_thread(void *arg)
{
	struct cam_socket_ctx *ctx = arg;
	int peer_fd;
	struct sockaddr_in peer_addr = { 0 };
	socklen_t peer_addr_size = sizeof(peer_addr);
	int ret = CAM_SERVICE_SUCCESS;
	int sem_ret;

	assert(arg != NULL);

	if (sem_init(&ctx->conn_cnt_sem, 0, CONFIG_CAM_SERVICE_MAX_CONN) != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Error initializing a semaphore.\n");
		ret = CAM_SERVICE_ERROR;
		goto fault_report;
	}

	while (1) {
		/* Decrease connection count semaphore */
		if (sem_trywait(&ctx->conn_cnt_sem) != 0) {
			/* Close the socket when connection reaches the maximum count.
			 * The following client connection requests will be rejected
			 * immediately.
			 */
			if (errno == EAGAIN) {
				close(ctx->fd);
				ctx->fd = -1;
				/* Block the thread until a connection is closed */
				if (sem_wait(&ctx->conn_cnt_sem) != 0) {
					cam_log(
						CAM_LOG_LEVEL_ERROR, "Error waiting for semaphore.\n");
					ret = CAM_SERVICE_ERROR;
					goto exit;
				}
			} else {
				perror("Error waiting for semaphore");
				cam_log(CAM_LOG_LEVEL_ERROR, "Error waiting for semaphore.\n");
				ret = CAM_SERVICE_ERROR;
				goto exit;
			}
		}

		/* Create the socket if socket is closed or not created */
		if (ctx->fd == -1) {
			ctx->fd = socket_create(ctx->cfg);
			if (ctx->fd < 0) {
				cam_log(CAM_LOG_LEVEL_ERROR, "Failed to create socket.\n");
				ret = CAM_SERVICE_ERROR;
				goto exit;
			}
		}

		do {
			peer_fd =
				accept(ctx->fd, (struct sockaddr *)&peer_addr, &peer_addr_size);

			if (peer_fd == -1) {
				switch (errno) {
				case ECONNABORTED:
				case EINTR:
				case EMFILE:
				case ENFILE:
					cam_log(
						CAM_LOG_LEVEL_DEBUG,
						"accept() failed with recoverable code (%lu): %s\n",
						errno,
						strerror(errno));
					break;
				default:
					cam_log(
						CAM_LOG_LEVEL_ERROR, "Error accepting connection.\n");
					ret = CAM_SERVICE_ERROR;
					goto exit;
				}
			}
		} while (peer_fd == -1);

		if ((disable_nagle_algorithm(peer_fd) != 0) ||
			(disable_delayed_tcp_ack(peer_fd) != 0) ||
			(set_socket_low_delay(peer_fd) != 0)) {
			ret = CAM_SERVICE_ERROR;
			close(peer_fd);
			goto exit;
		}

		if (cam_socket_add_conn_work(cam_stream_process, ctx, peer_fd) != 0) {
			cam_log(CAM_LOG_LEVEL_ERROR, "Failed to create stream thread.\n");
			ret = CAM_SERVICE_ERROR;
			close(peer_fd);
			goto exit;
		}
	}

exit:
	if (ctx->fd != -1) {
		close(ctx->fd);
	}

	/*
	 * To destroy the semaphore, all counts must be taken, so call sem_trywait
	 * until we can take the semaphore (sem_ret == 0) or in case of errors, the
	 * error is something recoverable (EINTR).
	 * The loop exits only in case all counts are taken (EAGAIN) or a not
	 * recoverable error is found.
	 */
	do {
		sem_ret = sem_trywait(&ctx->conn_cnt_sem);
	} while ((sem_ret == 0) || ((sem_ret == -1) && (errno == EINTR)));

	/* Check that sem_trywait result is as expected */
	if ((sem_ret == -1) && (errno != EAGAIN)) {
		cam_log(
			CAM_LOG_LEVEL_ERROR,
			"Failed to reset semaphore before the destroy operation.\n");
		goto fault_report;
	}

	if (sem_destroy(&ctx->conn_cnt_sem) != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Error destroying a semaphore.\n");
	}

fault_report:
	if (ret == CAM_SERVICE_ERROR) {
		cam_fault_service(CAM_FAULT_SOCKET_DISPATCHER_THREAD_ERROR);
	}

	return NULL;
}

int cam_socket_init(struct cam_socket_ctx *ctx, struct cam_socket_cfg *cfg)
{
	pthread_attr_t attr;
	int res;

	assert(ctx != NULL && cfg != NULL);

	ctx->fd = -1;
	ctx->cfg = cfg;

	if (thread_pool_init() != CAM_SERVICE_SUCCESS) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to init thread pool.\n");
		return CAM_SERVICE_ERROR;
	}

	res = pthread_attr_init(&attr);
	if (res != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to init pthread_attr.\n");
		return CAM_SERVICE_ERROR;
	}

#ifdef __ZEPHYR__
	/* Zephyr requires an explicit thread stack */
	res = pthread_attr_setstack(&attr, &stack[0], STACK_SIZE);
	if (res != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to set pthread_attr_setstack.\n");
		return CAM_SERVICE_ERROR;
	}
#endif

	res = pthread_create(&ctx->tid, &attr, dispatch_thread, ctx);
	if (res != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to create dispatch thread.\n");
		return CAM_SERVICE_ERROR;
	}

	/*
	 * Do not exit if pthread_setname_np is not supported as this is just an
	 * optional feature.
	 */
	if (pthread_setname_np(ctx->tid, "Dispatcher") != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to set thread name.\n");
	}

	return CAM_SERVICE_SUCCESS;
}
