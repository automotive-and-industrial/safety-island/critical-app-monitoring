/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <dirent.h>
#include <getopt.h>

#include <cam_cmdline.h>

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const struct option options[] = {
	{ "address", required_argument, NULL, 'a' },
	{ "port", required_argument, NULL, 'p' },
	{ "log-filename", required_argument, NULL, 'f' },
	{ "log-level", required_argument, NULL, 'l' },
	{ "config-dir", required_argument, NULL, 'c' },
	{ "fault-action", required_argument, NULL, 'e' },
	{ "help", no_argument, NULL, 'h' },
	{ NULL, 0, NULL, 0 }
};

static void print_help(char *app_name)
{
	assert(app_name != NULL);

	printf("Usage: %s [OPTIONS]\n", app_name);
	printf("\n");

	printf(
		"The service part of the Critical Application Monitoring project.\n");
	printf("\n");

	printf("Options:\n");

	printf(
		"-a, --address <addr>                      "
		"IP address of service binding. Default: %s\n",
		CONFIG_CAM_SERVICE_IP);

	printf(
		"-p, --port <port>                         "
		"Port number that service listens to. Default: %d\n",
		CONFIG_CAM_SERVICE_PORT);

	printf(
		"-f, --log-filename <name>                 "
		"Log file name.\n");

	printf(
		"-l, --log-level <none|error|info|debug>   "
		"Log level. Default: info\n");

	printf(
		"-c, --config-dir <path>                   "
		"The directory path for the configuration files. Default: current "
		"directory\n");

	printf(
		"-e, --fault-action <none|exit>       "
		"The action when fault is caught. Default: none\n");

	printf(
		"-h, --help                                "
		"Show this help.\n");
}

int cam_cmdline_parse(int argc, char *argv[], struct cam_service_cfg *cfg)
{
	int opt;
	int port;
	unsigned int i;
	DIR *cfg_dir;

	if (cfg == NULL) {
		return CAM_SERVICE_ERROR;
	}

	while ((opt = getopt_long(argc, argv, "a:p:f:l:c:e:h", options, NULL)) !=
		   -1) {
		switch (opt) {
		case 'a':
			cfg->socket_cfg.addr = optarg;
			break;
		case 'p':
			port = atoi(optarg);
			if (port <= 0) {
				fprintf(stderr, "Port must be a positive number\n");
				return CAM_SERVICE_ERROR;
			}
			cfg->socket_cfg.port = port;
			break;
		case 'f':
			cfg->log_file_name = optarg;
			break;
		case 'l':
			for (i = 0; i < CAM_LOG_LEVEL_COUNT; i++) {
				if (strcmp(cam_log_level_name[i], optarg) == 0) {
					cfg->log_level = i;
					break;
				}
			}
			if (i >= CAM_LOG_LEVEL_COUNT) {
				fprintf(stderr, "Invalid log level\n");
				return CAM_SERVICE_ERROR;
			}
			break;
		case 'c':
			cfg_dir = opendir(optarg);
			if (cfg_dir) {
				cfg->cfg_dir_path = optarg;
				closedir(cfg_dir);
			} else {
				fprintf(stderr, "Configuration directory does not exist.\n");
				return CAM_SERVICE_ERROR;
			}
			break;
		case 'e':
			for (i = 0; i < CAM_FAULT_ACTION_COUNT; i++) {
				if (strcmp(cam_fault_action_name[i], optarg) == 0) {
					cfg->fault_action = i;
					break;
				}
			}
			if (i >= CAM_FAULT_ACTION_COUNT) {
				fprintf(stderr, "Invalid fault action\n");
				return CAM_SERVICE_ERROR;
			}
			break;
		case 'h':
			print_help(argv[0]);
			exit(EXIT_SUCCESS);
		default:
			return CAM_SERVICE_ERROR;
		}
	}

	return CAM_SERVICE_SUCCESS;
}
