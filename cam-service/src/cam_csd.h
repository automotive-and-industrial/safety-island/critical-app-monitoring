/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

/*
 * Cam Stream Deployment Data Structure
 */

#ifndef CAM_CSD_H
#define CAM_CSD_H

#include <cam_uuid.h>

#include <stdint.h>

#define CAM_CSD_VERSION_MAJOR 1
#define CAM_CSD_VERSION_MINOR 0

#define CAM_CSD_STREAM_NAME_LEN 64

static const uint8_t cam_csd_signature[4] = { 0x59, 0x4a, 0x58, 0x46 };

struct __attribute((packed, aligned(8))) cam_csd_header {
	/* File signature */
	uint8_t signature[4];
	/* Format version */
	uint8_t version[2];
	/* Reserved */
	uint8_t reserved[2];
};

struct __attribute((packed, aligned(8))) cam_csd_metadata {
	/* Stream identification */
	cam_uuid_t uuid;
	/* Stream name */
	uint8_t name[CAM_CSD_STREAM_NAME_LEN];
};

struct __attribute((packed, aligned(8))) cam_csd_state_ctrl {
	/* Target timeout between init and start */
	uint32_t timeout_init_to_start;
	/* Target timeout between start and first event */
	uint32_t timeout_start_to_event;
};

/* Entries describing the event alarms */
struct __attribute((packed, aligned(8))) cam_csd_event_alarms {
	/* Timeout specified */
	uint32_t timeout;
	/* Identifies the event within the stream */
	uint16_t event_id;
	/* The event mode */
	uint8_t mode;
	/* Reserved */
	uint8_t reserved;
};

struct __attribute((packed, aligned(8))) cam_csd {
	struct cam_csd_header header;
	struct cam_csd_metadata metadata;
	struct cam_csd_state_ctrl state_ctrl;

	/* Entries describing the event alarms */
	struct cam_csd_event_alarms event_alarms[];
};

#endif /* CAM_CSD_H */
