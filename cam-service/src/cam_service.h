/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef CAM_SERVICE_H
#define CAM_SERVICE_H

#include <cam_fault.h>
#include <cam_log.h>
#include <cam_socket.h>

#define CAM_SERVICE_SUCCESS     0
#define CAM_SERVICE_ERROR       -1
#define CAM_SERVICE_APP_DISCONN -2
#define CAM_SERVICE_APP_ERROR   -3

#ifdef __ZEPHYR__
#	define CONFIG_CAM_SERVICE_DEFAULT_CFG_PATH "/RAM:"

BUILD_ASSERT(
	CONFIG_CAM_SERVICE_MAX_CONN + 1 <= CONFIG_MAX_PTHREAD_COUNT,
	"CONFIG_CAM_SERVICE_MAX_CONN + 1 should not greater than "
	"CONFIG_MAX_PTHREAD_COUNT");

BUILD_ASSERT(
	CONFIG_CAM_SERVICE_MAX_CONN *CONFIG_CAM_SERVICE_MAX_STREAM_PER_CONN + 1 <=
		CONFIG_MAX_PTHREAD_MUTEX_COUNT,
	"CONFIG_CAM_SERVICE_MAX_CONN * CONFIG_CAM_SERVICE_MAX_STREAM_PER_CONN + 1 "
	"should not  greater than CONFIG_MAX_PTHREAD_MUTEX_COUNT");

BUILD_ASSERT(
	CONFIG_CAM_SERVICE_MAX_CONN *CONFIG_CAM_SERVICE_MAX_STREAM_PER_CONN *(
		CONFIG_CAM_SERVICE_MAX_EVENT_PER_STREAM + 1) <= CONFIG_MAX_TIMER_COUNT,
	"CONFIG_CAM_SERVICE_MAX_CONN * "
	"CONFIG_CAM_SERVICE_MAX_STREAM_PER_CONN*(1+CONFIG_CAM_SERVICE_MAX_EVENT_"
	"PER_STREAM) should not greater than CONFIG_MAX_TIMER_COUNT");
#else
#	define CONFIG_CAM_SERVICE_DEFAULT_CFG_PATH     "./"
#	define CONFIG_CAM_SERVICE_IP                   "127.0.0.1"
#	define CONFIG_CAM_SERVICE_PORT                 21604
#	define CONFIG_CAM_SERVICE_MAX_CONN             8
#	define CONFIG_CAM_SERVICE_MAX_STREAM_PER_CONN  8
#	define CONFIG_CAM_SERVICE_MAX_EVENT_PER_STREAM 8
#	define CONFIG_CAM_LOG_LEVEL                    CAM_LOG_LEVEL_INFO
#	define CONFIG_CAM_FAULT_ACTION                 CAM_FAULT_ACTION_NONE
#	define CONFIG_CAM_SERVICE_CLOCK_TOLERANCE_NS   0
#endif

struct cam_service_cfg {
	/* Service socket config */
	struct cam_socket_cfg socket_cfg;
	/* Log level */
	enum cam_log_level log_level;
	/* Log file name */
	char *log_file_name;
	/* Configuration directory path */
	char *cfg_dir_path;
	enum cam_fault_action fault_action;
};

#endif /* CAM_SERVICE_H */
