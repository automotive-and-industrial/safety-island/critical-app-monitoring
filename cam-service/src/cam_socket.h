/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef CAM_SOCKET_H
#define CAM_SOCKET_H

#include <arpa/inet.h>
#include <poll.h>
#include <pthread.h>
#include <semaphore.h>

struct cam_socket_cfg {
	const char *addr;
	unsigned int port;
};

struct cam_socket_ctx {
	int fd;
	sem_t conn_cnt_sem;
	struct cam_socket_cfg *cfg;
	pthread_t tid;
};

/* Initialize the service socket */
int cam_socket_init(struct cam_socket_ctx *ctx, struct cam_socket_cfg *cfg);

/* Disable the TCP delayed ack feature of the socket */
int disable_delayed_tcp_ack(int socket_fd);

#endif /* CAM_SOCKET_H */
