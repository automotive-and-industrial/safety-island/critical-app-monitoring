/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <cam_cmdline.h>
#include <cam_config.h>
#include <cam_fault.h>
#include <cam_log.h>
#include <cam_service.h>
#include <cam_socket.h>
#include <cam_stream.h>

#include <assert.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>

extern const char *cam_fault_action_name[];

static struct cam_service_cfg cfg = {
	.socket_cfg.addr = CONFIG_CAM_SERVICE_IP,
	.socket_cfg.port = CONFIG_CAM_SERVICE_PORT,
	.log_level = CONFIG_CAM_LOG_LEVEL,
	.log_file_name = NULL,
	.cfg_dir_path = CONFIG_CAM_SERVICE_DEFAULT_CFG_PATH,
	.fault_action = CONFIG_CAM_FAULT_ACTION,
};

#ifndef __ZEPHYR__
static void sig_handler(int sig)
{
	(void)sig;

	cam_log_end();

	exit(EXIT_SUCCESS);
}
#endif

static void print_config(void)
{
	assert(cfg.socket_cfg.addr != NULL);
	assert(cfg.log_level < CAM_LOG_LEVEL_COUNT);
	assert(cfg.cfg_dir_path != NULL);

	cam_log(CAM_LOG_LEVEL_INFO, "Cam service configuration:\n");

	cam_log(CAM_LOG_LEVEL_INFO, "    IP address: %s\n", cfg.socket_cfg.addr);
	cam_log(CAM_LOG_LEVEL_INFO, "    port: %u\n", cfg.socket_cfg.port);

	if (cfg.log_file_name != NULL) {
		cam_log(
			CAM_LOG_LEVEL_INFO, "    log filename: %s\n", cfg.log_file_name);
	}

	cam_log(
		CAM_LOG_LEVEL_INFO,
		"    log level: %s\n",
		cam_log_level_name[cfg.log_level]);

	cam_log(
		CAM_LOG_LEVEL_INFO,
		"    configuration directory: %s\n",
		cfg.cfg_dir_path);

	cam_log(
		CAM_LOG_LEVEL_INFO,
		"    fault action: %s\n",
		cam_fault_action_name[cfg.fault_action]);
}

int main(int argc, char *argv[])
{
	int ret = EXIT_SUCCESS;
	struct cam_socket_ctx socket_ctx;

	if (cam_cmdline_parse(argc, argv, &cfg) != CAM_SERVICE_SUCCESS) {
		exit(CAM_FAULT_CMDLINE_ERROR);
	}

	if (cam_log_init(cfg.log_level, cfg.log_file_name) != CAM_SERVICE_SUCCESS) {
		fprintf(stderr, "Failed to initialize log.\n");
		exit(CAM_FAULT_CAM_LOG_INIT_ERROR);
	}

#ifndef __ZEPHYR__
	signal(SIGINT, sig_handler);
#endif

	print_config();

	cam_fault_register_action(cfg.fault_action);

	if (cam_cfg_init(cfg.cfg_dir_path) != CAM_SERVICE_SUCCESS) {
		cam_fault_service(CAM_FAULT_CFG_INIT_ERROR);
		ret = CAM_FAULT_CFG_INIT_ERROR;
		goto exit;
	}

	if (cam_socket_init(&socket_ctx, &cfg.socket_cfg) != CAM_SERVICE_SUCCESS) {
		cam_fault_service(CAM_FAULT_SOCKET_INIT_ERROR);
		ret = CAM_FAULT_SOCKET_INIT_ERROR;
		goto exit;
	}

	if (pthread_join(socket_ctx.tid, NULL) != 0) {
		cam_fault_service(CAM_FAULT_SOCKET_DISPATCHER_THREAD_ERROR);
		ret = CAM_FAULT_SOCKET_DISPATCHER_THREAD_ERROR;
	}

exit:
	cam_log_end();

	exit(ret);
}
