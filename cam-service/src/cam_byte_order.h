/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef CAM_BYTE_ORDER_H
#define CAM_BYTE_ORDER_H

#if defined(__ZEPHYR__)

#	include <zephyr/sys/byteorder.h>

static inline uint32_t le32toh(uint32_t n)
{
	return sys_le32_to_cpu(n);
}

#	ifndef htonll
static inline uint64_t htonll(uint64_t n)
{
	return sys_cpu_to_be64(n);
}
#	endif

#	ifndef ntohll
static inline uint64_t ntohll(uint64_t n)
{
	return sys_be64_to_cpu(n);
}
#	endif

#else

#	include <arpa/inet.h>
#	include <endian.h>

#	include <stdint.h>

static inline uint64_t htonll(uint64_t n)
{
	return (htonl(1) == 1) ? n : ((uint64_t)htonl(n) << 32) | htonl(n >> 32);
}

static inline uint64_t ntohll(uint64_t n)
{
	return (htonl(1) == 1) ? n : ((uint64_t)ntohl(n) << 32) | ntohl(n >> 32);
}

#endif /* __ZEPHYR__ */

#endif /* CAM_BYTE_ORDER_H */
