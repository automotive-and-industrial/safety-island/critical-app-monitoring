/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef CAM_STREAM_H
#define CAM_STREAM_H

enum cam_stream_state {
	CAM_STREAM_STATE_UNUSED,
	CAM_STREAM_STATE_STOPPED,
	CAM_STREAM_STATE_IN_PROGRESS,
	CAM_STREAM_STATE_FAILED,
	CAM_STREAM_STATE_COUNT,
};

void cam_stream_process(int fd);

#endif /* CAM_STREAM_H */
