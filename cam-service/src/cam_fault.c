/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <cam_fault.h>
#include <cam_fault_driver_itf.h>
#include <cam_log.h>
#include <cam_service.h>
#include <cam_stream.h>

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

/* Map of cam fault id to error message */
static const char *fault_message[CAM_FAULT_ID_COUNT] = {
	[CAM_FAULT_CMDLINE_ERROR] = "Failed to parse command line",
	[CAM_FAULT_CAM_LOG_INIT_ERROR] = "Failed to init cam log",
	[CAM_FAULT_CFG_INIT_ERROR] = "Failed to initialize config module",
	[CAM_FAULT_SOCKET_INIT_ERROR] = "Failed to initialize socket module",
	[CAM_FAULT_SOCKET_DISPATCHER_THREAD_ERROR] =
		"Socket dispatcher thread error",
	[CAM_FAULT_SOCKET_CONN_THREAD_ERROR] = "Socket connection thread error",
	[CAM_FAULT_STREAM_PROCESS_TASK_ERROR] = "Stream thread process task error",
	[CAM_FAULT_STREAM_INIT_ERROR] = "Stream init error",
	[CAM_FAULT_STREAM_START_ERROR] = "Stream start error",
	[CAM_FAULT_STREAM_STOP_ERROR] = "Stream stop error",
	[CAM_FAULT_STREAM_EVENT_ERROR] = "Stream event error",
	[CAM_FAULT_STREAM_STATE_TIMEOUT_CB_ERROR] =
		"Failed to handle state timeout",
	[CAM_FAULT_STREAM_TEMPORAL_CB_ERROR] =
		"Failed to handle stream temporal error",
	[CAM_FAULT_STREAM_DEPLOY_ERROR] = "Failed to deploy stream configuration",
	[CAM_FAULT_STREAM_SEQUENCE_ERROR] = "Stream sequence error",
	[CAM_FAULT_STREAM_INIT_TO_START_TIMEOUT] =
		"Stream state init to start status timeout",
	[CAM_FAULT_STREAM_START_TO_EVENT_TIMEOUT] =
		"Stream state start to event status timeout",
	[CAM_FAULT_STREAM_LOGICAL_ERROR] = "Stream event logical error",
	[CAM_FAULT_STREAM_TEMPORAL_ERROR] = "Stream event temporal error",
	[CAM_FAULT_STREAM_STATE_ERROR] = "Stream state logical error",
	[CAM_FAULT_UNKNOWN] = "Unknown error",
};

const char *cam_fault_action_name[] = {
	[CAM_FAULT_ACTION_NONE] = "none",
	[CAM_FAULT_ACTION_EXIT] = "exit",
};

/* Map of cam stream state to error message */
static const char *state_str[CAM_STREAM_STATE_COUNT] = {
	"Unused state",
	"Stopped state",
	"In-progress state",
	"Failed state",
};

struct cam_fault {
	fault_report_t fault_report;
	enum cam_fault_action fault_action;
};

static void default_fault_report(void)
{
	return;
}

static struct cam_fault fault_manager = {
	.fault_report = default_fault_report,
	.fault_action = CAM_FAULT_ACTION_NONE,
};

static void cam_fault_report_post(enum cam_fault_id fault_id)
{
	switch (fault_manager.fault_action) {
	case CAM_FAULT_ACTION_NONE:
		break;
	case CAM_FAULT_ACTION_EXIT:
		exit(fault_id);
	default:
		cam_log(CAM_LOG_LEVEL_ERROR, "Invalid fault action.");
		break;
	}
}

void cam_fault_register_action(enum cam_fault_action action)
{
	fault_manager.fault_action = action;
}

void cam_fault_stream_sequence(
	const char *stream_name,
	cam_uuid_t stream_uuid,
	uint64_t sequence_received,
	uint64_t sequence_expected)
{
	int ret;
	char uuid_str[CAM_UUID_STR_LEN];

	assert(stream_name != NULL);

	ret = cam_uuid_to_string(stream_uuid, uuid_str);
	assert(ret == CAM_UUID_SUCCESS);
	cam_log(CAM_LOG_LEVEL_ERROR, "Stream sequence error:\n");
	cam_log(CAM_LOG_LEVEL_ERROR, "    stream_name: %s\n", stream_name);
	cam_log(CAM_LOG_LEVEL_ERROR, "    stream_uuid: %s\n", uuid_str);
	cam_log(
		CAM_LOG_LEVEL_ERROR, "    sequence_received: %ld\n", sequence_received);
	cam_log(
		CAM_LOG_LEVEL_ERROR, "    sequence_expected: %ld\n", sequence_expected);

	fault_manager.fault_report();

	cam_fault_report_post(CAM_FAULT_STREAM_SEQUENCE_ERROR);
}

void cam_fault_stream_state_timeout(
	const char *stream_name,
	cam_uuid_t stream_uuid,
	enum cam_stream_state_timeout_type timeout_type)
{
	int ret;
	char uuid_str[CAM_UUID_STR_LEN];

	assert(stream_name != NULL);

	ret = cam_uuid_to_string(stream_uuid, uuid_str);
	assert(ret == CAM_UUID_SUCCESS);
	cam_log(CAM_LOG_LEVEL_ERROR, "Stream state timeout error:\n");
	cam_log(CAM_LOG_LEVEL_ERROR, "    stream_name: %s\n", stream_name);
	cam_log(CAM_LOG_LEVEL_ERROR, "    stream_uuid: %s\n", uuid_str);
	if (timeout_type == STATE_INIT_TO_START_TIMEOUT) {
		cam_log(
			CAM_LOG_LEVEL_ERROR, "    error: state init to start timeout\n");
		fault_manager.fault_report();

		cam_fault_report_post(CAM_FAULT_STREAM_INIT_TO_START_TIMEOUT);
	} else {
		cam_log(
			CAM_LOG_LEVEL_ERROR, "    error: state start to event timeout\n");
		fault_manager.fault_report();

		cam_fault_report_post(CAM_FAULT_STREAM_START_TO_EVENT_TIMEOUT);
	}
}

/* Cam stream state error */
void cam_fault_stream_state(
	const char *stream_name,
	cam_uuid_t stream_uuid,
	uint64_t timestamp,
	enum cam_stream_state current_state,
	enum cam_stream_state requested_state)
{
	int ret;
	char uuid_str[CAM_UUID_STR_LEN];

	assert(stream_name != NULL);

	ret = cam_uuid_to_string(stream_uuid, uuid_str);
	assert(ret == CAM_UUID_SUCCESS);
	cam_log(CAM_LOG_LEVEL_ERROR, "Stream state error:\n");
	cam_log(CAM_LOG_LEVEL_ERROR, "    stream_name: %s\n", stream_name);
	cam_log(CAM_LOG_LEVEL_ERROR, "    stream_uuid: %s\n", uuid_str);
	cam_log(CAM_LOG_LEVEL_ERROR, "    timestamp: %ld\n", timestamp);
	cam_log(
		CAM_LOG_LEVEL_ERROR,
		"    current_state: %s\n",
		state_str[current_state]);
	cam_log(
		CAM_LOG_LEVEL_ERROR,
		"    requested_state: %s\n",
		state_str[requested_state]);

	fault_manager.fault_report();

	cam_fault_report_post(CAM_FAULT_STREAM_STATE_ERROR);
}

/* Cam stream logical error */
void cam_fault_stream_logical(
	const char *stream_name,
	cam_uuid_t stream_uuid,
	uint64_t timestamp,
	uint16_t event_id_received,
	uint16_t event_id_expected)
{
	int ret;
	char uuid_str[CAM_UUID_STR_LEN];

	assert(stream_name != NULL);

	ret = cam_uuid_to_string(stream_uuid, uuid_str);
	assert(ret == CAM_UUID_SUCCESS);

	cam_log(CAM_LOG_LEVEL_ERROR, "Stream logical error:\n");
	cam_log(CAM_LOG_LEVEL_ERROR, "    stream_name: %s\n", stream_name);
	cam_log(CAM_LOG_LEVEL_ERROR, "    stream_uuid: %s\n", uuid_str);
	cam_log(CAM_LOG_LEVEL_ERROR, "    timestamp: %ld\n", timestamp);
	cam_log(
		CAM_LOG_LEVEL_ERROR, "    event_id_received: %d\n", event_id_received);
	cam_log(
		CAM_LOG_LEVEL_ERROR, "    event_id_expected: %d\n", event_id_expected);

	fault_manager.fault_report();

	cam_fault_report_post(CAM_FAULT_STREAM_LOGICAL_ERROR);
}

/* Cam stream temporal error */
void cam_fault_stream_temporal(
	const char *stream_name,
	cam_uuid_t stream_uuid,
	uint16_t event_id,
	uint64_t time_received,
	uint64_t time_expected)
{
	int ret;
	char uuid_str[CAM_UUID_STR_LEN];

	assert(stream_name != NULL);

	ret = cam_uuid_to_string(stream_uuid, uuid_str);
	assert(ret == CAM_UUID_SUCCESS);

	cam_log(CAM_LOG_LEVEL_ERROR, "Stream temporal error:\n");
	cam_log(CAM_LOG_LEVEL_ERROR, "    stream_name: %s\n", stream_name);
	cam_log(CAM_LOG_LEVEL_ERROR, "    stream_uuid: %s\n", uuid_str);
	cam_log(CAM_LOG_LEVEL_ERROR, "    event_id: %d\n", event_id);
	cam_log(CAM_LOG_LEVEL_ERROR, "    time_received: %ld\n", time_received);
	cam_log(CAM_LOG_LEVEL_ERROR, "    time_expected: %ld\n", time_expected);

	fault_manager.fault_report();

	cam_fault_report_post(CAM_FAULT_STREAM_TEMPORAL_ERROR);
}

/* Cam service error */
void cam_fault_service(enum cam_fault_id fault_id)
{
	assert(fault_id >= 0 && fault_id < CAM_FAULT_ID_COUNT);

	cam_log(CAM_LOG_LEVEL_ERROR, "Service error:\n");
	cam_log(
		CAM_LOG_LEVEL_ERROR,
		"    fault: [%d]%s\n",
		fault_id,
		fault_message[fault_id]);

	fault_manager.fault_report();

	cam_fault_report_post(fault_id);
}

/* Register cam fault driver */
int cam_fault_register(fault_report_t fault_report)
{
	if (fault_report == NULL) {
		return CAM_SERVICE_ERROR;
	}

	fault_manager.fault_report = fault_report;

	return CAM_SERVICE_SUCCESS;
}
