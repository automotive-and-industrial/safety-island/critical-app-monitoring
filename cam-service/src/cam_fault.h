/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef CAM_FAULT_H
#define CAM_FAULT_H

#include <cam_stream.h>
#include <cam_uuid.h>

#include <inttypes.h>

extern const char *cam_fault_action_name[];

enum cam_fault_id {
	CAM_FAULT_UNUSED,
	CAM_FAULT_CMDLINE_ERROR,
	CAM_FAULT_CAM_LOG_INIT_ERROR,
	CAM_FAULT_CFG_INIT_ERROR,
	CAM_FAULT_SOCKET_INIT_ERROR,
	CAM_FAULT_SOCKET_DISPATCHER_THREAD_ERROR,
	CAM_FAULT_SOCKET_CONN_THREAD_ERROR,
	CAM_FAULT_STREAM_PROCESS_TASK_ERROR,
	CAM_FAULT_STREAM_INIT_ERROR,
	CAM_FAULT_STREAM_START_ERROR,
	CAM_FAULT_STREAM_STOP_ERROR,
	CAM_FAULT_STREAM_EVENT_ERROR,
	CAM_FAULT_STREAM_STATE_TIMEOUT_CB_ERROR,
	CAM_FAULT_STREAM_TEMPORAL_CB_ERROR,
	CAM_FAULT_STREAM_DEPLOY_ERROR,
	CAM_FAULT_STREAM_SEQUENCE_ERROR,
	CAM_FAULT_STREAM_INIT_TO_START_TIMEOUT,
	CAM_FAULT_STREAM_START_TO_EVENT_TIMEOUT,
	CAM_FAULT_STREAM_LOGICAL_ERROR,
	CAM_FAULT_STREAM_TEMPORAL_ERROR,
	CAM_FAULT_STREAM_STATE_ERROR,
	CAM_FAULT_UNKNOWN,
	CAM_FAULT_ID_COUNT
};

enum cam_fault_action {
	CAM_FAULT_ACTION_NONE,
	CAM_FAULT_ACTION_EXIT,
	CAM_FAULT_ACTION_COUNT
};

enum cam_stream_state_timeout_type {
	STATE_INIT_TO_START_TIMEOUT,
	STATE_START_TO_EVENT_TIMEOUT
};

void cam_fault_register_action(enum cam_fault_action action);

void cam_fault_stream_sequence(
	const char *stream_name,
	cam_uuid_t stream_uuid,
	uint64_t sequence_received,
	uint64_t sequence_expected);

void cam_fault_stream_state_timeout(
	const char *stream_name,
	cam_uuid_t stream_uuid,
	enum cam_stream_state_timeout_type timeout_type);

void cam_fault_stream_state(
	const char *stream_name,
	cam_uuid_t stream_uuid,
	uint64_t timestamp,
	enum cam_stream_state current_state,
	enum cam_stream_state requested_state);

void cam_fault_stream_logical(
	const char *stream_name,
	cam_uuid_t stream_uuid,
	uint64_t timestamp,
	uint16_t event_id_received,
	uint16_t event_id_expected);

void cam_fault_stream_temporal(
	const char *stream_name,
	cam_uuid_t stream_uuid,
	uint16_t event_id,
	uint64_t time_received,
	uint64_t time_expected);

void cam_fault_service(enum cam_fault_id fault_id);

#endif /* CAM_FAULT_H */
