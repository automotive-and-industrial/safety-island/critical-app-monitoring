/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef CAM_CONFIG_H
#define CAM_CONFIG_H

#include <cam_csd.h>
#include <cam_message.h>
#include <cam_service.h>
#include <cam_uuid.h>

#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#define CAM_CFG_FILE_EXTENSION     ".csd"
#define CAM_CFG_TMP_FILE_EXTENSION ".tmp"

enum cam_cfg_event_mode {
	CAM_CFG_EVENT_MODE_ONDEMAND_FROM_ARRIVAL,
	CAM_CFG_EVENT_MODE_ONDEMAND,
	CAM_CFG_EVENT_MODE_PERIODIC,
	CAM_CFG_EVENT_MODE_COUNT,
};

struct cam_cfg_event {
	uint32_t timeout;
	enum cam_cfg_event_mode mode;
};

struct cam_cfg_stream {
	char name[CAM_CSD_STREAM_NAME_LEN];
	uint32_t timeout_init_to_start;
	uint32_t timeout_start_to_event;
	uint16_t event_count;
	struct cam_cfg_event event[CONFIG_CAM_SERVICE_MAX_EVENT_PER_STREAM];
};

struct cam_cfg_store_ctx {
	enum cam_msg_s2a_deploy_status status;
	unsigned int offset;
	char file_name[PATH_MAX];
	char tmp_file_name[PATH_MAX];
};

int cam_cfg_init(const char *path);

int cam_cfg_stream_load(cam_uuid_t stream_uuid, struct cam_cfg_stream *cfg);

int cam_cfg_stream_store_init(
	cam_uuid_t stream_uuid,
	bool overwrite,
	unsigned int file_size,
	struct cam_cfg_store_ctx *ctx);

int cam_cfg_stream_store_append(
	struct cam_cfg_store_ctx *ctx,
	void *data,
	unsigned int size);

int cam_cfg_stream_store_end(struct cam_cfg_store_ctx *ctx);

int cam_cfg_stream_store_fail(struct cam_cfg_store_ctx *ctx);

#endif /* CAM_CONFIG_H */
