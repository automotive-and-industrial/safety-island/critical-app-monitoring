/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef CAM_LOG_H
#define CAM_LOG_H

#include <stdint.h>

enum cam_log_level {
	CAM_LOG_LEVEL_NONE,
	CAM_LOG_LEVEL_ERROR,
	CAM_LOG_LEVEL_INFO,
	CAM_LOG_LEVEL_DEBUG,
	CAM_LOG_LEVEL_COUNT,
};

static const char *const cam_log_level_name[] = {
	[CAM_LOG_LEVEL_NONE] = "none",
	[CAM_LOG_LEVEL_ERROR] = "error",
	[CAM_LOG_LEVEL_INFO] = "info",
	[CAM_LOG_LEVEL_DEBUG] = "debug",
};

/* Initialize the log module */
int cam_log_init(enum cam_log_level level, const char *file_name);

/* Output log */
void cam_log(enum cam_log_level level, const char *format, ...);

/* End the log module */
void cam_log_end(void);

#endif /* CAM_LOG_H */
