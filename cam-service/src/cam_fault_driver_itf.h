/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef CAM_FAULT_DRIVER_ITF_H
#define CAM_FAULT_DRIVER_ITF_H

typedef void (*fault_report_t)(void);

int cam_fault_register(fault_report_t fault_report);

#endif /* CAM_FAULT_DRIVER_ITF_H */
