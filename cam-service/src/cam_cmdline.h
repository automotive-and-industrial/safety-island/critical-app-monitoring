/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef CAM_CMDLINE_H
#define CAM_CMDLINE_H

#include <cam_service.h>

int cam_cmdline_parse(int argc, char *argv[], struct cam_service_cfg *cfg);

#endif /* CAM_CMDLINE_H */
