/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <arpa/inet.h>
#include <pthread.h>
#include <unistd.h>

#include <cam_byte_order.h>
#include <cam_config.h>
#include <cam_fault.h>
#include <cam_log.h>
#include <cam_message.h>
#include <cam_service.h>
#include <cam_socket.h>
#include <cam_stream.h>

#include <assert.h>
#include <inttypes.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <time.h>

#define CAM_STREAM_INVALID_HANDLER_ID 0xFFFFFFFF
#define CAM_STREAM_RX_BUFF_SIZE       2048

struct stream_event_ctx {
	unsigned int event_id;
	uint64_t time_expected;
	timer_t timer;
	struct stream_ctx *stream;
};

struct stream_ctx {
	cam_uuid_t uuid;
	pthread_mutex_t ctx_lock;
	pthread_mutex_t *pkt_handling_lock;

	/* Stream configuration */
	struct cam_cfg_stream cfg;

	/* Stream state */
	enum cam_stream_state state;
	uint64_t state_timeout;
	timer_t state_timer;

	/* Event */
	uint32_t sequence_id;
	struct stream_event_ctx event_ctx[CONFIG_CAM_SERVICE_MAX_EVENT_PER_STREAM];
};

struct stream_handler {
	uint16_t msg_size;
	int (*func)(int fd, struct stream_ctx *ctx, void *msg);
	enum cam_fault_id fault_id;
};

struct stream_deploy_ctx {
	cam_uuid_t uuid;
	pthread_mutex_t ctx_lock;

	bool is_in_progress;

	uint16_t file_size;
	uint16_t remaining_size;

	uint8_t chunk_id;

	struct cam_cfg_store_ctx store_ctx;
};

static void stream_ctx_destroy(struct stream_ctx *ctx);

static int stream_timestamp_check(
	uint64_t msg_timestamp,
	uint64_t cur_timestamp)
{
	if (msg_timestamp >
		(cur_timestamp + (uint64_t)CONFIG_CAM_SERVICE_CLOCK_TOLERANCE_NS)) {
		return CAM_SERVICE_ERROR;
	}

	return CAM_SERVICE_SUCCESS;
}

static void usec2timespec(uint64_t usec, struct timespec *timeval)
{
	assert(timeval != NULL);

	timeval->tv_sec = usec / 1000000;
	timeval->tv_nsec = (usec % 1000000) * 1000;
}

static void print_init_message(struct cam_msg_a2s_init *message)
{
	int ret;
	char uuid_str[CAM_UUID_STR_LEN];

	assert(message != NULL);

	ret = cam_uuid_to_string(message->uuid, uuid_str);
	assert(ret == CAM_UUID_SUCCESS);

	cam_log(CAM_LOG_LEVEL_INFO, "Init Message\n");
	cam_log(CAM_LOG_LEVEL_DEBUG, "\tStream ID: %s\n", uuid_str);
	cam_log(
		CAM_LOG_LEVEL_DEBUG,
		"\tTimestamp: %" PRIu64 "\n",
		ntohll(message->timestamp));
}

static void print_start_message(struct cam_msg_a2s_start *message)
{
	assert(message != NULL);

	cam_log(CAM_LOG_LEVEL_INFO, "Start Message\n");
	cam_log(
		CAM_LOG_LEVEL_DEBUG, "\tHandler ID: %i, ", ntohl(message->handler_id));
	cam_log(
		CAM_LOG_LEVEL_DEBUG,
		"Timestamp: %" PRIu64 "\n",
		ntohll(message->timestamp));
}

static void print_stop_message(struct cam_msg_a2s_stop *message)
{
	assert(message != NULL);

	cam_log(CAM_LOG_LEVEL_INFO, "Stop Message\n");
	cam_log(
		CAM_LOG_LEVEL_DEBUG, "\tHandler ID: %i, ", ntohl(message->handler_id));
	cam_log(
		CAM_LOG_LEVEL_DEBUG,
		"Timestamp: %" PRIu64 "\n",
		ntohll(message->timestamp));
}

static void print_event_message(struct cam_msg_a2s_event *message)
{
	assert(message != NULL);

	cam_log(CAM_LOG_LEVEL_INFO, "Event Message\n");
	cam_log(
		CAM_LOG_LEVEL_DEBUG, "\tHandler ID: %i, ", ntohl(message->handler_id));
	cam_log(CAM_LOG_LEVEL_DEBUG, "Event ID: %i, ", ntohs(message->event_id));
	cam_log(
		CAM_LOG_LEVEL_DEBUG, "Sequence ID: %i, ", ntohl(message->sequence_id));
	cam_log(
		CAM_LOG_LEVEL_DEBUG,
		"Timestamp: %" PRIu64 "\n",
		ntohll(message->timestamp));
}

static void print_deploy_message(struct cam_msg_a2s_deploy *message)
{
	char uuid_str[CAM_UUID_STR_LEN];
	int ret;

	assert(message != NULL);

	ret = cam_uuid_to_string(message->uuid, uuid_str);
	assert(ret == CAM_UUID_SUCCESS);

	cam_log(CAM_LOG_LEVEL_INFO, "Deploy Message\n");
	cam_log(CAM_LOG_LEVEL_DEBUG, "\tStream ID: %s\n", uuid_str);
	cam_log(CAM_LOG_LEVEL_DEBUG, "\tAttributes: %i, ", ntohs(message->attr));
	cam_log(CAM_LOG_LEVEL_DEBUG, "File size: %i, ", ntohs(message->file_size));
	cam_log(CAM_LOG_LEVEL_DEBUG, "Chunk ID: %i, ", message->chunk_id);
	cam_log(
		CAM_LOG_LEVEL_DEBUG, "Chunk size: %i, ", ntohs(message->chunk_size));
	cam_log(
		CAM_LOG_LEVEL_DEBUG,
		"Timestamp: %" PRIu64 "\n",
		ntohll(message->timestamp));
}

static int stream_msg_recv(int fd, uint8_t *buffer, uint16_t size)
{
	unsigned int bytes_recv = 0u;

	assert(buffer != NULL);

	/* Check if received data covers a complete message */
	while (bytes_recv < size) {
		ssize_t count = recv(fd, buffer + bytes_recv, size - bytes_recv, 0);

		if (disable_delayed_tcp_ack(fd) != 0) {
			return CAM_SERVICE_ERROR;
		}

		if (count == -1) {
			perror("Error receiving data");
			return CAM_SERVICE_ERROR;
		} else if (count == 0) {
			cam_log(CAM_LOG_LEVEL_INFO, "\nConnection %d is closed.\n", fd);
			return CAM_SERVICE_APP_DISCONN;
		}

		bytes_recv += count;
	}

	if (bytes_recv > size) {
		cam_log(CAM_LOG_LEVEL_INFO, "Received bytes exceed expected size.\n");
		return CAM_SERVICE_ERROR;
	}

	return CAM_SERVICE_SUCCESS;
}

static int stream_msg_send(int fd, uint8_t *buffer, uint16_t size)
{
	unsigned int bytes_sent = 0u;

	assert(buffer != NULL);

	/* Check if the complete message is sent */
	while (bytes_sent < size) {
		ssize_t count = send(fd, buffer + bytes_sent, size - bytes_sent, 0);

		if (disable_delayed_tcp_ack(fd) != 0) {
			return CAM_SERVICE_ERROR;
		}

		if (count == -1) {
			cam_log(CAM_LOG_LEVEL_ERROR, "Error sending data");
			return CAM_SERVICE_ERROR;
		}

		bytes_sent += count;
	}

	if (bytes_sent > size) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Sent bytes exceed expected size.\n");
		return CAM_SERVICE_ERROR;
	}

	return CAM_SERVICE_SUCCESS;
}

static int stream_msg_header_init(
	struct cam_msg_header *header,
	enum cam_msg_s2a_id msg_id)
{
	uint16_t size;

	assert(header != NULL);

	switch (msg_id) {
	case CAM_MSG_S2A_ID_INIT:
		size = sizeof(struct cam_msg_s2a_init);
		break;
	case CAM_MSG_S2A_ID_DEPLOY:
		size = sizeof(struct cam_msg_s2a_deploy);
		break;
	default:
		cam_log(CAM_LOG_LEVEL_ERROR, "Invalid stream msg_id: %d.\n", msg_id);
		return CAM_SERVICE_ERROR;
	}

	header->version_major = CAM_MSG_VERSION_MAJOR;
	header->version_minor = CAM_MSG_VERSION_MINOR;
	header->msg_id = htons(msg_id);
	header->size = htons(size);

	return CAM_SERVICE_SUCCESS;
}

static int stream_msg_header_check(struct cam_msg_header *header)
{
	assert(header != NULL);

	if ((header->version_major != CAM_MSG_VERSION_MAJOR) ||
		(header->version_minor != CAM_MSG_VERSION_MINOR)) {
		return CAM_SERVICE_APP_ERROR;
	}

	return CAM_SERVICE_SUCCESS;
}

static int stream_timestamp_now(uint64_t *timestamp)
{
	struct timespec tp;

	assert(timestamp != NULL);

	if (clock_gettime(CLOCK_REALTIME, &tp) != 0) {
		return CAM_SERVICE_ERROR;
	}

	/* Convert timespec to microseconds */
	*timestamp = (((uint64_t)tp.tv_sec) * 1000000) + (tp.tv_nsec / 1000);

	return CAM_SERVICE_SUCCESS;
}

static void stream_state_timeout_callback(union sigval sv)
{
	struct stream_ctx *ctx = (struct stream_ctx *)sv.sival_ptr;
	char uuid_str[CAM_UUID_STR_LEN];
	uint64_t timestamp;

	assert(ctx != NULL);

	if (pthread_mutex_lock(ctx->pkt_handling_lock) != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to lock packet lock.\n");
		cam_fault_service(CAM_FAULT_STREAM_STATE_TIMEOUT_CB_ERROR);
		return;
	}

	if (pthread_mutex_lock(&ctx->ctx_lock) != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to lock mutex.\n");
		cam_fault_service(CAM_FAULT_STREAM_STATE_TIMEOUT_CB_ERROR);
		(void)pthread_mutex_unlock(ctx->pkt_handling_lock);
		return;
	}

	if (pthread_mutex_unlock(ctx->pkt_handling_lock) != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to unlock packet lock.\n");
		cam_fault_service(CAM_FAULT_STREAM_STATE_TIMEOUT_CB_ERROR);
		goto exit;
	}

	if (cam_uuid_to_string(ctx->uuid, uuid_str) != CAM_UUID_SUCCESS) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to convert uuid.\n");
		cam_fault_service(CAM_FAULT_STREAM_TEMPORAL_CB_ERROR);
		goto exit;
	}

	if (ctx->state == CAM_STREAM_STATE_STOPPED) {
		cam_fault_stream_state_timeout(
			ctx->cfg.name, ctx->uuid, STATE_INIT_TO_START_TIMEOUT);
	} else if (
		(ctx->state == CAM_STREAM_STATE_IN_PROGRESS) &&
		(ctx->sequence_id == 0)) {
		cam_fault_stream_state_timeout(
			ctx->cfg.name, ctx->uuid, STATE_START_TO_EVENT_TIMEOUT);
	} else {
		/*
		 * The callback was fired when this context was receiving an update from
		 * a CAM packet, so if the above conditions doesn't hold anymore, we
		 * can exit without any error
		 */
		goto exit_state_ok;
	}

	if (stream_timestamp_now(&timestamp) != CAM_SERVICE_SUCCESS) {
		cam_log(
			CAM_LOG_LEVEL_ERROR,
			"Current timestamp: failed to get timestamp.\n");
		cam_fault_service(CAM_FAULT_STREAM_STATE_TIMEOUT_CB_ERROR);
		goto exit;
	}

	cam_log(CAM_LOG_LEVEL_ERROR, "Stream ID: %s\n", uuid_str);
	cam_log(
		CAM_LOG_LEVEL_ERROR,
		"    Expected timestamp: %" PRIu64 "\n",
		ctx->state_timeout);
	cam_log(
		CAM_LOG_LEVEL_ERROR, "    Current timestamp: %" PRIu64 "\n", timestamp);

exit:
	stream_ctx_destroy(ctx);
exit_state_ok:
	(void)pthread_mutex_unlock(&ctx->ctx_lock);
}

static void stream_temporal_fault_callback(union sigval sv)
{
	struct stream_event_ctx *event = (struct stream_event_ctx *)sv.sival_ptr;
	char uuid_str[CAM_UUID_STR_LEN];

	assert(event != NULL);
	assert(event->stream != NULL);

	if (pthread_mutex_lock(event->stream->pkt_handling_lock) != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to lock packet lock.\n");
		cam_fault_service(CAM_FAULT_STREAM_TEMPORAL_CB_ERROR);
		return;
	}

	if (pthread_mutex_lock(&event->stream->ctx_lock) != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to lock mutex.\n");
		cam_fault_service(CAM_FAULT_STREAM_TEMPORAL_CB_ERROR);
		(void)pthread_mutex_unlock(event->stream->pkt_handling_lock);
		return;
	}

	if (pthread_mutex_unlock(event->stream->pkt_handling_lock) != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to unlock packet lock.\n");
		cam_fault_service(CAM_FAULT_STREAM_TEMPORAL_CB_ERROR);
		goto exit;
	}

	if (cam_uuid_to_string(event->stream->uuid, uuid_str) != CAM_UUID_SUCCESS) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to convert uuid.\n");
		cam_fault_service(CAM_FAULT_STREAM_TEMPORAL_CB_ERROR);
		goto exit;
	}

	cam_fault_stream_temporal(
		event->stream->cfg.name,
		event->stream->uuid,
		event->event_id,
		0,
		event->time_expected);

exit:
	stream_ctx_destroy(event->stream);

	(void)pthread_mutex_unlock(&event->stream->ctx_lock);
}

static int stream_timer_create(
	timer_t *timer,
	void *arg,
	void (*cb)(union sigval))
{
	struct sigevent evp;

	assert(timer != NULL);
	assert(cb != NULL);

	evp.sigev_value.sival_ptr = arg;
	evp.sigev_notify_attributes = NULL;
	evp.sigev_notify_function = cb;
#ifdef __ZEPHYR__
	evp.sigev_notify = SIGEV_SIGNAL;
#else
	evp.sigev_notify = SIGEV_THREAD;
#endif

	if (timer_create(CLOCK_REALTIME, &evp, timer) != 0) {
		perror("Error creating timer");
		return CAM_SERVICE_ERROR;
	}

	return CAM_SERVICE_SUCCESS;
}

static int stream_timer_delete(timer_t timer)
{
	if (timer_delete(timer) != 0) {
		return CAM_SERVICE_ERROR;
	}

	return CAM_SERVICE_SUCCESS;
}

static int stream_timer_set(timer_t timer, uint64_t us)
{
	struct itimerspec ts;

	usec2timespec(0, &ts.it_interval);
	usec2timespec(us, &ts.it_value);

	if (timer_settime(timer, TIMER_ABSTIME, &ts, NULL) != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Error setting timer.\n");
		return CAM_SERVICE_ERROR;
	}

	return CAM_SERVICE_SUCCESS;
}

static int stream_ctx_init(
	struct stream_ctx *ctx,
	cam_uuid_t uuid,
	uint64_t init_timestamp)
{
	int ret = CAM_SERVICE_SUCCESS;
	unsigned int i, j;
	struct stream_event_ctx *event_ctx;

	assert(ctx != NULL);

	ret = cam_cfg_stream_load(uuid, &ctx->cfg);
	if (ret != CAM_SERVICE_SUCCESS) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to load stream configuration.\n");
		return ret;
	}

	cam_uuid_copy(ctx->uuid, uuid);

	ret = stream_timer_create(
		&ctx->state_timer, ctx, stream_state_timeout_callback);
	if (ret != CAM_SERVICE_SUCCESS) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to create state timer.\n");
		return ret;
	}

	/* Set init to start timer if timeout_init_to_start is not zero */
	if (ctx->cfg.timeout_init_to_start > 0) {
		ctx->state_timeout = init_timestamp + ctx->cfg.timeout_init_to_start;
		if (ctx->state_timeout < init_timestamp) {
			cam_log(CAM_LOG_LEVEL_ERROR, "Time overflowed.\n");
			ret = CAM_SERVICE_ERROR;
			goto exit;
		}
		ret = stream_timer_set(ctx->state_timer, ctx->state_timeout);
		if (ret != CAM_SERVICE_SUCCESS) {
			goto exit;
		}
	}

	memset(ctx->event_ctx, 0, sizeof(ctx->event_ctx));
	for (i = 0; i < ctx->cfg.event_count; i++) {
		event_ctx = &ctx->event_ctx[i];
		event_ctx->stream = ctx;
		event_ctx->event_id = i;

		ret = stream_timer_create(
			&event_ctx->timer, event_ctx, stream_temporal_fault_callback);
		if (ret != CAM_SERVICE_SUCCESS) {
			cam_log(CAM_LOG_LEVEL_ERROR, "Failed to create event timer.\n");
			for (j = 0; j < i; j++) {
				(void)stream_timer_delete(ctx->event_ctx[j].timer);
			}
			goto exit;
		}
	}

exit:
	if (ret != CAM_SERVICE_SUCCESS) {
		(void)stream_timer_delete(ctx->state_timer);
	} else {
		ctx->state = CAM_STREAM_STATE_STOPPED;
	}

	return ret;
}

static void stream_ctx_destroy(struct stream_ctx *ctx)
{
	assert(ctx != NULL);

	if (ctx->state == CAM_STREAM_STATE_UNUSED ||
		ctx->state == CAM_STREAM_STATE_FAILED) {
		return;
	}

	if (stream_timer_delete(ctx->state_timer) != CAM_SERVICE_SUCCESS) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to delete state timer.\n");
	}

	for (unsigned int i = 0; i < ctx->cfg.event_count; i++) {
		if (stream_timer_delete(ctx->event_ctx[i].timer) !=
			CAM_SERVICE_SUCCESS) {
			cam_log(CAM_LOG_LEVEL_ERROR, "Failed to delete event timer.\n");
		}
	}

	ctx->state = CAM_STREAM_STATE_FAILED;
}

static int stream_init(int fd, struct stream_ctx *ctx, void *stream_msg)
{
	uint64_t timestamp, msg_timestamp;
	struct cam_msg_a2s_init *msg = (struct cam_msg_a2s_init *)stream_msg;
	struct cam_msg_s2a_init send_msg;
	bool is_alloc = false;
	bool is_config = false;
	uint32_t i = 0;
	int ret = CAM_SERVICE_SUCCESS;

	assert(ctx != NULL && stream_msg != NULL);

	print_init_message(msg);

	msg_timestamp = ntohll(msg->timestamp);

	if (stream_timestamp_now(&timestamp) == CAM_SERVICE_ERROR) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to get timestamp.\n");
		return CAM_SERVICE_ERROR;
	}

	if (stream_timestamp_check(msg_timestamp, timestamp) == CAM_SERVICE_ERROR) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Received timestamp is in the future.\n");
		return CAM_SERVICE_ERROR;
	}

	for (i = 0; i < CONFIG_CAM_SERVICE_MAX_STREAM_PER_CONN; i++) {
		if (pthread_mutex_lock(&ctx[i].ctx_lock) != 0) {
			cam_log(CAM_LOG_LEVEL_ERROR, "Failed to lock mutex lock.\n");
			return CAM_SERVICE_ERROR;
		}

		/* Find a free stream_ctx element */
		if (ctx[i].state == CAM_STREAM_STATE_UNUSED) {
			is_alloc = true;
			if (pthread_mutex_unlock(ctx[i].pkt_handling_lock) != 0) {
				cam_log(CAM_LOG_LEVEL_ERROR, "Failed to unlock packet lock.\n");
				ret = CAM_SERVICE_ERROR;
			} else {
				ret = stream_ctx_init(&ctx[i], msg->uuid, msg_timestamp);
				if (ret == CAM_SERVICE_SUCCESS) {
					is_config = true;
				}
			}
		}

		if (pthread_mutex_unlock(&ctx[i].ctx_lock) != 0) {
			cam_log(CAM_LOG_LEVEL_ERROR, "Failed to unlock mutex lock.\n");
			ret = CAM_SERVICE_ERROR;
		}

		if (ret == CAM_SERVICE_ERROR) {
			goto exit;
		}

		if (is_alloc) {
			break;
		}
	}

	cam_uuid_copy(send_msg.uuid, msg->uuid);

	send_msg.timestamp = htonll(timestamp);

	if (is_alloc) {
		if (is_config) {
			send_msg.status = htons(CAM_MSG_S2A_INIT_STATUS_SUCCESS);
		} else {
			send_msg.status = htons(CAM_MSG_S2A_INIT_STATUS_NO_CONF);
		}
		send_msg.handler_id = htonl(i);
	} else {
		send_msg.status = htons(CAM_MSG_S2A_INIT_STATUS_OUT_OF_RES);
		send_msg.handler_id = htonl(CAM_STREAM_INVALID_HANDLER_ID);
	}

	ret = stream_msg_header_init(&send_msg.header, CAM_MSG_S2A_ID_INIT);
	if (ret != CAM_SERVICE_SUCCESS) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to init message header\n");
		goto exit;
	}

	ret = stream_msg_send(fd, (uint8_t *)&send_msg, sizeof(send_msg));
	if (ret != CAM_SERVICE_SUCCESS) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to send message\n");
		goto exit;
	}

exit:
	if (ret != CAM_SERVICE_SUCCESS && is_alloc) {
		stream_ctx_destroy(&ctx[i]);
	}

	return ret;
}

static int stream_start(int fd, struct stream_ctx *ctx_base, void *stream_msg)
{
	uint64_t timestamp, msg_timestamp;
	struct cam_msg_a2s_start *msg = (struct cam_msg_a2s_start *)stream_msg;
	uint32_t handler_id;
	struct stream_ctx *ctx;
	int ret = CAM_SERVICE_SUCCESS;

	(void)fd;

	assert(ctx_base != NULL && stream_msg != NULL);

	print_start_message(msg);

	msg_timestamp = ntohll(msg->timestamp);

	if (stream_timestamp_now(&timestamp) == CAM_SERVICE_ERROR) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to get timestamp.\n");
		return CAM_SERVICE_ERROR;
	}

	if (stream_timestamp_check(msg_timestamp, timestamp) == CAM_SERVICE_ERROR) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Received timestamp is in the future.\n");
		return CAM_SERVICE_ERROR;
	}

	handler_id = ntohl(msg->handler_id);

	if (handler_id >= CONFIG_CAM_SERVICE_MAX_STREAM_PER_CONN) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Invalid handler ID.\n");
		return CAM_SERVICE_APP_ERROR;
	}

	ctx = &ctx_base[handler_id];

	if (pthread_mutex_lock(&ctx->ctx_lock) != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to lock mutex lock.\n");
		return CAM_SERVICE_ERROR;
	}

	if (pthread_mutex_unlock(ctx->pkt_handling_lock) != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to unlock packet lock.\n");
		ret = CAM_SERVICE_ERROR;
		goto exit;
	}

	if (ctx->state == CAM_STREAM_STATE_UNUSED) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Invalid handler ID.\n");
		ret = CAM_SERVICE_APP_ERROR;
		goto exit;
	}

	/* Clear state timer */
	if (ctx->state != CAM_STREAM_STATE_FAILED) {
		if (stream_timer_set(ctx->state_timer, 0) == CAM_SERVICE_ERROR) {
			cam_log(
				CAM_LOG_LEVEL_ERROR, "Failed to clear stream start timer.\n");
			ret = CAM_SERVICE_ERROR;
			goto exit;
		}
	}

	if (ctx->state != CAM_STREAM_STATE_STOPPED) {
		cam_fault_stream_state(
			ctx->cfg.name,
			ctx->uuid,
			timestamp,
			ctx->state,
			CAM_STREAM_STATE_STOPPED);
		ret = CAM_SERVICE_APP_ERROR;
		goto exit;
	}

	/* Set start to event timer */
	ctx->state_timeout = msg_timestamp + ctx->cfg.timeout_start_to_event;
	if (ctx->state_timeout < msg_timestamp) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Start to event timer time overflowed.\n");
		ret = CAM_SERVICE_ERROR;
		goto exit;
	}

	ret = stream_timer_set(ctx->state_timer, ctx->state_timeout);

	ctx->sequence_id = 0;
	ctx->state = CAM_STREAM_STATE_IN_PROGRESS;

exit:
	if (ret != CAM_SERVICE_SUCCESS) {
		stream_ctx_destroy(ctx);
	}

	if (pthread_mutex_unlock(&ctx->ctx_lock) != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to unlock mutex lock.\n");
		ctx->state = CAM_STREAM_STATE_FAILED;
		return CAM_SERVICE_ERROR;
	}

	return ret;
}

static int stream_stop(int fd, struct stream_ctx *ctx_base, void *stream_msg)
{
	uint64_t msg_timestamp, timestamp;
	struct cam_msg_a2s_stop *msg = (struct cam_msg_a2s_stop *)stream_msg;
	uint32_t handler_id;
	struct stream_ctx *ctx;
	int ret = CAM_SERVICE_SUCCESS;

	(void)fd;

	assert(ctx_base != NULL && stream_msg != NULL);

	print_stop_message(msg);

	msg_timestamp = ntohll(msg->timestamp);

	if (stream_timestamp_now(&timestamp) == CAM_SERVICE_ERROR) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to get timestamp.\n");
		return CAM_SERVICE_ERROR;
	}

	if (stream_timestamp_check(msg_timestamp, timestamp) == CAM_SERVICE_ERROR) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Received timestamp is in the future.\n");
		return CAM_SERVICE_ERROR;
	}

	handler_id = ntohl(msg->handler_id);

	if (handler_id >= CONFIG_CAM_SERVICE_MAX_STREAM_PER_CONN) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Invalid handler ID.\n");
		return CAM_SERVICE_APP_ERROR;
	}

	ctx = &ctx_base[handler_id];

	if (pthread_mutex_lock(&ctx->ctx_lock) != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to lock mutex lock.\n");
		return CAM_SERVICE_ERROR;
	}

	if (pthread_mutex_unlock(ctx->pkt_handling_lock) != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to unlock packet lock.\n");
		ret = CAM_SERVICE_ERROR;
		goto exit;
	}

	if (ctx->state == CAM_STREAM_STATE_UNUSED) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Invalid handler ID.\n");
		ret = CAM_SERVICE_APP_ERROR;
		goto exit;
	}

	/* Clear state timer */
	if (ctx->state != CAM_STREAM_STATE_FAILED) {
		if (stream_timer_set(ctx->state_timer, 0) == CAM_SERVICE_ERROR) {
			cam_log(
				CAM_LOG_LEVEL_ERROR, "Failed to clear stream start timer.\n");
			ret = CAM_SERVICE_ERROR;
			goto exit;
		}
	}

	if (ctx->state != CAM_STREAM_STATE_IN_PROGRESS) {
		cam_fault_stream_state(
			ctx->cfg.name,
			ctx->uuid,
			timestamp,
			ctx->state,
			CAM_STREAM_STATE_IN_PROGRESS);
		ret = CAM_SERVICE_APP_ERROR;
		goto exit;
	}

	/* Clear event timer */
	for (unsigned int i = 0; i < ctx->cfg.event_count; i++) {
		if (stream_timer_set(ctx->event_ctx[i].timer, 0) == CAM_SERVICE_ERROR) {
			cam_log(
				CAM_LOG_LEVEL_ERROR, "Failed to clear stream start timer.\n");
			ret = CAM_SERVICE_ERROR;
			goto exit;
		}
	}

	ctx->state = CAM_STREAM_STATE_STOPPED;

exit:
	if (ret != CAM_SERVICE_SUCCESS) {
		stream_ctx_destroy(ctx);
	}

	if (pthread_mutex_unlock(&ctx->ctx_lock) != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to unlock mutex lock.\n");
		ctx->state = CAM_STREAM_STATE_FAILED;
		return CAM_SERVICE_ERROR;
	}

	return ret;
}

static int stream_event(int fd, struct stream_ctx *ctx_base, void *stream_msg)
{
	uint64_t msg_timestamp, timestamp;
	struct cam_msg_a2s_event *msg = (struct cam_msg_a2s_event *)stream_msg;
	uint32_t handler_id;
	uint16_t event_id, exp_event_id;
	uint32_t sequence_id;
	struct stream_ctx *ctx;
	struct stream_event_ctx *event_ctx;

	int ret = CAM_SERVICE_SUCCESS;

	(void)fd;

	assert(ctx_base != NULL && stream_msg != NULL);

	print_event_message(msg);

	msg_timestamp = ntohll(msg->timestamp);

	if (stream_timestamp_now(&timestamp) == CAM_SERVICE_ERROR) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to get timestamp.\n");
		return CAM_SERVICE_ERROR;
	}

	if (stream_timestamp_check(msg_timestamp, timestamp) == CAM_SERVICE_ERROR) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Received timestamp is in the future.\n");
		return CAM_SERVICE_ERROR;
	}

	handler_id = ntohl(msg->handler_id);
	event_id = ntohs(msg->event_id);
	sequence_id = ntohl(msg->sequence_id);

	if (handler_id >= CONFIG_CAM_SERVICE_MAX_STREAM_PER_CONN) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Invalid handler ID.\n");
		return CAM_SERVICE_APP_ERROR;
	}

	ctx = &ctx_base[handler_id];

	if (pthread_mutex_lock(&ctx->ctx_lock) != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to lock mutex lock.\n");
		return CAM_SERVICE_ERROR;
	}

	if (pthread_mutex_unlock(ctx->pkt_handling_lock) != 0) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to unlock packet lock.\n");
		ret = CAM_SERVICE_ERROR;
		goto exit;
	}

	if (ctx->state == CAM_STREAM_STATE_UNUSED) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Invalid handler ID.\n");
		ret = CAM_SERVICE_APP_ERROR;
		goto exit;
	}

	/* Clear state timer */
	if (ctx->state != CAM_STREAM_STATE_FAILED && sequence_id == 0) {
		if (stream_timer_set(ctx->state_timer, 0) == CAM_SERVICE_ERROR) {
			cam_log(
				CAM_LOG_LEVEL_ERROR, "Failed to clear stream state timer.\n");
			ret = CAM_SERVICE_ERROR;
			goto exit;
		}
	}

	if (ctx->state != CAM_STREAM_STATE_IN_PROGRESS) {
		cam_fault_stream_state(
			ctx->cfg.name,
			ctx->uuid,
			timestamp,
			ctx->state,
			CAM_STREAM_STATE_IN_PROGRESS);
		ret = CAM_SERVICE_APP_ERROR;
		goto exit;
	}

	/* Check sequence id */
	if (ctx->sequence_id != sequence_id) {
		cam_fault_stream_sequence(
			ctx->cfg.name, ctx->uuid, sequence_id, ctx->sequence_id);
		ret = CAM_SERVICE_APP_ERROR;
		goto exit;
	}

	ctx->sequence_id++;

	/* Check event id */
	exp_event_id = sequence_id % ctx->cfg.event_count;

	if (event_id != exp_event_id) {
		cam_fault_stream_logical(
			ctx->cfg.name, ctx->uuid, timestamp, event_id, exp_event_id);
		ret = CAM_SERVICE_APP_ERROR;
		goto exit;
	}

	/* Set the event period timer */
	event_ctx = &ctx->event_ctx[event_id];

	event_ctx->time_expected = timestamp + ctx->cfg.event[event_id].timeout;
	if (event_ctx->time_expected < timestamp) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Event time overflowed.\n");
		ret = CAM_SERVICE_ERROR;
		goto exit;
	}

	ret = stream_timer_set(event_ctx->timer, event_ctx->time_expected);
	if (ret != CAM_SERVICE_SUCCESS) {
		cam_log(CAM_LOG_LEVEL_ERROR, "Failed to set event timer.\n");
	}

exit:
	if (ret != CAM_SERVICE_SUCCESS) {
		stream_ctx_destroy(ctx);
	}

	if (pthread_mutex_unlock(&ctx->ctx_lock) != 0) {
		return CAM_SERVICE_ERROR;
	}

	return ret;
}

static int stream_deploy_reply(
	int fd,
	cam_uuid_t uuid,
	enum cam_msg_s2a_deploy_status status)
{
	uint64_t timestamp;
	struct cam_msg_s2a_deploy send_msg;

	assert(uuid != NULL);

	if (status != CAM_MSG_S2A_DEPLOY_STATUS_SUCCESS) {
		cam_log(CAM_LOG_LEVEL_INFO, "Deploy error status is %d.\n", status);
	}

	if (stream_msg_header_init(&send_msg.header, CAM_MSG_S2A_ID_DEPLOY) !=
		CAM_SERVICE_SUCCESS) {
		return CAM_SERVICE_ERROR;
	}

	if (stream_timestamp_now(&timestamp) != CAM_SERVICE_SUCCESS) {
		return CAM_SERVICE_ERROR;
	}

	cam_uuid_copy(send_msg.uuid, uuid);
	send_msg.timestamp = htonll(timestamp);
	send_msg.status = status;

	if (stream_msg_send(fd, (uint8_t *)&send_msg, sizeof(send_msg)) !=
		CAM_SERVICE_SUCCESS) {
		return CAM_SERVICE_ERROR;
	}

	return CAM_SERVICE_SUCCESS;
}

static int stream_deploy(
	int fd,
	struct stream_deploy_ctx *ctx,
	void *stream_msg)
{
	struct cam_msg_a2s_deploy *msg = (struct cam_msg_a2s_deploy *)stream_msg;
	uint16_t chunk_size;
	enum cam_msg_s2a_deploy_status status = CAM_MSG_S2A_DEPLOY_STATUS_SUCCESS;
	int ret = CAM_SERVICE_SUCCESS;

	assert(ctx != NULL);
	assert(stream_msg != NULL);

	print_deploy_message(msg);

	if (!ctx->is_in_progress) {
		bool overwrite =
			(ntohs(msg->attr) & CAM_MSG_DEPLOY_ATTR_OVERWRITE) ? true : false;

		cam_uuid_copy(ctx->uuid, msg->uuid);
		ctx->chunk_id = 0;
		ctx->file_size = ntohs(msg->file_size);
		ctx->remaining_size = ctx->file_size;

		ret = cam_cfg_stream_store_init(
			msg->uuid, overwrite, ctx->file_size, &ctx->store_ctx);
		if (ret != CAM_SERVICE_SUCCESS) {
			cam_log(
				CAM_LOG_LEVEL_ERROR,
				"Failed to initialize deployment session.\n");
			if (ret == CAM_SERVICE_APP_ERROR) {
				status = ctx->store_ctx.status;
				goto reply;
			}
			goto exit;
		}
		ctx->is_in_progress = true;
	} else if (cam_uuid_compare(ctx->uuid, msg->uuid) != 0) {
		status = CAM_MSG_S2A_DEPLOY_STATUS_EXCEED_MAX_DEPLOY_SESSION;
		ret = CAM_SERVICE_APP_ERROR;
		goto reply;
	}

	if (ctx->chunk_id != msg->chunk_id) {
		status = CAM_MSG_S2A_DEPLOY_STATUS_INVALID_ORDER;
		ret = CAM_SERVICE_APP_ERROR;
		goto reply;
	}

	chunk_size = ntohs(msg->chunk_size);

	if (chunk_size + sizeof(struct cam_msg_a2s_deploy) !=
		ntohs(msg->header.size)) {
		status = CAM_MSG_S2A_DEPLOY_STATUS_INVALID_CHUNK_SIZE;
		ret = CAM_SERVICE_APP_ERROR;
		goto reply;
	}

	if (ctx->remaining_size < chunk_size) {
		status = CAM_MSG_S2A_DEPLOY_STATUS_INVALID_CHUNK_SIZE;
		ret = CAM_SERVICE_APP_ERROR;
		goto reply;
	}

	if (chunk_size == 0) {
		if (ctx->chunk_id != 0) {
			status = CAM_MSG_S2A_DEPLOY_STATUS_INVALID_CHUNK_SIZE;
			ret = CAM_SERVICE_APP_ERROR;
			goto reply;
		}
	} else {
		ret = cam_cfg_stream_store_append(
			&ctx->store_ctx, msg->chunk_data, chunk_size);
		if (ret != CAM_SERVICE_SUCCESS) {
			if (ret == CAM_SERVICE_APP_ERROR) {
				status = ctx->store_ctx.status;
				goto reply;
			}
			goto exit;
		}
	}

	ctx->chunk_id++;
	ctx->remaining_size -= chunk_size;

	if (ctx->remaining_size == 0) {
		ret = cam_cfg_stream_store_end(&ctx->store_ctx);

		if (ret == CAM_SERVICE_APP_ERROR) {
			status = ctx->store_ctx.status;
			goto reply;
		} else if (ret != CAM_SERVICE_SUCCESS) {
			goto exit;
		}

		ctx->is_in_progress = false;
	}

reply:
	if (stream_deploy_reply(fd, msg->uuid, status) != CAM_SERVICE_SUCCESS) {
		ret = CAM_SERVICE_ERROR;
	}

exit:
	if (ret != CAM_SERVICE_SUCCESS) {
		ctx->is_in_progress = false;
		if (cam_cfg_stream_store_fail(&ctx->store_ctx) != CAM_SERVICE_SUCCESS) {
			ret = CAM_SERVICE_ERROR;
		}
		memset(ctx, 0, sizeof(struct stream_deploy_ctx));
	}

	return ret;
}

static const struct stream_handler handlers[CAM_MSG_A2S_ID_COUNT] = {
	{ 0, NULL, CAM_FAULT_UNUSED },
	{ sizeof(struct cam_msg_a2s_init),
	  stream_init,
	  CAM_FAULT_STREAM_INIT_ERROR },
	{ sizeof(struct cam_msg_a2s_start),
	  stream_start,
	  CAM_FAULT_STREAM_START_ERROR },
	{ sizeof(struct cam_msg_a2s_stop),
	  stream_stop,
	  CAM_FAULT_STREAM_STOP_ERROR },
	{ sizeof(struct cam_msg_a2s_event),
	  stream_event,
	  CAM_FAULT_STREAM_EVENT_ERROR },
};

void cam_stream_process(int fd)
{
	struct stream_ctx ctx[CONFIG_CAM_SERVICE_MAX_STREAM_PER_CONN];
	uint8_t rx_buff[CAM_STREAM_RX_BUFF_SIZE];
	struct cam_msg_header *header = (struct cam_msg_header *)rx_buff;
	const uint16_t header_size = sizeof(struct cam_msg_header);
	uint16_t msg_size, payload_size;
	enum cam_msg_a2s_id msg_id;
	struct stream_deploy_ctx deploy_ctx;
	pthread_mutex_t pkt_handling_lock;
	int i;
	int ret;

	cam_log(CAM_LOG_LEVEL_INFO, "\nConnection %d is created.\n", fd);

	memset(ctx, 0, sizeof(ctx));
	memset(&deploy_ctx, 0, sizeof(deploy_ctx));
	if (pthread_mutex_init(&pkt_handling_lock, NULL) != 0) {
		cam_log(
			CAM_LOG_LEVEL_ERROR, "Failed to initialize packet mutex lock.\n");
		cam_fault_service(CAM_FAULT_STREAM_PROCESS_TASK_ERROR);
		goto exit;
	}
	for (i = 0; i < CONFIG_CAM_SERVICE_MAX_STREAM_PER_CONN; i++) {
		if (pthread_mutex_init(&ctx[i].ctx_lock, NULL) != 0) {
			cam_log(
				CAM_LOG_LEVEL_ERROR,
				"Failed to initialize stream mutex lock.\n");
			cam_fault_service(CAM_FAULT_STREAM_PROCESS_TASK_ERROR);
			goto exit;
		}
		ctx[i].pkt_handling_lock = &pkt_handling_lock;
	}

	/* Process message */
	while (1) {
		/* Receive message header */
		ret = stream_msg_recv(fd, rx_buff, sizeof(struct cam_msg_header));
		if (ret == CAM_SERVICE_ERROR) {
			cam_log(CAM_LOG_LEVEL_ERROR, "Failed to receive stream message.\n");
			cam_fault_service(CAM_FAULT_STREAM_PROCESS_TASK_ERROR);
			goto exit;
		}
		if (ret == CAM_SERVICE_APP_DISCONN) {
			goto exit;
		}
		if (pthread_mutex_lock(&pkt_handling_lock) != 0) {
			cam_log(CAM_LOG_LEVEL_ERROR, "Failed to lock packet lock.\n");
			goto exit;
		}

		ret = stream_msg_header_check(header);
		if (ret != CAM_SERVICE_SUCCESS) {
			cam_log(CAM_LOG_LEVEL_ERROR, "Message header is invalid.\n");
			goto pkt_unlock_exit;
		}

		msg_id = ntohs(header->msg_id);
		if (msg_id >= CAM_MSG_A2S_ID_COUNT || msg_id == 0) {
			cam_log(CAM_LOG_LEVEL_ERROR, "Unknown message ID %d.\n", msg_id);
			goto pkt_unlock_continue;
		}

		/* Receive message payload */
		msg_size = ntohs(header->size);
		payload_size = msg_size - header_size;

		if (msg_size <= CAM_STREAM_RX_BUFF_SIZE) {
			ret = stream_msg_recv(fd, rx_buff + header_size, payload_size);
			if (ret == CAM_SERVICE_ERROR) {
				cam_log(
					CAM_LOG_LEVEL_ERROR, "Failed to receive stream payload.\n");
				cam_fault_service(CAM_FAULT_STREAM_PROCESS_TASK_ERROR);
				goto pkt_unlock_exit;
			}
			if (ret == CAM_SERVICE_APP_DISCONN) {
				goto pkt_unlock_exit;
			}
		} else {
			cam_log(CAM_LOG_LEVEL_ERROR, "Message is too long.\n");

			if (msg_id == CAM_MSG_A2S_ID_DEPLOY) {
				struct cam_msg_a2s_deploy *msg =
					(struct cam_msg_a2s_deploy *)rx_buff;
				ret = stream_msg_recv(
					fd,
					rx_buff + header_size,
					sizeof(struct cam_msg_a2s_deploy) - header_size);
				if (ret != CAM_SERVICE_SUCCESS) {
					goto pkt_unlock_exit;
				}
				(void)stream_deploy_reply(
					fd,
					msg->uuid,
					CAM_MSG_S2A_DEPLOY_STATUS_INVALID_CHUNK_SIZE);
			}

			goto pkt_unlock_exit;
		}

		if (msg_id == CAM_MSG_A2S_ID_DEPLOY) {
			ret = stream_deploy(fd, &deploy_ctx, rx_buff);
			if (ret == CAM_SERVICE_ERROR) {
				cam_log(
					CAM_LOG_LEVEL_ERROR,
					"Unexpected error in deploy processing.\n");
				cam_fault_service(CAM_FAULT_STREAM_DEPLOY_ERROR);
				goto pkt_unlock_exit;
			}
			goto pkt_unlock_continue;
		}

		if (msg_size != handlers[msg_id].msg_size) {
			cam_log(CAM_LOG_LEVEL_ERROR, "Message size is incorrect.\n");
			goto pkt_unlock_continue;
		}

		if (handlers[msg_id].func != NULL) {
			ret = handlers[msg_id].func(fd, ctx, rx_buff);
			if (ret == CAM_SERVICE_ERROR) {
				cam_log(
					CAM_LOG_LEVEL_ERROR,
					"Unexpected error in message processing.\n");
				cam_fault_service(handlers[msg_id].fault_id);
				goto exit;
			}
			/*
			 * pkt_handling_lock is unlocked from the handlers, so here there is
			 * no need to unlock it, continue to the next iteration
			 */
			continue;
		}
		/*
		 * This part is unreachable, if the code reaches here, it means that
		 * handlers[msg_id].func above was NULL, meaning a bug in the code,
		 * since the handler is NULL, jump to unlock the packet lock and exit
		 */
		cam_log(
			CAM_LOG_LEVEL_ERROR,
			"Unreachable code inside cam_stream_process reached.\n");
		goto pkt_unlock_exit;

	pkt_unlock_continue:
		if (pthread_mutex_unlock(&pkt_handling_lock) != 0) {
			cam_log(CAM_LOG_LEVEL_ERROR, "Failed to unlock packet lock.\n");
			goto exit;
		}
	}
pkt_unlock_exit:
	(void)pthread_mutex_unlock(&pkt_handling_lock);
exit:
	(void)pthread_mutex_destroy(&pkt_handling_lock);
	for (i = 0; i < CONFIG_CAM_SERVICE_MAX_STREAM_PER_CONN; i++) {
		stream_ctx_destroy(&ctx[i]);
		(void)pthread_mutex_destroy(&ctx[i].ctx_lock);
	}

	if (deploy_ctx.is_in_progress) {
		if (cam_cfg_stream_store_fail(&deploy_ctx.store_ctx) !=
			CAM_SERVICE_SUCCESS) {
			cam_log(CAM_LOG_LEVEL_ERROR, "Failed to clean store session.\n");
		}
	}
}
