/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <cam_log.h>
#include <cam_service.h>

#include <assert.h>
#include <stdarg.h>
#include <stdio.h>

struct cam_log_config {
	uint8_t level;
	FILE *fd;
};

static struct cam_log_config cfg = {
	.level = CAM_LOG_LEVEL_NONE,
	.fd = NULL,
};

void cam_log(enum cam_log_level level, const char *format, ...)
{
	if (level <= cfg.level) {
		assert(cfg.fd != NULL);

		if (level == CAM_LOG_LEVEL_ERROR) {
			fprintf(cfg.fd, "ERROR: ");
		}

		va_list args;
		va_start(args, format);
		vfprintf(cfg.fd, format, args);
		va_end(args);
	}
}

int cam_log_init(enum cam_log_level level, const char *file_name)
{
	switch (level) {
	case CAM_LOG_LEVEL_NONE:
		return CAM_SERVICE_SUCCESS;
	case CAM_LOG_LEVEL_ERROR:
		cfg.fd = stderr;
		break;
	case CAM_LOG_LEVEL_INFO:
	case CAM_LOG_LEVEL_DEBUG:
		cfg.fd = stdout;
		break;
	default:
		fprintf(stderr, "Invalid log level.\n");
		return CAM_SERVICE_ERROR;
	}

	if (file_name != NULL) {
#ifdef __ZEPHYR__
		fprintf(stderr, "Log file not supported.\n");
		return CAM_SERVICE_ERROR;
#else
		cfg.fd = fopen(file_name, "w");
		if (cfg.fd == NULL) {
			fprintf(stderr, "Can not open log file %s.\n", file_name);
			return CAM_SERVICE_ERROR;
		}
#endif
	}

	cfg.level = level;

	return CAM_SERVICE_SUCCESS;
}

void cam_log_end()
{
	FILE *fd = cfg.fd;

	cfg.level = CAM_LOG_LEVEL_NONE;
	cfg.fd = NULL;

	if (fd == NULL) {
		return;
	}

#ifndef __ZEPHYR__
	if (fflush(fd) != 0) {
		fprintf(stderr, "Failed to flush file.\n");
	}

	if ((fd != stderr) && (fd != stdout)) {
		if (fclose(fd) != 0) {
			fprintf(stderr, "Failed to close file.\n");
		}
	}
#endif
}
