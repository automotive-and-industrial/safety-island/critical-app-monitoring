/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef CAM_MESSAGE_H
#define CAM_MESSAGE_H

#include <cam_uuid.h>

#include <stdint.h>

#define CAM_MSG_VERSION_MAJOR 1
#define CAM_MSG_VERSION_MINOR 0

/* Message ID from application to service */
enum cam_msg_a2s_id {
	CAM_MSG_A2S_ID_INIT = 1,
	CAM_MSG_A2S_ID_START,
	CAM_MSG_A2S_ID_STOP,
	CAM_MSG_A2S_ID_EVENT,
	CAM_MSG_A2S_ID_DEPLOY,
	CAM_MSG_A2S_ID_COUNT,
};

/* Message ID from service to application */
enum cam_msg_s2a_id {
	CAM_MSG_S2A_ID_INIT = 1,
	CAM_MSG_S2A_ID_DEPLOY = 4,
	CAM_MSG_S2A_ID_COUNT,
};

struct __attribute((packed, aligned(8))) cam_msg_header {
	/* Message major version */
	uint8_t version_major;
	/* Message minor version */
	uint8_t version_minor;
	/* Message size */
	uint16_t size;
	/* Message identification */
	uint16_t msg_id;
	/* Reserved */
	uint16_t reserved;
};

/* Initialization message from application to service */
struct __attribute((packed, aligned(8))) cam_msg_a2s_init {
	/* Message header */
	struct cam_msg_header header;
	/* Timestamp from when the message was sent */
	uint64_t timestamp;
	/* Stream identification */
	cam_uuid_t uuid;
};

/* Initialization message status from service to application */
enum cam_msg_s2a_init_status {
	/* Request is success */
	CAM_MSG_S2A_INIT_STATUS_SUCCESS = 0,
	/* Service is out of resources */
	CAM_MSG_S2A_INIT_STATUS_OUT_OF_RES,
	/* Service has no configuration for the requested UUID */
	CAM_MSG_S2A_INIT_STATUS_NO_CONF,
	CAM_MSG_S2A_INIT_STATUS_COUNT,
};

/* Initialization message from service to application */
struct __attribute((packed, aligned(8))) cam_msg_s2a_init {
	/* Message header */
	struct cam_msg_header header;
	/* Timestamp from when the message was sent */
	uint64_t timestamp;
	/* Stream identification */
	cam_uuid_t uuid;
	/* Status */
	uint16_t status;
	/* Reserved */
	uint16_t reserved;
	/* Handler identification */
	uint32_t handler_id;
};

/* Start message from application to service */
struct __attribute((packed, aligned(8))) cam_msg_a2s_start {
	/* Message header */
	struct cam_msg_header header;
	/* Timestamp from when the message was sent */
	uint64_t timestamp;
	/* Stream Handler */
	uint32_t handler_id;
	/* Reserved */
	uint32_t reserved;
};

/* Stop message from application to service */
struct __attribute((packed, aligned(8))) cam_msg_a2s_stop {
	/* Message header */
	struct cam_msg_header header;
	/* Timestamp from when the message was sent */
	uint64_t timestamp;
	/* Stream Handler */
	uint32_t handler_id;
	/* Reserved */
	uint32_t reserved;
};

/* Event message from application to service */
struct __attribute((packed, aligned(8))) cam_msg_a2s_event {
	/* Message header */
	struct cam_msg_header header;
	/* Timestamp from when the message was sent */
	uint64_t timestamp;
	/* Stream Handler */
	uint32_t handler_id;
	/* Sequence identifier */
	uint32_t sequence_id;
	/* Event identifier */
	uint16_t event_id;
	/* Reserved */
	uint64_t reserved : 6;
};

#define CAM_MSG_DEPLOY_ATTR_OVERWRITE 0x0001

/* Deployment message from application to service */
struct __attribute((packed, aligned(8))) cam_msg_a2s_deploy {
	/* Message header */
	struct cam_msg_header header;
	/* Timestamp from when the message was sent */
	uint64_t timestamp;
	/* Stream identification */
	cam_uuid_t uuid;
	/* Deployment attributes */
	uint16_t attr;
	/* File size in bytes */
	uint16_t file_size;
	/* Reserved */
	uint8_t reserved;
	/* Chunk index */
	uint8_t chunk_id;
	/* Chunk size in bytes */
	uint16_t chunk_size;
	/* Chuck data */
	uint8_t chunk_data[];
};

/* Deployment message status from service to application */
enum cam_msg_s2a_deploy_status {
	/* Request is success */
	CAM_MSG_S2A_DEPLOY_STATUS_SUCCESS = 0,
	/* Deployment message is disable */
	CAM_MSG_S2A_DEPLOY_STATUS_DISABLED,
	/* Failed to write chunk */
	CAM_MSG_S2A_DEPLOY_STATUS_FAILED_TO_WRITE,
	/* Deployment file already exists */
	CAM_MSG_S2A_DEPLOY_STATUS_DEPLOY_FILE_EXIST,
	/* Invalid file size */
	CAM_MSG_S2A_DEPLOY_STATUS_INVALID_FILE_SIZE,
	/* Invalid chunk order */
	CAM_MSG_S2A_DEPLOY_STATUS_INVALID_ORDER,
	/* Invalid chunk size */
	CAM_MSG_S2A_DEPLOY_STATUS_INVALID_CHUNK_SIZE,
	/* Exceeded maximum deployment sessions */
	CAM_MSG_S2A_DEPLOY_STATUS_EXCEED_MAX_DEPLOY_SESSION,
	CAM_MSG_S2A_DEPLOY_STATUS_COUNT,
};

/* Deployment message from service to application */
struct __attribute((packed, aligned(8))) cam_msg_s2a_deploy {
	/* Message header */
	struct cam_msg_header header;
	/* Timestamp from when the message was sent */
	uint64_t timestamp;
	/* Stream identification */
	cam_uuid_t uuid;
	/* Status */
	uint8_t status;
};

#endif /* CAM_MESSAGE_H */
