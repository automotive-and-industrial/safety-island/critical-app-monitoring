/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "test_suite.h"

#include <CUnit/Basic.h>

#include <cam_fault.h>
#include <cam_fault_driver_itf.h>
#include <cam_log.h>
#include <cam_service.h>
#include <cam_uuid.h>

#include <stdlib.h>
#include <time.h>

static unsigned fault_report_count = 0;
static const char *log_name = "test_cam_fault.log";

static void cam_fault_report(void)
{
	fault_report_count++;
}

static int test_fault_suite_init(void)
{
	if (cam_log_init(CAM_LOG_LEVEL_DEBUG, log_name) != CAM_SERVICE_SUCCESS) {
		fprintf(stderr, "Failed to initialize log.\n");
		return -1;
	}

	return 0;
}

static void test_cam_fault_register(void)
{
	fault_report_t empty_fault_report = NULL;

	CU_ASSERT(cam_fault_register(empty_fault_report) == CAM_SERVICE_ERROR);
	CU_ASSERT(cam_fault_register(cam_fault_report) == CAM_SERVICE_SUCCESS);
}

static void test_cam_fault_stream_sequence(void)
{
	unsigned int report_cnt = fault_report_count;
	char *stream_name = "CAM STREAM 0001";
	char *uuid_str = "84085ddc-bc10-11ed-9a44-7ef9696e0001";
	cam_uuid_t stream_uuid;
	uint64_t sequence_received = 2;
	uint64_t sequence_expected = 1;
	int ret;

	ret = cam_uuid_from_string(uuid_str, stream_uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	cam_fault_register(cam_fault_report);
	cam_fault_stream_sequence(
		stream_name, stream_uuid, sequence_received, sequence_expected);

	CU_ASSERT(fault_report_count == report_cnt + 1);
}

static void test_cam_fault_stream_state_timeout(void)
{
	unsigned int report_cnt = fault_report_count;
	char *stream_name = "CAM STREAM 0001";
	char *uuid_str = "84085ddc-bc10-11ed-9a44-7ef9696e0001";
	cam_uuid_t stream_uuid;
	int ret;

	ret = cam_uuid_from_string(uuid_str, stream_uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	cam_fault_register(cam_fault_report);
	cam_fault_stream_state_timeout(
		stream_name, stream_uuid, STATE_INIT_TO_START_TIMEOUT);

	CU_ASSERT(fault_report_count == report_cnt + 1);
}

static void test_cam_fault_state(void)
{
	unsigned int report_cnt = fault_report_count;
	struct timespec tp;
	char *stream_name = "CAM STREAM 0001";
	char *uuid_str = "84085ddc-bc10-11ed-9a44-7ef9696e0001";
	enum cam_stream_state current_state = CAM_STREAM_STATE_STOPPED;
	enum cam_stream_state requested_state = CAM_STREAM_STATE_IN_PROGRESS;
	uint64_t timestamp;
	cam_uuid_t stream_uuid;
	int ret;

	ret = cam_uuid_from_string(uuid_str, stream_uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	CU_ASSERT(clock_gettime(CLOCK_REALTIME, &tp) == 0);
	timestamp = (((uint64_t)tp.tv_sec) * 1000000) + (tp.tv_nsec / 1000);

	cam_fault_register(cam_fault_report);
	cam_fault_stream_state(
		stream_name, stream_uuid, timestamp, current_state, requested_state);

	CU_ASSERT(fault_report_count == report_cnt + 1);
}

static void test_cam_fault_stream_logical(void)
{
	unsigned int report_cnt = fault_report_count;
	struct timespec tp;
	char *stream_name = "CAM STREAM 0001";
	char *uuid_str = "84085ddc-bc10-11ed-9a44-7ef9696e0001";
	uint16_t event_id_received = 1;
	uint16_t event_id_expected = 2;
	uint64_t timestamp;
	cam_uuid_t stream_uuid;
	int ret;

	ret = cam_uuid_from_string(uuid_str, stream_uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	CU_ASSERT(clock_gettime(CLOCK_REALTIME, &tp) == 0);
	timestamp = (((uint64_t)tp.tv_sec) * 1000000) + (tp.tv_nsec / 1000);

	cam_fault_register(cam_fault_report);
	cam_fault_stream_logical(
		stream_name,
		stream_uuid,
		timestamp,
		event_id_received,
		event_id_expected);

	CU_ASSERT(fault_report_count == report_cnt + 1);
}

static void test_cam_fault_stream_temporal(void)
{
	unsigned int report_cnt = fault_report_count;
	struct timespec tp;
	char *stream_name = "CAM STREAM 0001";
	char *uuid_str = "84085ddc-bc10-11ed-9a44-7ef9696e0001";
	uint16_t event_id = 1;
	cam_uuid_t stream_uuid;
	uint64_t time_received;
	uint64_t time_expected;
	int ret;

	ret = cam_uuid_from_string(uuid_str, stream_uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	CU_ASSERT(clock_gettime(CLOCK_REALTIME, &tp) == 0);
	time_expected = (((uint64_t)tp.tv_sec) * 1000000) + (tp.tv_nsec / 1000);
	time_received = time_expected - 1;

	cam_fault_register(cam_fault_report);
	cam_fault_stream_temporal(
		stream_name, stream_uuid, event_id, time_received, time_expected);

	CU_ASSERT(fault_report_count == report_cnt + 1);
}

static void test_cam_fault_service(void)
{
	unsigned int report_cnt = fault_report_count;
	enum cam_fault_id fault_id = CAM_FAULT_SOCKET_INIT_ERROR;

	cam_fault_register(cam_fault_report);
	cam_fault_service(fault_id);

	CU_ASSERT(fault_report_count == report_cnt + 1);
}

static int test_fault_suite_clean(void)
{
	cam_log_end();
	return 0;
}

static CU_TestInfo tests_cam_fault[] = {
	{ "cam fault register", test_cam_fault_register },
	{ "cam fault stream sequence error", test_cam_fault_stream_sequence },
	{ "cam fault stream state timeout error",
	  test_cam_fault_stream_state_timeout },
	{ "cam fault stream state error", test_cam_fault_state },
	{ "cam fault stream logical error", test_cam_fault_stream_logical },
	{ "cam fault stream temporal error", test_cam_fault_stream_temporal },
	{ "cam fault service error", test_cam_fault_service },
	CU_TEST_INFO_NULL,
};

static CU_SuiteInfo suites[] = {
	{ "fault",
	  test_fault_suite_init,
	  test_fault_suite_clean,
	  NULL,
	  NULL,
	  tests_cam_fault },
	CU_SUITE_INFO_NULL,
};

int main(int argc, char *argv[])
{
	(void)argc;
	(void)argv;

	return test_suite_run(suites);
}
