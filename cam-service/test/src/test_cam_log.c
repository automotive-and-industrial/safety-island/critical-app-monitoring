/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "test_suite.h"

#include <CUnit/Basic.h>

#include <cam_log.h>
#include <cam_service.h>

#include <stdlib.h>

#define TEST_LOG_STRING_MAX_LEN 64

static const char *test_str = "test string";

static void test_log_stdout(void)
{
	int ret;

	ret = cam_log_init(CAM_LOG_LEVEL_DEBUG + 1, NULL);
	CU_ASSERT(ret == CAM_SERVICE_ERROR);

	ret = cam_log_init(CAM_LOG_LEVEL_DEBUG, NULL);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);

	cam_log(CAM_LOG_LEVEL_DEBUG, "Test log stdout: %s. \n", test_str);

	cam_log_end();
}

static void test_log_file(void)
{
	int ret;
	const char *file_name = "test_log.txt";
	char log_str[TEST_LOG_STRING_MAX_LEN];
	char *str;
	FILE *file_ptr;

	(void)remove(file_name);

	/* CAM_LOG_LEVEL_NONE */
	ret = cam_log_init(CAM_LOG_LEVEL_NONE, file_name);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
	cam_log(CAM_LOG_LEVEL_DEBUG, "%s", test_str);
	cam_log_end();
	file_ptr = fopen(file_name, "r");
	CU_ASSERT(file_ptr == NULL);
	if (file_ptr != NULL) {
		fclose(file_ptr);
	}

	/* Lower than the configured log level */
	ret = cam_log_init(CAM_LOG_LEVEL_INFO, file_name);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
	cam_log(CAM_LOG_LEVEL_DEBUG, "%s", test_str);
	cam_log_end();
	file_ptr = fopen(file_name, "r");
	CU_ASSERT(file_ptr != NULL);
	str = fgets(log_str, strlen(test_str) + 1, file_ptr);
	CU_ASSERT(str == NULL);
	fclose(file_ptr);

	/* Valid parameters */
	ret = cam_log_init(CAM_LOG_LEVEL_INFO, file_name);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
	cam_log(CAM_LOG_LEVEL_INFO, "%s", test_str);
	cam_log_end();

	file_ptr = fopen(file_name, "r");
	CU_ASSERT(file_ptr != NULL);
	str = fgets(log_str, strlen(test_str) + 1, file_ptr);
	CU_ASSERT(str == log_str);
	ret = strcmp(test_str, log_str);
	CU_ASSERT(ret == 0);
	fclose(file_ptr);
}

static CU_TestInfo tests_log[] = {
	{ "output to stdout", test_log_stdout },
	{ "output to file", test_log_file },
	CU_TEST_INFO_NULL,
};

static CU_SuiteInfo suites[] = {
	{ "log", NULL, NULL, NULL, NULL, tests_log },
	CU_SUITE_INFO_NULL,
};

int main(int argc, char *argv[])
{
	(void)argc;
	(void)argv;

	return test_suite_run(suites);
}
