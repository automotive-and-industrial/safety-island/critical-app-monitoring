/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2024 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef POSIX_CALL_UTILS_H
#define POSIX_CALL_UTILS_H

#include <CUnit/Basic.h>

/*
 * A utility macro to test any error code set by a posix compliant function
 * dealing with sockets through errno.
 *  - out: variable where the call result will be stored
 *  - fn:  the name of the function to be called
 *  - ...: variadic arguments passed to the function to be called
 */
#define POSIX_SOCKET_CALL(out, fn, ...) \
	do { \
		out = fn(__VA_ARGS__); \
		if ((out < 0) && (errno != 0)) { \
			fprintf( \
				stderr, \
				"Line %u, posix call '" #fn "' failed with errno %d: %s\n", \
				(unsigned int)__LINE__, \
				errno, \
				strerror(errno)); \
		} \
	} while (0);

#endif /* POSIX_CALL_UTILS_H */
