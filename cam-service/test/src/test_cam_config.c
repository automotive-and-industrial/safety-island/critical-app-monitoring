/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#define _GNU_SOURCE

#include "test_suite.h"

#include <CUnit/Basic.h>
#include <unistd.h>

#include <cam_config.h>
#include <cam_log.h>
#include <cam_service.h>
#include <cam_uuid.h>

#include <stdarg.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/stat.h>

enum test_cfg_case {
	TEST_CFG_CASE_VALID,
	TEST_CFG_CASE_INVALID_STATE_START2EVENT,
	TEST_CFG_CASE_INVALID_SIGNATURE,
	TEST_CFG_CASE_INVALID_VERSION,
	TEST_CFG_CASE_INVALID_EVENT_TO,
	TEST_CFG_CASE_INVALID_EVENT_ID,
	TEST_CFG_CASE_INVALID_EVENT_MODE,
	TEST_CFG_CASE_COUNT,
};

/* Expected configuration in valid case */
#define TEST_CAM_CFG_EVENT_CNT      3
#define TEST_CAM_CFG_INIT_TIMEOUT   1000000
#define TEST_CAM_CFG_START_TIMEOUT  2000000
#define TEST_CAM_CFG_EVENT0_TIMEOUT 3000000
#define TEST_CAM_CFG_EVENT1_TIMEOUT 4000000
#define TEST_CAM_CFG_EVENT2_TIMEOUT 5000000

static const struct cam_cfg_stream exp_cfg = {
	.timeout_init_to_start = TEST_CAM_CFG_INIT_TIMEOUT,
	.timeout_start_to_event = TEST_CAM_CFG_START_TIMEOUT,
	.event_count = TEST_CAM_CFG_EVENT_CNT,
	.event[0] = {
		.timeout = TEST_CAM_CFG_EVENT0_TIMEOUT,
		.mode = CAM_CFG_EVENT_MODE_ONDEMAND_FROM_ARRIVAL,
	},
	.event[1] = {
		.timeout = TEST_CAM_CFG_EVENT1_TIMEOUT,
		.mode = CAM_CFG_EVENT_MODE_ONDEMAND_FROM_ARRIVAL,
	},
	.event[2] = {
		.timeout = TEST_CAM_CFG_EVENT2_TIMEOUT,
		.mode = CAM_CFG_EVENT_MODE_ONDEMAND_FROM_ARRIVAL,
	},
};

static const char *cfg_uuid[TEST_CFG_CASE_COUNT] = {
	[TEST_CFG_CASE_VALID] = "84085ddc-bc10-11ed-9a44-7ef9696e9dd3",
	[TEST_CFG_CASE_INVALID_SIGNATURE] = "84085ddc-bc10-11ed-9a44-7ef9696e9dd4",
	[TEST_CFG_CASE_INVALID_STATE_START2EVENT] =
		"84085ddc-bc10-11ed-9a44-7ef9696e9dd5",
	[TEST_CFG_CASE_INVALID_VERSION] = "84085ddc-bc10-11ed-9a44-7ef9696e9dd6",
	[TEST_CFG_CASE_INVALID_EVENT_TO] = "84085ddc-bc10-11ed-9a44-7ef9696e9dd7",
	[TEST_CFG_CASE_INVALID_EVENT_ID] = "84085ddc-bc10-11ed-9a44-7ef9696e9dd8",
	[TEST_CFG_CASE_INVALID_EVENT_MODE] = "84085ddc-bc10-11ed-9a44-7ef9696e9dd9",
};

#define CAM_CFG_FILE_SIZE 1024

static const char *cfg_store_dir = "./cfg_store_dir";

void cam_log(enum cam_log_level level, const char *format, ...)
{
	(void)level;

	va_list args;
	va_start(args, format);
	vfprintf(stdout, format, args);
	va_end(args);
}

static void test_cfg_load(void)
{
	int ret;
	cam_uuid_t uuid;
	struct cam_cfg_stream cfg;

	ret = cam_uuid_from_string(cfg_uuid[TEST_CFG_CASE_VALID], uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	ret = cam_cfg_stream_load(uuid, &cfg);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);

	CU_ASSERT(exp_cfg.timeout_init_to_start == cfg.timeout_init_to_start);
	CU_ASSERT(exp_cfg.timeout_start_to_event == cfg.timeout_start_to_event);
	CU_ASSERT(exp_cfg.event_count == cfg.event_count);

	if (exp_cfg.event_count == cfg.event_count) {
		for (int i = 0; i < exp_cfg.event_count; i++) {
			CU_ASSERT(exp_cfg.event[i].mode == cfg.event[i].mode);
			CU_ASSERT(exp_cfg.event[i].timeout == cfg.event[i].timeout);
		}
	}

	ret = cam_cfg_stream_load(NULL, &cfg);
	CU_ASSERT(ret == CAM_SERVICE_ERROR);

	ret = cam_cfg_stream_load(uuid, NULL);
	CU_ASSERT(ret == CAM_SERVICE_ERROR);
}

static void test_cfg_load_invalid(void)
{
	int ret;
	cam_uuid_t uuid;
	struct cam_cfg_stream cfg;
	unsigned int i;

	ret = cam_uuid_from_string(cfg_uuid[TEST_CFG_CASE_INVALID_SIGNATURE], uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	ret = cam_cfg_stream_load(uuid, &cfg);
	CU_ASSERT(ret == CAM_SERVICE_APP_ERROR);

	ret = cam_uuid_from_string(
		cfg_uuid[TEST_CFG_CASE_INVALID_STATE_START2EVENT], uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);
	ret = cam_cfg_stream_load(uuid, &cfg);
	CU_ASSERT(ret == CAM_SERVICE_APP_ERROR);

	ret = cam_uuid_from_string(cfg_uuid[TEST_CFG_CASE_INVALID_VERSION], uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	ret = cam_cfg_stream_load(uuid, &cfg);
	CU_ASSERT(ret == CAM_SERVICE_APP_ERROR);

	ret = cam_uuid_from_string(cfg_uuid[TEST_CFG_CASE_INVALID_EVENT_TO], uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	ret = cam_cfg_stream_load(uuid, &cfg);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
	CU_ASSERT(exp_cfg.event_count == cfg.event_count);
	for (i = 0; i < exp_cfg.event_count; i++) {
		CU_ASSERT(exp_cfg.event[i].timeout != cfg.event[i].timeout);
	}

	ret = cam_uuid_from_string(cfg_uuid[TEST_CFG_CASE_INVALID_EVENT_ID], uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	ret = cam_cfg_stream_load(uuid, &cfg);
	CU_ASSERT(ret == CAM_SERVICE_APP_ERROR);

	ret =
		cam_uuid_from_string(cfg_uuid[TEST_CFG_CASE_INVALID_EVENT_MODE], uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	ret = cam_cfg_stream_load(uuid, &cfg);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
	CU_ASSERT(exp_cfg.event_count == cfg.event_count);
	for (i = 0; i < exp_cfg.event_count; i++) {
		CU_ASSERT(exp_cfg.event[i].mode != cfg.event[i].mode);
	}
}

static void test_cfg_path(void)
{
	int ret;
	cam_uuid_t uuid;
	struct cam_cfg_stream cfg;

	ret = cam_cfg_init(get_current_dir_name());
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);

	ret = cam_uuid_from_string(cfg_uuid[TEST_CFG_CASE_VALID], uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	ret = cam_cfg_stream_load(uuid, &cfg);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);

	ret = cam_cfg_init("./");
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);

	ret = cam_uuid_from_string(cfg_uuid[TEST_CFG_CASE_VALID], uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	ret = cam_cfg_stream_load(uuid, &cfg);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
}

static void test_cfg_store(void)
{
	int ret;
	cam_uuid_t uuid;
	struct cam_cfg_store_ctx ctx;
	struct cam_cfg_stream cfg;
	char file_name[PATH_MAX];
	char tmp_file_name[PATH_MAX];
	char chunk[1024];
	FILE *ptr;
	unsigned int file_size;

	ret = cam_uuid_from_string(cfg_uuid[TEST_CFG_CASE_VALID], uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	/* Create config directory */
	mkdir(cfg_store_dir, S_IRWXU);
	ret = cam_cfg_init(cfg_store_dir);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);

	/* Invalid parameters */
	ret = cam_cfg_stream_store_init(NULL, false, CAM_CFG_FILE_SIZE, &ctx);
	CU_ASSERT(ret == CAM_SERVICE_ERROR);

	ret = cam_cfg_stream_store_init(uuid, false, CAM_CFG_FILE_SIZE, NULL);
	CU_ASSERT(ret == CAM_SERVICE_ERROR);

	ret = cam_cfg_stream_store_init(uuid, false, 0, &ctx);
	CU_ASSERT(ret == CAM_SERVICE_APP_ERROR);
	CU_ASSERT(ctx.status == CAM_MSG_S2A_DEPLOY_STATUS_INVALID_FILE_SIZE);

	/* Create csd file under the config directory */
	ret = snprintf(
		file_name,
		PATH_MAX,
		"%s/%s%s",
		cfg_store_dir,
		cfg_uuid[TEST_CFG_CASE_VALID],
		CAM_CFG_FILE_EXTENSION);
	CU_ASSERT(ret >= 0);

	ptr = fopen(file_name, "wb");
	CU_ASSERT(ptr != NULL);

	ret = fclose(ptr);
	CU_ASSERT(ret == 0);

	/* Overwrite is not allowed */
	ret = cam_cfg_stream_store_init(uuid, false, CAM_CFG_FILE_SIZE, &ctx);
	CU_ASSERT(ret == CAM_SERVICE_APP_ERROR);
	CU_ASSERT(ctx.status == CAM_MSG_S2A_DEPLOY_STATUS_DEPLOY_FILE_EXIST);

	/* Create temporary csd file under the config directory */
	ret = snprintf(
		tmp_file_name, PATH_MAX, "%s%s", file_name, CAM_CFG_TMP_FILE_EXTENSION);
	CU_ASSERT(ret >= 0);

	ptr = fopen(tmp_file_name, "wb");
	CU_ASSERT(ptr != NULL);

	ret = fclose(ptr);
	CU_ASSERT(ret == 0);

	/* Temporary csd file existed */
	ret = cam_cfg_stream_store_init(uuid, true, CAM_CFG_FILE_SIZE, &ctx);
	CU_ASSERT(ret == CAM_SERVICE_APP_ERROR);
	CU_ASSERT(
		ctx.status == CAM_MSG_S2A_DEPLOY_STATUS_EXCEED_MAX_DEPLOY_SESSION);

	/* Delete temporary config file */
	ret = remove(ctx.tmp_file_name);
	CU_ASSERT(ret == 0);

	/* Valid store */
	ret = snprintf(
		file_name,
		PATH_MAX,
		"%s%s",
		cfg_uuid[TEST_CFG_CASE_VALID],
		CAM_CFG_FILE_EXTENSION);
	CU_ASSERT(ret >= 0);
	ptr = fopen(file_name, "rb");
	CU_ASSERT(ptr != NULL);

	ret = fseek(ptr, 0, SEEK_END);
	CU_ASSERT(ret == 0);

	file_size = ftell(ptr);
	CU_ASSERT(file_size > 0);

	/* Overwrite is allowed */
	ret = cam_cfg_stream_store_init(uuid, true, file_size, &ctx);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);

	rewind(ptr);

	while (1) {
		size_t read_size;
		read_size = fread(chunk, 1, sizeof(chunk), ptr);
		if (read_size != sizeof(chunk)) {
			CU_ASSERT(ferror(ptr) == 0);
		}
		ret = cam_cfg_stream_store_append(&ctx, chunk, read_size);
		CU_ASSERT(ret == CAM_SERVICE_SUCCESS);

		if (feof(ptr) != 0) {
			break;
		}
	}

	ret = fclose(ptr);
	CU_ASSERT(ret == 0);

	/* Invalid parameter */
	ret = cam_cfg_stream_store_end(NULL);
	CU_ASSERT(ret == CAM_SERVICE_ERROR);

	ret = cam_cfg_stream_store_end(&ctx);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);

	/* Check the stored configuration */
	ret = cam_cfg_stream_load(uuid, &cfg);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);

	CU_ASSERT(exp_cfg.timeout_init_to_start == cfg.timeout_init_to_start);
	CU_ASSERT(exp_cfg.timeout_start_to_event == cfg.timeout_start_to_event);
	CU_ASSERT(exp_cfg.event_count == cfg.event_count);

	if (exp_cfg.event_count == cfg.event_count) {
		for (int i = 0; i < exp_cfg.event_count; i++) {
			CU_ASSERT(exp_cfg.event[i].mode == cfg.event[i].mode);
			CU_ASSERT(exp_cfg.event[i].timeout == cfg.event[i].timeout);
		}
	}

	ret = remove(ctx.file_name);
	CU_ASSERT(ret == 0);

	/* Store failure */
	ret = cam_cfg_stream_store_init(uuid, true, file_size, &ctx);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);

	ptr = fopen(ctx.tmp_file_name, "rb");
	CU_ASSERT(ptr != NULL);
	if (ptr != NULL) {
		fclose(ptr);
	}

	ret = cam_cfg_stream_store_fail(&ctx);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);

	ptr = fopen(ctx.tmp_file_name, "rb");
	CU_ASSERT(ptr == NULL);
	if (ptr != NULL) {
		fclose(ptr);
	}
}

static CU_TestInfo tests_config[] = {
	{ "load valid config", test_cfg_load },
	{ "load invalid config", test_cfg_load_invalid },
	{ "config path", test_cfg_path },
	{ "store config", test_cfg_store },
	CU_TEST_INFO_NULL,
};

static CU_SuiteInfo suites[] = {
	{ "config", NULL, NULL, NULL, NULL, tests_config },
	CU_SUITE_INFO_NULL,
};

int main(int argc, char *argv[])
{
	(void)argc;
	(void)argv;

	return test_suite_run(suites);
}
