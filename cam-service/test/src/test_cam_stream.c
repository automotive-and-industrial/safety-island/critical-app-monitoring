/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "posix_call_utils.h"
#include "test_suite.h"

#include <CUnit/Basic.h>
#include <arpa/inet.h>
#include <cam_stream.c>
#include <pthread.h>
#include <unistd.h>

#include <cam_config.h>
#include <cam_message.h>
#include <cam_service.h>
#include <cam_socket.h>

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>

#define TEST_STREAM_DEFAULT_PORT     21606
#define TEST_STREAM_IP_ADDR          "127.0.0.1"
#define TEST_STREAM_UUID_STR         "84085ddc-bc10-11ed-9a44-7ef9696e9dd3"
#define TEST_STREAM_INVALID_UUID_STR "84085ddc-bc10-11ed-9a44-7ef9696e9dd2"

#define TEST_STREAM_DEPLOY_CHUNK_SIZE 512
#define TEST_STREAM_DEPLOY_MSG_BUF    3000

struct test_cam_stream_ctx {
	const char *address;
	unsigned int port;
	int server_fd;
	int client_fd;
	const char *uuid_str;
	char src_file_name[PATH_MAX];
	char dst_file_name[PATH_MAX];
};

static struct test_cam_stream_ctx test_ctx = {
	.address = TEST_STREAM_IP_ADDR,
	.port = TEST_STREAM_DEFAULT_PORT,
	.uuid_str = TEST_STREAM_UUID_STR,
};

static const char *csd_dir = "./stream_deploy_dir";

void cam_log(enum cam_log_level level, const char *format, ...)
{
	(void)level;

	va_list args;
	va_start(args, format);
	vfprintf(stdout, format, args);
	va_end(args);
}

static void *test_server_thread(void *arg)
{
	(void)arg;
	struct sockaddr_in peer_addr = { 0 };
	socklen_t peer_addr_size = sizeof(peer_addr);
	int peer_fd;

	peer_fd = accept(
		test_ctx.server_fd, (struct sockaddr *)&peer_addr, &peer_addr_size);

	if (peer_fd == -1) {
		perror("Error accepting connection");
		return NULL;
	}

	/* Run stream handler */
	cam_stream_process(peer_fd);

	return NULL;
}

static int test_client_create(void)
{
	int ret;
	struct timeval tv;
	struct sockaddr_in addr;
	pthread_t tid;

	/* Create a thread because accept() blocks the caller */
	ret = pthread_create(&tid, NULL, test_server_thread, NULL);
	if (ret != 0) {
		perror("Error creating server thread");
		close(test_ctx.server_fd);
		return -1;
	}

	/* Wait 10ms for the server thread to initialize */
	usleep(10000);

	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr(test_ctx.address);
	addr.sin_port = htons(test_ctx.port);

	test_ctx.client_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (test_ctx.client_fd == -1) {
		perror("Error creating client socket");
		close(test_ctx.server_fd);
		return -1;
	}

	tv.tv_sec = 10;
	tv.tv_usec = 0;
	ret = setsockopt(
		test_ctx.client_fd,
		SOL_SOCKET,
		SO_RCVTIMEO,
		(const char *)&tv,
		sizeof tv);
	if (ret == -1) {
		perror("Error set client socket");
		close(test_ctx.server_fd);
		close(test_ctx.client_fd);
		return -1;
	}

	POSIX_SOCKET_CALL(
		ret,
		connect,
		test_ctx.client_fd,
		(struct sockaddr *)&addr,
		sizeof(addr));
	if (ret == -1) {
		perror("Error connecting socket");
		close(test_ctx.server_fd);
		close(test_ctx.client_fd);
		return -1;
	}

	usleep(10000);

	return 0;
}

static int test_client_close(void)
{
	int ret;

	/* Close the client connection */
	ret = close(test_ctx.client_fd);
	CU_ASSERT(ret == 0);

	/* Wait 10ms for service to close the connection */
	usleep(10000);

	return ret;
}

static int test_stream_suite_init(void)
{
	struct sockaddr_in addr;

	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr(test_ctx.address);
	addr.sin_port = htons(test_ctx.port);

	/* Server socket */
	test_ctx.server_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (test_ctx.server_fd == -1) {
		perror("Error creating server socket");
		return -1;
	}

	if (bind(test_ctx.server_fd, (struct sockaddr *)&addr, sizeof(addr)) ==
		-1) {
		perror("Error binding server socket");
		close(test_ctx.server_fd);
		return -1;
	}

	if (listen(test_ctx.server_fd, 1) == -1) {
		perror("Error listening to server socket");
		close(test_ctx.server_fd);
		return -1;
	}

	/* Initialize the store context */
	if (snprintf(
			test_ctx.src_file_name,
			PATH_MAX,
			"%s%s",
			test_ctx.uuid_str,
			CAM_CFG_FILE_EXTENSION) < 0) {
		fprintf(stderr, "Failed to format src file name.\n");
		return -1;
	}

	if (snprintf(
			test_ctx.dst_file_name,
			PATH_MAX,
			"%s/%s",
			csd_dir,
			test_ctx.src_file_name) < 0) {
		fprintf(stderr, "Failed to format dst file name.\n");
		return -1;
	}

	return 0;
}

static int test_stream_suite_clean(void)
{
	close(test_ctx.server_fd);
	return 0;
}

static void test_stream_msg_recv(int fd, uint8_t *buffer, uint16_t size)
{
	unsigned int bytes_recv = 0u;

	CU_ASSERT_FATAL(buffer != NULL);

	while (bytes_recv < size) {
		ssize_t count;
		POSIX_SOCKET_CALL(
			count, recv, fd, buffer + bytes_recv, size - bytes_recv, 0);
		CU_ASSERT_FATAL(count != -1);
		/* Connection is closed */
		CU_ASSERT_FATAL(count != 0);
		CU_ASSERT_FATAL((bytes_recv + count) <= size);
		bytes_recv += count;
	}
}

static void test_stream_msg_send(int fd, uint8_t *buffer, uint16_t size)
{
	unsigned int bytes_sent = 0u;

	CU_ASSERT_FATAL(buffer != NULL);

	/* Check if the complete message is sent */
	while (bytes_sent < size) {
		ssize_t count;
		POSIX_SOCKET_CALL(
			count, send, fd, buffer + bytes_sent, size - bytes_sent, 0);
		CU_ASSERT_FATAL(count != -1);
		CU_ASSERT_FATAL((bytes_sent + count) <= size);

		bytes_sent += count;
	}
}

static void test_stream_init_msg(void)
{
	int i;
	int ret;

	struct cam_msg_a2s_init send_msg;
	struct cam_msg_s2a_init recv_msg;
	uint8_t *p_send = (uint8_t *)&send_msg;
	uint8_t *p_recv = (uint8_t *)&recv_msg;
	struct cam_msg_header *header;
	uint64_t timestamp;

	ret = test_client_create();
	CU_ASSERT(ret == 0);

	/* No configuration */
	header = &send_msg.header;
	header->version_major = CAM_MSG_VERSION_MAJOR;
	header->version_minor = CAM_MSG_VERSION_MINOR;
	header->msg_id = htons(CAM_MSG_A2S_ID_INIT);
	header->size = htons(sizeof(struct cam_msg_a2s_init));

	ret = cam_uuid_from_string(TEST_STREAM_INVALID_UUID_STR, send_msg.uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	ret = stream_timestamp_now(&timestamp);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
	send_msg.timestamp = htonll(timestamp);

	test_stream_msg_send(test_ctx.client_fd, p_send, sizeof(send_msg));
	test_stream_msg_recv(test_ctx.client_fd, p_recv, sizeof(recv_msg));
	header = &recv_msg.header;
	CU_ASSERT(header->version_major == CAM_MSG_VERSION_MAJOR);
	CU_ASSERT(header->version_minor == CAM_MSG_VERSION_MINOR);
	CU_ASSERT(header->size == ntohs(sizeof(struct cam_msg_s2a_init)));
	CU_ASSERT(ntohs(recv_msg.status) == CAM_MSG_S2A_INIT_STATUS_NO_CONF);

	/* Valid configuration */
	for (i = 0; i < CONFIG_CAM_SERVICE_MAX_STREAM_PER_CONN; i++) {
		header = &send_msg.header;
		header->version_major = CAM_MSG_VERSION_MAJOR;
		header->version_minor = CAM_MSG_VERSION_MINOR;
		header->msg_id = htons(CAM_MSG_A2S_ID_INIT);
		header->size = htons(sizeof(struct cam_msg_a2s_init));

		ret = cam_uuid_from_string(test_ctx.uuid_str, send_msg.uuid);
		CU_ASSERT(ret == CAM_UUID_SUCCESS);

		test_stream_msg_send(test_ctx.client_fd, p_send, sizeof(send_msg));
		test_stream_msg_recv(test_ctx.client_fd, p_recv, sizeof(recv_msg));

		header = &recv_msg.header;
		CU_ASSERT(header->version_major == CAM_MSG_VERSION_MAJOR);
		CU_ASSERT(header->version_minor == CAM_MSG_VERSION_MINOR);
		CU_ASSERT(header->size == ntohs(sizeof(struct cam_msg_s2a_init)));
		CU_ASSERT(ntohs(recv_msg.status) == CAM_MSG_S2A_INIT_STATUS_SUCCESS);
		ret = cam_uuid_compare(recv_msg.uuid, send_msg.uuid);
		CU_ASSERT(ret == 0);
		CU_ASSERT(
			ntohl(recv_msg.handler_id) <
			CONFIG_CAM_SERVICE_MAX_STREAM_PER_CONN);
	}

	/* Out of resource */
	header = &send_msg.header;
	header->version_major = CAM_MSG_VERSION_MAJOR;
	header->version_minor = CAM_MSG_VERSION_MINOR;
	header->msg_id = htons(CAM_MSG_A2S_ID_INIT);
	header->size = htons(sizeof(struct cam_msg_a2s_init));

	ret = cam_uuid_from_string(test_ctx.uuid_str, send_msg.uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	test_stream_msg_send(test_ctx.client_fd, p_send, sizeof(send_msg));
	test_stream_msg_recv(test_ctx.client_fd, p_recv, sizeof(recv_msg));

	header = &recv_msg.header;
	CU_ASSERT(header->version_major == CAM_MSG_VERSION_MAJOR);
	CU_ASSERT(header->version_minor == CAM_MSG_VERSION_MINOR);
	CU_ASSERT(header->size == ntohs(sizeof(struct cam_msg_s2a_init)));
	CU_ASSERT(ntohs(recv_msg.status) == CAM_MSG_S2A_INIT_STATUS_OUT_OF_RES);

	/* Close the client connection */
	ret = test_client_close();
	CU_ASSERT(ret == 0);
}

static bool compare_file(FILE *fp1, FILE *fp2)
{
	bool result = true;

	rewind(fp1);
	rewind(fp2);

	while (1) {
		int c1, c2;

		c1 = fgetc(fp1);
		c2 = fgetc(fp2);

		if (c1 != c2) {
			result = false;
			break;
		}

		if (c1 == EOF || c2 == EOF) {
			break;
		}
	}

	return result;
}

static void test_stream_deploy_msg(void)
{
	int ret;
	uint8_t send_buff[TEST_STREAM_DEPLOY_MSG_BUF];
	uint8_t recv_buff[TEST_STREAM_DEPLOY_MSG_BUF];
	struct cam_msg_a2s_deploy *send_msg =
		(struct cam_msg_a2s_deploy *)send_buff;
	struct cam_msg_s2a_deploy *recv_msg =
		(struct cam_msg_s2a_deploy *)recv_buff;
	struct cam_msg_header *header;
	uint64_t timestamp;
	FILE *src_ptr;
	FILE *dst_ptr;
	unsigned int file_size;

	/* Create directory for storing csd file */
	mkdir(csd_dir, S_IRWXU);
	ret = cam_cfg_init(csd_dir);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);

	ret = test_client_create();
	CU_ASSERT(ret == 0);

	header = &send_msg->header;
	header->version_major = CAM_MSG_VERSION_MAJOR;
	header->version_minor = CAM_MSG_VERSION_MINOR;
	header->msg_id = htons(CAM_MSG_A2S_ID_DEPLOY);

	ret = stream_timestamp_now(&timestamp);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
	send_msg->timestamp = htonll(timestamp);

	ret = cam_uuid_from_string(test_ctx.uuid_str, send_msg->uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	/* Calculate the size of csd file */
	src_ptr = fopen(test_ctx.src_file_name, "rb");
	CU_ASSERT(src_ptr != NULL);
	ret = fseek(src_ptr, 0, SEEK_END);
	CU_ASSERT(ret == 0);

	file_size = ftell(src_ptr);
	CU_ASSERT(file_size > 0);
	rewind(src_ptr);

	send_msg->file_size = htons(file_size);
	send_msg->chunk_id = 0;

	while (1) {
		size_t read_size;
		read_size = fread(
			send_msg->chunk_data, 1, TEST_STREAM_DEPLOY_CHUNK_SIZE, src_ptr);
		if (read_size != TEST_STREAM_DEPLOY_CHUNK_SIZE) {
			CU_ASSERT(ferror(src_ptr) == 0);
		}

		send_msg->attr = htons(CAM_MSG_DEPLOY_ATTR_OVERWRITE);

		send_msg->chunk_size = htons(read_size);
		send_msg->chunk_id = 0;
		header->size = htons(sizeof(struct cam_msg_a2s_deploy) + read_size);

		test_stream_msg_send(
			test_ctx.client_fd,
			send_buff,
			sizeof(struct cam_msg_a2s_deploy) + read_size);
		test_stream_msg_recv(
			test_ctx.client_fd, recv_buff, sizeof(struct cam_msg_s2a_deploy));

		header = &recv_msg->header;
		CU_ASSERT(header->version_major == CAM_MSG_VERSION_MAJOR);
		CU_ASSERT(header->version_minor == CAM_MSG_VERSION_MINOR);
		CU_ASSERT(header->size == ntohs(sizeof(struct cam_msg_s2a_deploy)));
		CU_ASSERT(recv_msg->status == CAM_MSG_S2A_DEPLOY_STATUS_SUCCESS);
		ret = cam_uuid_compare(recv_msg->uuid, send_msg->uuid);
		CU_ASSERT(ret == 0);

		if (feof(src_ptr) != 0) {
			break;
		}
	}

	dst_ptr = fopen(test_ctx.dst_file_name, "rb");
	CU_ASSERT(dst_ptr != NULL);

	bool result = compare_file(src_ptr, dst_ptr);
	CU_ASSERT(result == true);

	ret = fclose(src_ptr);
	CU_ASSERT(ret == 0);

	ret = fclose(dst_ptr);
	CU_ASSERT(ret == 0);

	/* Close the client connection */
	ret = test_client_close();
	CU_ASSERT(ret == 0);
}

static void test_stream_invalid_deploy_msg(void)
{
	int ret;
	uint8_t send_buff[TEST_STREAM_DEPLOY_MSG_BUF];
	uint8_t recv_buff[TEST_STREAM_DEPLOY_MSG_BUF];
	struct cam_msg_a2s_deploy *send_msg =
		(struct cam_msg_a2s_deploy *)send_buff;
	struct cam_msg_s2a_deploy *recv_msg =
		(struct cam_msg_s2a_deploy *)recv_buff;
	struct cam_msg_header *header;
	uint64_t timestamp;
	FILE *src_ptr, *tmp_ptr;
	char tmp_file_name[PATH_MAX];
	unsigned int file_size;

	ret = test_client_create();
	CU_ASSERT(ret == 0);

	send_msg->header.version_major = CAM_MSG_VERSION_MAJOR;
	send_msg->header.version_minor = CAM_MSG_VERSION_MINOR;
	send_msg->header.msg_id = htons(CAM_MSG_A2S_ID_DEPLOY);

	ret = cam_uuid_from_string(test_ctx.uuid_str, send_msg->uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	ret = stream_timestamp_now(&timestamp);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
	send_msg->timestamp = htonll(timestamp);

	src_ptr = fopen(test_ctx.src_file_name, "rb");
	CU_ASSERT(src_ptr != NULL);
	ret = fseek(src_ptr, 0, SEEK_END);
	CU_ASSERT(ret == 0);

	file_size = ftell(src_ptr);
	CU_ASSERT(file_size > 0);

	ret = fclose(src_ptr);
	CU_ASSERT(ret == 0);

	send_msg->file_size = htons(file_size);

	/* Deployment file existed */
	send_msg->attr = 0;
	send_msg->chunk_size = 0;
	send_msg->chunk_id = 0;
	send_msg->header.size = htons(sizeof(struct cam_msg_a2s_deploy));

	test_stream_msg_send(
		test_ctx.client_fd, send_buff, sizeof(struct cam_msg_a2s_deploy));
	test_stream_msg_recv(
		test_ctx.client_fd, recv_buff, sizeof(struct cam_msg_s2a_deploy));

	header = &recv_msg->header;
	CU_ASSERT(header->version_major == CAM_MSG_VERSION_MAJOR);
	CU_ASSERT(header->version_minor == CAM_MSG_VERSION_MINOR);
	CU_ASSERT(header->size == ntohs(sizeof(struct cam_msg_s2a_deploy)));
	CU_ASSERT(recv_msg->status == CAM_MSG_S2A_DEPLOY_STATUS_DEPLOY_FILE_EXIST);
	ret = cam_uuid_compare(recv_msg->uuid, send_msg->uuid);
	CU_ASSERT(ret == 0);

	ret = test_client_close();
	CU_ASSERT(ret == 0);

	ret = remove(test_ctx.dst_file_name);
	CU_ASSERT(ret == 0);

	/* Invalid chunk size */
	ret = test_client_create();
	CU_ASSERT(ret == 0);

	send_msg->attr = htons(CAM_MSG_DEPLOY_ATTR_OVERWRITE);
	send_msg->chunk_size = htons(1);
	send_msg->chunk_id = 0;
	send_msg->header.size = htons(sizeof(struct cam_msg_a2s_deploy));

	test_stream_msg_send(
		test_ctx.client_fd, send_buff, sizeof(struct cam_msg_a2s_deploy) + 1);
	test_stream_msg_recv(
		test_ctx.client_fd, recv_buff, sizeof(struct cam_msg_s2a_deploy));

	header = &recv_msg->header;
	CU_ASSERT(header->version_major == CAM_MSG_VERSION_MAJOR);
	CU_ASSERT(header->version_minor == CAM_MSG_VERSION_MINOR);
	CU_ASSERT(header->size == ntohs(sizeof(struct cam_msg_s2a_deploy)));
	CU_ASSERT(recv_msg->status == CAM_MSG_S2A_DEPLOY_STATUS_INVALID_CHUNK_SIZE);
	ret = cam_uuid_compare(recv_msg->uuid, send_msg->uuid);
	CU_ASSERT(ret == 0);

	ret = test_client_close();
	CU_ASSERT(ret == 0);

	/* Message is too long */
	ret = test_client_create();
	CU_ASSERT(ret == 0);

	send_msg->attr = htons(CAM_MSG_DEPLOY_ATTR_OVERWRITE);
	send_msg->chunk_size =
		htons(TEST_STREAM_DEPLOY_MSG_BUF - sizeof(struct cam_msg_a2s_deploy));
	send_msg->chunk_id = 0;
	send_msg->header.size = htons(TEST_STREAM_DEPLOY_MSG_BUF);

	test_stream_msg_send(
		test_ctx.client_fd, send_buff, TEST_STREAM_DEPLOY_MSG_BUF);
	test_stream_msg_recv(
		test_ctx.client_fd, recv_buff, sizeof(struct cam_msg_s2a_deploy));

	header = &recv_msg->header;
	CU_ASSERT(header->version_major == CAM_MSG_VERSION_MAJOR);
	CU_ASSERT(header->version_minor == CAM_MSG_VERSION_MINOR);
	CU_ASSERT(header->size == ntohs(sizeof(struct cam_msg_s2a_deploy)));
	CU_ASSERT(recv_msg->status == CAM_MSG_S2A_DEPLOY_STATUS_INVALID_CHUNK_SIZE);
	ret = cam_uuid_compare(recv_msg->uuid, send_msg->uuid);
	CU_ASSERT(ret == 0);

	ret = test_client_close();
	CU_ASSERT(ret == 0);

	/* Invalid chunk Id */
	ret = test_client_create();
	CU_ASSERT(ret == 0);

	send_msg->attr = htons(CAM_MSG_DEPLOY_ATTR_OVERWRITE);
	send_msg->chunk_size = 0;
	send_msg->chunk_id = 0;
	send_msg->header.size = htons(sizeof(struct cam_msg_a2s_deploy));

	test_stream_msg_send(
		test_ctx.client_fd, send_buff, sizeof(struct cam_msg_a2s_deploy));
	test_stream_msg_recv(
		test_ctx.client_fd, recv_buff, sizeof(struct cam_msg_s2a_deploy));

	CU_ASSERT(header->version_major == CAM_MSG_VERSION_MAJOR);
	CU_ASSERT(header->version_minor == CAM_MSG_VERSION_MINOR);
	CU_ASSERT(header->size == ntohs(sizeof(struct cam_msg_s2a_deploy)));
	CU_ASSERT(recv_msg->status == CAM_MSG_S2A_DEPLOY_STATUS_SUCCESS);
	ret = cam_uuid_compare(recv_msg->uuid, send_msg->uuid);
	CU_ASSERT(ret == 0);

	send_msg->attr = htons(CAM_MSG_DEPLOY_ATTR_OVERWRITE);
	;
	send_msg->chunk_size = 1;
	send_msg->chunk_id = 2;
	send_msg->header.size = htons(sizeof(struct cam_msg_a2s_deploy) + 1);

	test_stream_msg_send(
		test_ctx.client_fd, send_buff, sizeof(struct cam_msg_a2s_deploy) + 1);
	test_stream_msg_recv(
		test_ctx.client_fd, recv_buff, sizeof(struct cam_msg_s2a_deploy));

	CU_ASSERT(header->version_major == CAM_MSG_VERSION_MAJOR);
	CU_ASSERT(header->version_minor == CAM_MSG_VERSION_MINOR);
	CU_ASSERT(header->size == ntohs(sizeof(struct cam_msg_s2a_deploy)));
	CU_ASSERT(recv_msg->status == CAM_MSG_S2A_DEPLOY_STATUS_INVALID_ORDER);
	ret = cam_uuid_compare(recv_msg->uuid, send_msg->uuid);
	CU_ASSERT(ret == 0);

	ret = test_client_close();
	CU_ASSERT(ret == 0);

	/* Incomplete store session */
	ret = test_client_create();
	CU_ASSERT(ret == 0);

	send_msg->attr = htons(CAM_MSG_DEPLOY_ATTR_OVERWRITE);
	;
	send_msg->chunk_size = 0;
	send_msg->chunk_id = 0;
	send_msg->header.size = htons(sizeof(struct cam_msg_a2s_deploy));

	test_stream_msg_send(
		test_ctx.client_fd, send_buff, sizeof(struct cam_msg_a2s_deploy));
	test_stream_msg_recv(
		test_ctx.client_fd, recv_buff, sizeof(struct cam_msg_s2a_deploy));

	CU_ASSERT(header->version_major == CAM_MSG_VERSION_MAJOR);
	CU_ASSERT(header->version_minor == CAM_MSG_VERSION_MINOR);
	CU_ASSERT(header->size == ntohs(sizeof(struct cam_msg_s2a_deploy)));
	CU_ASSERT(recv_msg->status == CAM_MSG_S2A_DEPLOY_STATUS_SUCCESS);
	ret = cam_uuid_compare(recv_msg->uuid, send_msg->uuid);
	CU_ASSERT(ret == 0);

	ret = test_client_close();
	CU_ASSERT(ret == 0);

	ret = snprintf(
		tmp_file_name,
		PATH_MAX,
		"%s%s",
		test_ctx.dst_file_name,
		CAM_CFG_TMP_FILE_EXTENSION);
	CU_ASSERT(ret >= 0);

	tmp_ptr = fopen(tmp_file_name, "rb");
	if (tmp_ptr != NULL) {
		fclose(tmp_ptr);
	}
	CU_ASSERT(tmp_ptr == NULL);

	/* Previous deployment session is not done */
	ret = test_client_create();
	CU_ASSERT(ret == 0);

	send_msg->attr = htons(CAM_MSG_DEPLOY_ATTR_OVERWRITE);
	;
	send_msg->chunk_size = 0;
	send_msg->chunk_id = 0;
	send_msg->header.size = htons(sizeof(struct cam_msg_a2s_deploy));

	test_stream_msg_send(
		test_ctx.client_fd, send_buff, sizeof(struct cam_msg_a2s_deploy));
	test_stream_msg_recv(
		test_ctx.client_fd, recv_buff, sizeof(struct cam_msg_s2a_deploy));

	CU_ASSERT(header->version_major == CAM_MSG_VERSION_MAJOR);
	CU_ASSERT(header->version_minor == CAM_MSG_VERSION_MINOR);
	CU_ASSERT(header->size == ntohs(sizeof(struct cam_msg_s2a_deploy)));
	CU_ASSERT(recv_msg->status == CAM_MSG_S2A_DEPLOY_STATUS_SUCCESS);
	ret = cam_uuid_compare(recv_msg->uuid, send_msg->uuid);
	CU_ASSERT(ret == 0);

	ret = cam_uuid_from_string(TEST_STREAM_INVALID_UUID_STR, send_msg->uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	send_msg->attr = htons(CAM_MSG_DEPLOY_ATTR_OVERWRITE);
	;
	send_msg->chunk_size = 0;
	send_msg->chunk_id = 0;
	send_msg->header.size = htons(sizeof(struct cam_msg_a2s_deploy));

	test_stream_msg_send(
		test_ctx.client_fd, send_buff, sizeof(struct cam_msg_a2s_deploy));
	test_stream_msg_recv(
		test_ctx.client_fd, recv_buff, sizeof(struct cam_msg_s2a_deploy));

	CU_ASSERT(header->version_major == CAM_MSG_VERSION_MAJOR);
	CU_ASSERT(header->version_minor == CAM_MSG_VERSION_MINOR);
	CU_ASSERT(header->size == ntohs(sizeof(struct cam_msg_s2a_deploy)));
	CU_ASSERT(
		recv_msg->status ==
		CAM_MSG_S2A_DEPLOY_STATUS_EXCEED_MAX_DEPLOY_SESSION);
	ret = cam_uuid_compare(recv_msg->uuid, send_msg->uuid);
	CU_ASSERT(ret == 0);

	ret = test_client_close();
	CU_ASSERT(ret == 0);

	tmp_ptr = fopen(tmp_file_name, "rb");
	if (tmp_ptr != NULL) {
		fclose(tmp_ptr);
	}
	CU_ASSERT(tmp_ptr == NULL);
}

static void test_stream_start(void)
{
	int ret;
	int i;
	uint64_t timestamp;
	struct stream_ctx ctx[CONFIG_CAM_SERVICE_MAX_STREAM_PER_CONN];
	pthread_mutex_t pkt_handling_lock;
	struct cam_msg_a2s_start msg = {
		.header = {
			.version_major = CAM_MSG_VERSION_MAJOR,
			.version_minor = CAM_MSG_VERSION_MINOR,
			.size = htons(sizeof(struct cam_msg_a2s_start)),
			.msg_id = htons(CAM_MSG_A2S_ID_START),
		},
		.timestamp = 0,
		.handler_id = 0,
	};
	cam_uuid_t uuid = { 0 };

	ret = cam_cfg_init("./");
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);

	ret = cam_uuid_from_string(test_ctx.uuid_str, uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	ret = stream_timestamp_now(&timestamp);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
	msg.timestamp = htonll(timestamp);

	ret = pthread_mutex_init(&pkt_handling_lock, NULL);
	CU_ASSERT(ret == 0);

	for (i = 0; i < CONFIG_CAM_SERVICE_MAX_STREAM_PER_CONN; i++) {
		memset(&ctx[i], 0, sizeof(struct stream_ctx));
		ret = pthread_mutex_init(&ctx[i].ctx_lock, NULL);
		CU_ASSERT(ret == 0);
		ctx[i].pkt_handling_lock = &pkt_handling_lock;
		ret = stream_ctx_init(&ctx[i], uuid, timestamp);
		CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
	}

	for (i = 0; i < CONFIG_CAM_SERVICE_MAX_STREAM_PER_CONN; i++) {
		msg.handler_id = htonl(i);
		uint8_t *stream_msg = (uint8_t *)&msg;

		/* Invalid state */
		CU_ASSERT(pthread_mutex_lock(&pkt_handling_lock) == 0);
		ctx[i].state = CAM_STREAM_STATE_UNUSED;
		ret = stream_start(-1, ctx, stream_msg);
		CU_ASSERT(ret == CAM_SERVICE_APP_ERROR);
		CU_ASSERT(ctx[i].state == CAM_STREAM_STATE_UNUSED);

		CU_ASSERT(pthread_mutex_lock(&pkt_handling_lock) == 0);
		ctx[i].state = CAM_STREAM_STATE_IN_PROGRESS;
		ret = stream_start(-1, ctx, stream_msg);
		CU_ASSERT(ret == CAM_SERVICE_APP_ERROR);
		CU_ASSERT(ctx[i].state == CAM_STREAM_STATE_FAILED);

		CU_ASSERT(pthread_mutex_lock(&pkt_handling_lock) == 0);
		ctx[i].state = CAM_STREAM_STATE_FAILED;
		ret = stream_start(-1, ctx, stream_msg);
		CU_ASSERT(ret == CAM_SERVICE_APP_ERROR);
		CU_ASSERT(ctx[i].state == CAM_STREAM_STATE_FAILED);

		/* Invalid handler ID */
		ctx[i].state = CAM_STREAM_STATE_STOPPED;
		msg.handler_id = htonl(CONFIG_CAM_SERVICE_MAX_STREAM_PER_CONN + 1);
		stream_msg = (uint8_t *)&msg;
		/* Called with pkt_handling_lock unlocked on purpose */
		ret = stream_start(-1, ctx, stream_msg);
		CU_ASSERT(ret == CAM_SERVICE_APP_ERROR);

		/* Valid parameters */
		ret = stream_ctx_init(&ctx[i], uuid, timestamp);
		CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
		ctx[i].state = CAM_STREAM_STATE_STOPPED;
		msg.handler_id = htonl(i);
		stream_msg = (uint8_t *)&msg;
		CU_ASSERT(pthread_mutex_lock(&pkt_handling_lock) == 0);
		ret = stream_start(-1, ctx, stream_msg);
		CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
		CU_ASSERT(ctx[i].state == CAM_STREAM_STATE_IN_PROGRESS);
	}

	for (i = 0; i < CONFIG_CAM_SERVICE_MAX_STREAM_PER_CONN; i++) {
		stream_ctx_destroy(&ctx[i]);
		ret = pthread_mutex_destroy(&ctx[i].ctx_lock);
		CU_ASSERT(ret == 0);
	}
	CU_ASSERT(pthread_mutex_destroy(&pkt_handling_lock) == 0);
}

static void test_stream_stop(void)
{
	int ret;
	int i;
	uint64_t timestamp;
	struct stream_ctx ctx[CONFIG_CAM_SERVICE_MAX_STREAM_PER_CONN];
	pthread_mutex_t pkt_handling_lock;
	struct cam_msg_a2s_stop msg = {
		.header = {
			.version_major = CAM_MSG_VERSION_MAJOR,
			.version_minor = CAM_MSG_VERSION_MINOR,
			.size = htons(sizeof(struct cam_msg_a2s_stop)),
			.msg_id = htons(CAM_MSG_A2S_ID_STOP),
		},
		.timestamp = 0,
		.handler_id = 0,
	};
	cam_uuid_t uuid = { 0 };

	ret = cam_uuid_from_string(test_ctx.uuid_str, uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	ret = stream_timestamp_now(&timestamp);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
	msg.timestamp = htonll(timestamp);

	ret = pthread_mutex_init(&pkt_handling_lock, NULL);
	CU_ASSERT(ret == 0);

	for (i = 0; i < CONFIG_CAM_SERVICE_MAX_STREAM_PER_CONN; i++) {
		memset(&ctx[i], 0, sizeof(struct stream_ctx));
		ret = pthread_mutex_init(&ctx[i].ctx_lock, NULL);
		CU_ASSERT(ret == 0);
		ctx[i].pkt_handling_lock = &pkt_handling_lock;
		ret = stream_ctx_init(&ctx[i], uuid, timestamp);
		CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
	}

	for (i = 0; i < CONFIG_CAM_SERVICE_MAX_STREAM_PER_CONN; i++) {
		msg.handler_id = htonl(i);
		uint8_t *stream_msg = (uint8_t *)&msg;

		/* Invalid state */
		CU_ASSERT(pthread_mutex_lock(&pkt_handling_lock) == 0);
		ctx[i].state = CAM_STREAM_STATE_UNUSED;
		ret = stream_stop(-1, ctx, stream_msg);
		CU_ASSERT(ret == CAM_SERVICE_APP_ERROR);
		CU_ASSERT(ctx[i].state == CAM_STREAM_STATE_UNUSED);

		CU_ASSERT(pthread_mutex_lock(&pkt_handling_lock) == 0);
		ctx[i].state = CAM_STREAM_STATE_STOPPED;
		ret = stream_stop(-1, ctx, stream_msg);
		CU_ASSERT(ret == CAM_SERVICE_APP_ERROR);
		CU_ASSERT(ctx[i].state == CAM_STREAM_STATE_FAILED);

		CU_ASSERT(pthread_mutex_lock(&pkt_handling_lock) == 0);
		ctx[i].state = CAM_STREAM_STATE_FAILED;
		ret = stream_stop(-1, ctx, stream_msg);
		CU_ASSERT(ret == CAM_SERVICE_APP_ERROR);
		CU_ASSERT(ctx[i].state == CAM_STREAM_STATE_FAILED);

		/* Invalid handler ID */
		ctx[i].state = CAM_STREAM_STATE_IN_PROGRESS;
		msg.handler_id = htonl(CONFIG_CAM_SERVICE_MAX_STREAM_PER_CONN + 1);
		stream_msg = (uint8_t *)&msg;
		/* Called with pkt_handling_lock unlocked on purpose */
		ret = stream_stop(-1, ctx, stream_msg);
		CU_ASSERT(ret == CAM_SERVICE_APP_ERROR);

		/* Valid parameters */
		ret = stream_ctx_init(&ctx[i], uuid, timestamp);
		CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
		CU_ASSERT(pthread_mutex_lock(&pkt_handling_lock) == 0);
		ctx[i].state = CAM_STREAM_STATE_IN_PROGRESS;
		msg.handler_id = htonl(i);
		stream_msg = (uint8_t *)&msg;
		ret = stream_stop(-1, ctx, stream_msg);
		CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
		CU_ASSERT(ctx[i].state == CAM_STREAM_STATE_STOPPED);
	}

	for (i = 0; i < CONFIG_CAM_SERVICE_MAX_STREAM_PER_CONN; i++) {
		stream_ctx_destroy(&ctx[i]);
		ret = pthread_mutex_destroy(&ctx[i].ctx_lock);
		CU_ASSERT(ret == 0);
	}
	CU_ASSERT(pthread_mutex_destroy(&pkt_handling_lock) == 0);
}

static void test_stream_event(void)
{
	int ret;
	int i;
	uint64_t timestamp;
	struct stream_ctx ctx[CONFIG_CAM_SERVICE_MAX_STREAM_PER_CONN];
	pthread_mutex_t pkt_handling_lock;
	struct cam_msg_a2s_event msg = {
		.header = {
			.version_major = CAM_MSG_VERSION_MAJOR,
			.version_minor = CAM_MSG_VERSION_MINOR,
			.size = htons(sizeof(struct cam_msg_a2s_event)),
			.msg_id = htons(CAM_MSG_A2S_ID_EVENT),
		},
		.timestamp = 0,
		.handler_id = 0,
		.sequence_id = 0,
		.event_id = 0,
	};
	cam_uuid_t uuid = { 0 };

	ret = cam_uuid_from_string(test_ctx.uuid_str, uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	ret = stream_timestamp_now(&timestamp);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
	msg.timestamp = htonll(timestamp);

	ret = pthread_mutex_init(&pkt_handling_lock, NULL);
	CU_ASSERT(ret == 0);

	for (i = 0; i < CONFIG_CAM_SERVICE_MAX_STREAM_PER_CONN; i++) {
		memset(&ctx[i], 0, sizeof(struct stream_ctx));
		ret = pthread_mutex_init(&ctx[i].ctx_lock, NULL);
		CU_ASSERT(ret == 0);
		ctx[i].pkt_handling_lock = &pkt_handling_lock;
		ret = stream_ctx_init(&ctx[i], uuid, timestamp);
		CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
	}

	for (i = 0; i < CONFIG_CAM_SERVICE_MAX_STREAM_PER_CONN; i++) {
		msg.handler_id = htonl(i);
		uint8_t *stream_msg = (uint8_t *)&msg;

		/* Invalid state */
		CU_ASSERT(pthread_mutex_lock(&pkt_handling_lock) == 0);
		ctx[i].state = CAM_STREAM_STATE_UNUSED;
		ret = stream_event(-1, ctx, stream_msg);
		CU_ASSERT(ret == CAM_SERVICE_APP_ERROR);
		CU_ASSERT(ctx[i].state == CAM_STREAM_STATE_UNUSED);

		CU_ASSERT(pthread_mutex_lock(&pkt_handling_lock) == 0);
		ctx[i].state = CAM_STREAM_STATE_STOPPED;
		ret = stream_event(-1, ctx, stream_msg);
		CU_ASSERT(ret == CAM_SERVICE_APP_ERROR);
		CU_ASSERT(ctx[i].state == CAM_STREAM_STATE_FAILED);

		CU_ASSERT(pthread_mutex_lock(&pkt_handling_lock) == 0);
		ctx[i].state = CAM_STREAM_STATE_FAILED;
		ret = stream_event(-1, ctx, stream_msg);
		CU_ASSERT(ret == CAM_SERVICE_APP_ERROR);
		CU_ASSERT(ctx[i].state == CAM_STREAM_STATE_FAILED);

		/* Invalid handler ID */
		ctx[i].state = CAM_STREAM_STATE_IN_PROGRESS;
		msg.handler_id = htonl(CONFIG_CAM_SERVICE_MAX_STREAM_PER_CONN + 1);
		stream_msg = (uint8_t *)&msg;
		/* Called with pkt_handling_lock unlocked on purpose */
		ret = stream_event(-1, ctx, stream_msg);
		CU_ASSERT(ret == CAM_SERVICE_APP_ERROR);

		/* Valid parameters */
		ret = stream_ctx_init(&ctx[i], uuid, timestamp);
		CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
		CU_ASSERT(pthread_mutex_lock(&pkt_handling_lock) == 0);
		ctx[i].state = CAM_STREAM_STATE_IN_PROGRESS;
		msg.handler_id = htonl(i);
		stream_msg = (uint8_t *)&msg;
		ret = stream_event(-1, ctx, stream_msg);
		CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
		CU_ASSERT(ctx[i].state == CAM_STREAM_STATE_IN_PROGRESS);
	}

	for (i = 0; i < CONFIG_CAM_SERVICE_MAX_STREAM_PER_CONN; i++) {
		stream_ctx_destroy(&ctx[i]);
		ret = pthread_mutex_destroy(&ctx[i].ctx_lock);
		CU_ASSERT(ret == 0);
	}
	CU_ASSERT(pthread_mutex_destroy(&pkt_handling_lock) == 0);
}

static void test_stream_multiple_event(void)
{
	int ret;
	int i;
	uint64_t timestamp;
	struct stream_ctx ctx;
	pthread_mutex_t pkt_handling_lock;
	struct cam_msg_a2s_event msg = {
		.header = {
			.version_major = CAM_MSG_VERSION_MAJOR,
			.version_minor = CAM_MSG_VERSION_MINOR,
			.size = htons(sizeof(struct cam_msg_a2s_event)),
			.msg_id = htons(CAM_MSG_A2S_ID_EVENT),
		},
		.timestamp = 0,
		.handler_id = 0,
		.sequence_id = 0,
		.event_id = 0,
	};
	uint8_t *stream_msg = (uint8_t *)&msg;
	cam_uuid_t uuid = { 0 };

	ret = cam_uuid_from_string(test_ctx.uuid_str, uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	ret = stream_timestamp_now(&timestamp);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
	msg.timestamp = htonll(timestamp);

	ret = pthread_mutex_init(&pkt_handling_lock, NULL);
	CU_ASSERT(ret == 0);

	memset(&ctx, 0, sizeof(struct stream_ctx));
	ret = pthread_mutex_init(&ctx.ctx_lock, NULL);
	CU_ASSERT(ret == 0);
	ctx.pkt_handling_lock = &pkt_handling_lock;

	/* Test multiple events in stream 0 */
	ret = stream_ctx_init(&ctx, uuid, timestamp);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
	ctx.state = CAM_STREAM_STATE_IN_PROGRESS;

	msg.handler_id = htonl(0);
	msg.sequence_id = htonl(0);

	for (i = 0; i < ctx.cfg.event_count; i++) {
		CU_ASSERT(pthread_mutex_lock(&pkt_handling_lock) == 0);
		msg.event_id = htons(i);
		msg.sequence_id = htonl(i);
		ret = stream_event(-1, &ctx, stream_msg);
		CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
		CU_ASSERT(ctx.state == CAM_STREAM_STATE_IN_PROGRESS);
	}

	stream_ctx_destroy(&ctx);

	/* Test incorrect event ID */
	ret = stream_ctx_init(&ctx, uuid, timestamp);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);

	CU_ASSERT(pthread_mutex_lock(&pkt_handling_lock) == 0);
	ctx.state = CAM_STREAM_STATE_IN_PROGRESS;
	ctx.sequence_id = 0;

	msg.handler_id = htonl(0);
	msg.sequence_id = htonl(0);
	msg.event_id = htons(0xFF);
	ret = stream_event(-1, &ctx, stream_msg);
	CU_ASSERT(ret == CAM_SERVICE_APP_ERROR);
	CU_ASSERT(ctx.state == CAM_STREAM_STATE_FAILED);

	stream_ctx_destroy(&ctx);
	ret = pthread_mutex_destroy(&ctx.ctx_lock);
	CU_ASSERT(ret == 0);
	CU_ASSERT(pthread_mutex_destroy(&pkt_handling_lock) == 0);
}

static void test_stream_start_timeout(void)
{
	int ret;
	struct stream_ctx ctx;
	pthread_mutex_t pkt_handling_lock;
	uint64_t timestamp;
	struct cam_msg_a2s_start msg = {
		.header = {
			.version_major = CAM_MSG_VERSION_MAJOR,
			.version_minor = CAM_MSG_VERSION_MINOR,
			.size = htons(sizeof(struct cam_msg_a2s_start)),
			.msg_id = htons(CAM_MSG_A2S_ID_START),
		},
		.timestamp = 0,
		.handler_id = 0,
	};
	cam_uuid_t uuid = { 0 };
	uint8_t *stream_msg = (uint8_t *)&msg;

	ret = cam_uuid_from_string(test_ctx.uuid_str, uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	ret = stream_timestamp_now(&timestamp);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);

	ret = pthread_mutex_init(&pkt_handling_lock, NULL);
	CU_ASSERT(ret == 0);

	memset(&ctx, 0, sizeof(struct stream_ctx));
	ret = pthread_mutex_init(&ctx.ctx_lock, NULL);
	CU_ASSERT(ret == 0);
	ctx.pkt_handling_lock = &pkt_handling_lock;
	ret = stream_ctx_init(&ctx, uuid, timestamp);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);

	/* Wait for init to start timeout + 100ms */
	usleep(ctx.cfg.timeout_init_to_start + 100000);

	CU_ASSERT(pthread_mutex_lock(&pkt_handling_lock) == 0);
	ret = stream_start(-1, &ctx, stream_msg);
	CU_ASSERT(ret == CAM_SERVICE_APP_ERROR);
	CU_ASSERT(ctx.state == CAM_STREAM_STATE_FAILED);

	stream_ctx_destroy(&ctx);
	ret = pthread_mutex_destroy(&ctx.ctx_lock);
	CU_ASSERT(ret == 0);
	CU_ASSERT(pthread_mutex_destroy(&pkt_handling_lock) == 0);
}

static void test_stream_event_timeout(void)
{
	int ret;
	struct stream_ctx ctx;
	pthread_mutex_t pkt_handling_lock;
	uint64_t timestamp;
	struct cam_msg_a2s_start start_msg = {
		.header = {
			.version_major = CAM_MSG_VERSION_MAJOR,
			.version_minor = CAM_MSG_VERSION_MINOR,
			.size = htons(sizeof(struct cam_msg_a2s_start)),
			.msg_id = htons(CAM_MSG_A2S_ID_START),
		},
		.timestamp = 0,
		.handler_id = 0,
	};
	struct cam_msg_a2s_event event_msg = {
		.header = {
			.version_major = CAM_MSG_VERSION_MAJOR,
			.version_minor = CAM_MSG_VERSION_MINOR,
			.size = htons(sizeof(struct cam_msg_a2s_event)),
			.msg_id = htons(CAM_MSG_A2S_ID_EVENT),
		},
		.timestamp = 0,
		.handler_id = 0,
		.sequence_id = 0,
		.event_id = 0,
	};
	cam_uuid_t uuid = { 0 };
	uint8_t *stream_msg;

	ret = cam_uuid_from_string(test_ctx.uuid_str, uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	/* First event timeout */
	ret = stream_timestamp_now(&timestamp);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
	start_msg.timestamp = htonll(timestamp);

	ret = pthread_mutex_init(&pkt_handling_lock, NULL);
	CU_ASSERT(ret == 0);

	memset(&ctx, 0, sizeof(struct stream_ctx));
	ret = pthread_mutex_init(&ctx.ctx_lock, NULL);
	CU_ASSERT(ret == 0);
	ctx.pkt_handling_lock = &pkt_handling_lock;
	ret = stream_ctx_init(&ctx, uuid, timestamp);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);

	CU_ASSERT(pthread_mutex_lock(&pkt_handling_lock) == 0);
	ctx.state = CAM_STREAM_STATE_STOPPED;
	stream_msg = (uint8_t *)&start_msg;

	ret = stream_start(-1, &ctx, stream_msg);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);

	/* Wait for start to event timeout + 100ms */
	usleep(ctx.cfg.timeout_start_to_event + 100000);

	CU_ASSERT(pthread_mutex_lock(&pkt_handling_lock) == 0);
	ret = stream_timestamp_now(&timestamp);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
	event_msg.timestamp = htonll(timestamp);
	stream_msg = (uint8_t *)&event_msg;
	ret = stream_event(-1, &ctx, stream_msg);
	CU_ASSERT(ret == CAM_SERVICE_APP_ERROR);
	CU_ASSERT(ctx.state == CAM_STREAM_STATE_FAILED);

	/* Following event timeout */
	ret = stream_timestamp_now(&timestamp);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
	start_msg.timestamp = htonll(timestamp);

	ret = stream_ctx_init(&ctx, uuid, timestamp);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);

	CU_ASSERT(pthread_mutex_lock(&pkt_handling_lock) == 0);
	ctx.state = CAM_STREAM_STATE_STOPPED;
	stream_msg = (uint8_t *)&start_msg;

	ret = stream_start(-1, &ctx, stream_msg);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
	CU_ASSERT(ctx.state == CAM_STREAM_STATE_IN_PROGRESS);

	CU_ASSERT(pthread_mutex_lock(&pkt_handling_lock) == 0);
	ret = stream_timestamp_now(&timestamp);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
	event_msg.timestamp = htonll(timestamp);
	stream_msg = (uint8_t *)&event_msg;
	ret = stream_event(-1, &ctx, stream_msg);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
	CU_ASSERT(ctx.state == CAM_STREAM_STATE_IN_PROGRESS);

	/* Wait for event timeout + 100ms */
	usleep(ctx.cfg.event[0].timeout + 100000);

	CU_ASSERT(pthread_mutex_lock(&pkt_handling_lock) == 0);
	ret = stream_timestamp_now(&timestamp);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);
	event_msg.timestamp = htonll(timestamp);
	event_msg.event_id = htons(1);
	event_msg.sequence_id = htonl(1);
	stream_msg = (uint8_t *)&event_msg;
	ret = stream_event(-1, &ctx, stream_msg);
	CU_ASSERT(ret == CAM_SERVICE_APP_ERROR);
	CU_ASSERT(ctx.state == CAM_STREAM_STATE_FAILED);

	stream_ctx_destroy(&ctx);
	ret = pthread_mutex_destroy(&ctx.ctx_lock);
	CU_ASSERT(ret == 0);
	CU_ASSERT(pthread_mutex_destroy(&pkt_handling_lock) == 0);
}

static CU_TestInfo tests_stream[] = {
	{ "init message", test_stream_init_msg },
	{ "deploy message", test_stream_deploy_msg },
	{ "invalid deploy message", test_stream_invalid_deploy_msg },
	{ "stream start", test_stream_start },
	{ "stream stop", test_stream_stop },
	{ "stream event", test_stream_event },
	{ "stream multiple events", test_stream_multiple_event },
	{ "stream start timeout", test_stream_start_timeout },
	{ "stream event timeout", test_stream_event_timeout },
	CU_TEST_INFO_NULL,
};

static CU_SuiteInfo suites[] = {
	{ "stream",
	  test_stream_suite_init,
	  test_stream_suite_clean,
	  NULL,
	  NULL,
	  tests_stream },
	CU_SUITE_INFO_NULL,
};

int main(int argc, char *argv[])
{
	(void)argc;
	(void)argv;

	test_suite_run(suites);
}
