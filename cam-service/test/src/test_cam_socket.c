/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "posix_call_utils.h"
#include "test_suite.h"

#include <CUnit/Basic.h>
#include <pthread.h>
#include <unistd.h>

#include <cam_service.h>
#include <cam_socket.h>
#include <cam_stream.h>

#include <stdlib.h>

#define TEST_SOCKET_DEFAULT_PORT 21605
#define TEST_SOCKET_IP_ADDR      "127.0.0.1"

#define TEST_SOCKET_BUFF_SIZE 1

struct test_cam_socket_ctx {
	const char *address;
	unsigned int port;
	struct cam_socket_ctx ctx;
	struct cam_socket_cfg cfg;
	uint8_t recv_cnt;
};

static struct test_cam_socket_ctx test_ctx = {
	.address = TEST_SOCKET_IP_ADDR,
	.port = TEST_SOCKET_DEFAULT_PORT,
	.recv_cnt = 0,
};

void cam_stream_process(int fd)
{
	int peer_fd = fd;

	while (1) {
		ssize_t count;
		uint8_t buffer[TEST_SOCKET_BUFF_SIZE];
		POSIX_SOCKET_CALL(
			count, recv, peer_fd, buffer, TEST_SOCKET_BUFF_SIZE, 0);
		if (count == 0) {
			break;
		}

		test_ctx.recv_cnt++;
		POSIX_SOCKET_CALL(
			count, send, peer_fd, buffer, TEST_SOCKET_BUFF_SIZE, MSG_NOSIGNAL);
		CU_ASSERT(count == 1);
	}
}

static int test_socket_suite_init(void)
{
	return 0;
}

static int test_socket_suite_clean(void)
{
	if (close(test_ctx.ctx.fd) == -1) {
		return -1;
	}

	return 0;
}

static void test_socket_single_client(void)
{
	int ret;
	struct cam_socket_cfg *cfg = &test_ctx.cfg;
	int client_fd;
	struct sockaddr_in addr = { 0 };
	ssize_t count;
	uint8_t buffer[TEST_SOCKET_BUFF_SIZE];
	uint8_t retry = 3;

	cfg->addr = test_ctx.address;
	cfg->port = test_ctx.port;

	ret = cam_socket_init(&test_ctx.ctx, &test_ctx.cfg);
	CU_ASSERT(ret == CAM_SERVICE_SUCCESS);

	client_fd = socket(AF_INET, SOCK_STREAM, 0);
	CU_ASSERT_FATAL(client_fd != -1)

	CU_ASSERT_FATAL(test_ctx.recv_cnt == 0);

	addr.sin_family = AF_INET;
	addr.sin_port = htons(test_ctx.port);
	addr.sin_addr.s_addr = inet_addr(test_ctx.address);

	do {
		/* Wait 10ms for the server socket to initialize.*/
		usleep(10000);

		POSIX_SOCKET_CALL(
			ret, connect, client_fd, (struct sockaddr *)&addr, sizeof(addr));
		retry--;
	} while ((ret != 0) && (retry > 0));
	CU_ASSERT(ret == 0);

	CU_ASSERT(test_ctx.recv_cnt == 0);

	POSIX_SOCKET_CALL(
		count, send, client_fd, buffer, TEST_SOCKET_BUFF_SIZE, MSG_NOSIGNAL);
	CU_ASSERT(count == 1);

	POSIX_SOCKET_CALL(count, recv, client_fd, buffer, TEST_SOCKET_BUFF_SIZE, 0);
	CU_ASSERT(count == 1);
	CU_ASSERT(test_ctx.recv_cnt == 1);

	ret = close(client_fd);
	CU_ASSERT(ret == 0);

	test_ctx.recv_cnt = 0;

	/* Wait 10ms to let server drop the connection */
	usleep(10000);
}

static void test_socket_multi_client(void)
{
	int ret;
	int client_fd[CONFIG_CAM_SERVICE_MAX_CONN];
	int extra_client_fd;
	struct sockaddr_in addr = { 0 };
	ssize_t count;
	uint8_t buffer[TEST_SOCKET_BUFF_SIZE];

	CU_ASSERT(test_ctx.recv_cnt == 0);

	addr.sin_family = AF_INET;
	addr.sin_port = htons(test_ctx.port);
	addr.sin_addr.s_addr = inet_addr(test_ctx.address);
	for (int i = 0; i < CONFIG_CAM_SERVICE_MAX_CONN; i++) {
		client_fd[i] = socket(AF_INET, SOCK_STREAM, 0);
		CU_ASSERT(client_fd[i] != -1)
		POSIX_SOCKET_CALL(
			ret, connect, client_fd[i], (struct sockaddr *)&addr, sizeof(addr));
		CU_ASSERT(ret == 0);
	}

	/* Test max number of connections */
	for (int i = 0; i < CONFIG_CAM_SERVICE_MAX_CONN; i++) {
		uint8_t server_recv_cnt = test_ctx.recv_cnt;
		POSIX_SOCKET_CALL(
			count, send, client_fd[i], buffer, TEST_SOCKET_BUFF_SIZE, 0);
		CU_ASSERT(count == 1);

		POSIX_SOCKET_CALL(
			count, recv, client_fd[i], buffer, TEST_SOCKET_BUFF_SIZE, 0);
		CU_ASSERT(count == 1);
		CU_ASSERT(test_ctx.recv_cnt == (server_recv_cnt + 1));
	}

	/* Test the number of connections that exceed the maximum */
	extra_client_fd = socket(AF_INET, SOCK_STREAM, 0);
	CU_ASSERT(extra_client_fd != -1)
	/* Not using the util wrapper here because connect should fail */
	ret = connect(extra_client_fd, (struct sockaddr *)&addr, sizeof(addr));
	if (ret == 0) {
		POSIX_SOCKET_CALL(
			count,
			send,
			extra_client_fd,
			buffer,
			TEST_SOCKET_BUFF_SIZE,
			MSG_NOSIGNAL);

		POSIX_SOCKET_CALL(
			count, recv, extra_client_fd, buffer, TEST_SOCKET_BUFF_SIZE, 0);
		CU_ASSERT(count == 0);
	}

	for (int i = 0; i < CONFIG_CAM_SERVICE_MAX_CONN; i++) {
		ret = close(client_fd[i]);
		CU_ASSERT(ret == 0);
	}
	test_ctx.recv_cnt = 0;

	/* Wait 10ms to let server drop the connections */
	usleep(10000);

	/* Test connection when the connection count is less than the maximum */
	do {
		POSIX_SOCKET_CALL(
			ret,
			connect,
			extra_client_fd,
			(struct sockaddr *)&addr,
			sizeof(addr));
	} while (ret != 0);
	POSIX_SOCKET_CALL(
		count, send, extra_client_fd, buffer, TEST_SOCKET_BUFF_SIZE, 0);
	CU_ASSERT(count == 1);
	POSIX_SOCKET_CALL(
		count, recv, extra_client_fd, buffer, TEST_SOCKET_BUFF_SIZE, 0);
	CU_ASSERT(count == 1);
	ret = close(extra_client_fd);
	CU_ASSERT(ret == 0);
}

static CU_TestInfo tests_socket[] = {
	{ "single client", test_socket_single_client },
	{ "multiple client", test_socket_multi_client },
	CU_TEST_INFO_NULL,
};

static CU_SuiteInfo suites[] = {
	{ "socket",
	  test_socket_suite_init,
	  test_socket_suite_clean,
	  NULL,
	  NULL,
	  tests_socket },
	CU_SUITE_INFO_NULL,
};

int main(int argc, char *argv[])
{
	(void)argc;
	(void)argv;

	return test_suite_run(suites);
}
