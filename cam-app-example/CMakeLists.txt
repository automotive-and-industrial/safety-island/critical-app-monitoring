#
# Critical Application Monitoring (CAM)
#
# SPDX-FileCopyrightText: <text>Copyright 2023
#   Arm Limited and/or its affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause
#

set(CAM_APP_EXAMPLE cam-app-example)
set(DATA_DIR cam-data)

#
# Source files
#
set(CAM_APP_EXAMPLE_SOURCE
	src/example.c
	src/cmdline.c
)

#
# Application
#
add_executable(${CAM_APP_EXAMPLE} ${CAM_APP_EXAMPLE_SOURCE})
target_compile_options(${CAM_APP_EXAMPLE} PRIVATE ${COMPILE_OPTIONS})
target_link_libraries(${CAM_APP_EXAMPLE} libcam::libcam rt Threads::Threads)

install(
	TARGETS
		${CAM_APP_EXAMPLE}
	DESTINATION
		${CMAKE_INSTALL_BINDIR}
)

install(
	DIRECTORY
		${DATA_DIR}
	DESTINATION
		${CMAKE_INSTALL_DATADIR}
)
