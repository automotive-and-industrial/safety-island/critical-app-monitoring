/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef CONFIG_H
#define CONFIG_H

#include <stdbool.h>

#define CONFIG_DEFAULT_SERVICE_ADDRESS "127.0.0.1"

/* Length of uuid_str_base is 28 hexadecimal characters with four hyphens. The
 * number of characters per hyphen is 8-4-4-4-8.
 */
#define CONFIG_DEFAULT_UUID_BASE "84085ddc-bc10-11ed-9a44-7ef9696e"
#define CONFIG_UUID_BASE_LEN     32

#define CONFIG_DEFAULT_MULTIPLE_CONNECTION_ENABLED false
#define CONFIG_DEFAULT_CALIBRATION_MODE_ENABLED    false

#define CONFIG_DEFAULT_STREAM_COUNT 1
#define CONFIG_MAX_STREAM_COUNT     100

#define CONFIG_DEFAULT_PROCESSING_COUNT  2
#define CONFIG_DEFAULT_PROCESSING_PERIOD 3000

#define CONFIG_MAX_EVENT_INTERVAL_COUNT     10
#define CONFIG_DEFAULT_EVENT_INTERVAL_COUNT 1
#define CONFIG_DEFAULT_EVENT_INTERVAL_TIME  0

#define CONFIG_DEFAULT_FAULT_INJECT_ENABLED false
#define CONFIG_DEFAULT_FAULT_INJECT_TIME    0
#define CONFIG_DEFAULT_FAULT_INJECT_STREAM  0

#define CONFIG_DEFAULT_INIT_TO_START_TIME  200
#define CONFIG_DEFAULT_START_TO_EVENT_TIME 300

struct config_stream {
	/* Number of events per processing invocation */
	unsigned int event_interval_count;

	/* Interval time between events in milliseconds */
	unsigned int event_interval_time[CONFIG_MAX_EVENT_INTERVAL_COUNT];
};

struct config_app {
	char *service_address;
	unsigned int service_port;
	char *uuid_base;
	bool enable_multiple_connection;
	unsigned int stream_count;
	unsigned int processing_period;
	unsigned int processing_count;
	bool enable_calibration_mode;
	char *calibration_dir;
	bool enable_fault_injection;
	unsigned int fault_injection_time;
	unsigned int fault_injection_stream;
	struct config_stream config_stream;
};

#endif /* CONFIG_H */
