/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "cmdline.h"
#include "config.h"

#include <dirent.h>
#include <getopt.h>
#include <unistd.h>

#include <cam.h>

#include <assert.h>
#include <limits.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static const struct option long_options[] = {
	{ "service-address", required_argument, NULL, 'a' },
	{ "service-port", required_argument, NULL, 'p' },
	{ "uuid-base", required_argument, NULL, 'u' },
	{ "stream-count", required_argument, NULL, 's' },
	{ "processing-period", required_argument, NULL, 't' },
	{ "processing-count", required_argument, NULL, 'c' },
	{ "event-interval", required_argument, NULL, 'i' },
	{ "enable-multiple-connection", no_argument, NULL, 'e' },
	{ "enable-calibration-mode", no_argument, NULL, 'C' },
	{ "calibration-directory", required_argument, NULL, 'd' },
	{ "enable-fault-injection", no_argument, NULL, 'f' },
	{ "fault-injection-time", required_argument, NULL, 'T' },
	{ "fault-injection-stream", required_argument, NULL, 'S' },
	{ "help", no_argument, NULL, 'h' },
	{ NULL, 0, NULL, 0 }
};

static void cmdline_print_help(char *app_name)
{
	assert(app_name != NULL);

	printf("Usage: %s [OPTIONS]\n", app_name);
	printf("\n");
	printf(
		"An example application part of the Critical Application Monitoring "
		"project.\n");
	printf("\n");
	printf("Options:\n");

	printf(
		"  -a, --service-address <address>             "
		"IP address of the service that applications connect to. "
		"Default: %s\n",
		CONFIG_DEFAULT_SERVICE_ADDRESS);

	printf(
		"  -p, --service-port <port>                   "
		"Port number of the service that applications connect to. "
		"Default: %d\n",
		CAM_DEFAULT_PORT);

	printf(
		"  -u, --uuid-base                             "
		"The UUID base used by the cam-app-example when interacting with "
		"cam-service. The format is 28 hexadecimal characters with four "
		"hyphens. The number of characters per hyphen is 8-4-4-4-8. "
		"Default: %s\n",
		CONFIG_DEFAULT_UUID_BASE);

	printf(
		"  -s, --stream-count <count>                  "
		"Number of streams. A maximum of %d streams can be specified. "
		"Default: %d\n",
		CONFIG_MAX_STREAM_COUNT,
		CONFIG_DEFAULT_STREAM_COUNT);

	printf(
		"  -t, --processing-period <period>            "
		"Period (in ms) of processing activities. "
		"Default: %d\n",
		CONFIG_DEFAULT_PROCESSING_PERIOD);

	printf(
		"  -c, --processing-count <count>              "
		"Total number of processing activities to be performed. "
		"Default: %d\n",
		CONFIG_DEFAULT_PROCESSING_COUNT);

	printf(
		"  -i, --event-interval <interval,[interval]>  "
		"The list of event intervals(in ms) in each processing activity. Items "
		"are separated with comma. The 1st item is the interval time from "
		"activity start to event 0. The subsequent items are the interval from "
		"event n-1 to event n. A maximum of %d intervals can be specified. "
		"Default: %d\n",
		CONFIG_MAX_EVENT_INTERVAL_COUNT,
		CONFIG_DEFAULT_EVENT_INTERVAL_TIME);

	printf(
		"  -e, --enable-multiple-connection            "
		"Enable CAM multiple connection support. "
		"If enabled, cam-app-example will establish dedicated connection for"
		"each stream. If disabled, cam-app-example will establish only one "
		"connection for all the streams. "
		"Default: %s\n",
		CONFIG_DEFAULT_MULTIPLE_CONNECTION_ENABLED ? "true" : "false");

	printf(
		"  -C, --enable-calibration-mode               "
		"Enable calibration mode. "
		"If enabled, cam-app-example will generate a csel file for each "
		"stream. "
		"Default: %s\n",
		CONFIG_DEFAULT_CALIBRATION_MODE_ENABLED ? "true" : "false");

	printf(
		"  -d, --calibration-directory <path>          "
		"Calibration mode option: "
		"The directory path to save the generated csel files. "
		"Default: current directory\n");

	printf(
		"  -f, --enable-fault-injection                "
		"Enable fault injection. "
		"If enabled, new activity of the stream will not be performed after "
		"the preset time. "
		"Default: %s\n",
		CONFIG_DEFAULT_FAULT_INJECT_ENABLED ? "true" : "false");

	printf(
		"  -T, --fault-injection-time <time>           "
		"Fault injection option: "
		"Time(in ms) of stopping new processing activity. "
		"Default: %d\n",
		CONFIG_DEFAULT_FAULT_INJECT_TIME);

	printf(
		"  -S, --fault-injection-stream <stream>       "
		"Fault injection option: "
		"Stream index where the fault is injected. "
		"Default: %d\n",
		CONFIG_DEFAULT_FAULT_INJECT_STREAM);

	printf(
		"  -h, --help                                  "
		"Show this help\n");
}

static void cmdline_show_config(struct config_app *cfg_app)
{
	assert(cfg_app != NULL);

	printf("Cam application configuration:\n");

	printf("    Service IP address: %s\n", cfg_app->service_address);
	printf("    Service port: %u\n", cfg_app->service_port);
	printf("    UUID base: %s\n", cfg_app->uuid_base);
	printf("    Stream count: %u\n", cfg_app->stream_count);
	printf("    Processing period (ms): %u\n", cfg_app->processing_period);
	printf("    Processing count: %u\n", cfg_app->processing_count);
	printf(
		"    Multiple connection support: %s\n",
		cfg_app->enable_multiple_connection ? "true" : "false");
	printf(
		"    Calibration mode support: %s\n",
		cfg_app->enable_calibration_mode ? "true" : "false");

	if (cfg_app->enable_calibration_mode) {
		printf(
			"    Calibration directory: %s[uuid].csel\n",
			cfg_app->calibration_dir);
	}

	printf(
		"    Fault injection support: %s\n",
		cfg_app->enable_fault_injection ? "true" : "false");

	if (cfg_app->enable_fault_injection) {
		printf("    Fault injection time: %u\n", cfg_app->fault_injection_time);
		printf(
			"    Fault injection stream: %u\n",
			cfg_app->fault_injection_stream);
	}

	printf("    Event(s) interval time (ms):");
	for (unsigned int i = 0; i < cfg_app->config_stream.event_interval_count;
		 i++) {
		printf(
			" %u%s",
			cfg_app->config_stream.event_interval_time[i],
			i == (cfg_app->config_stream.event_interval_count - 1) ? "" : ",");
	}
	printf("\n");
}

static int cmdline_process_event_interval(
	char *event_intervals_str,
	struct config_stream *config_stream)
{
	const char *event_sep = ",";
	unsigned int index = 0;
	char *event_interval_str;

	assert(event_intervals_str != NULL);
	assert(config_stream != NULL);

	/*
	 * Split event_intervals_str (which is comma separated) and save each result
	 * into config_stream->event_interval_time[] as an integer.
	 */
	event_interval_str = strtok(event_intervals_str, event_sep);
	if (event_interval_str == NULL) {
		fprintf(
			stderr,
			"Missing value before the first comma in the event interval.\n");
		return CAM_ERROR;
	}

	while (event_interval_str != NULL) {
		long interval_time;
		char *endptr;

		interval_time = strtol(event_interval_str, &endptr, 10);
		if (*endptr != '\0') {
			fprintf(stderr, "Invalid event interval format.\n");
			return CAM_ERROR;
		}

		if ((interval_time < 0) || (interval_time > UINT_MAX)) {
			fprintf(
				stderr,
				"Values in event interval must be a number between 0 and %u\n.",
				UINT_MAX);
			return CAM_ERROR;
		}

		if (index >= CONFIG_MAX_EVENT_INTERVAL_COUNT) {
			fprintf(
				stderr,
				"Number of event intervals exceed the limit of %d.\n",
				CONFIG_MAX_EVENT_INTERVAL_COUNT);
			return CAM_ERROR;
		}

		if ((interval_time == 0) && (index != 0)) {
			fprintf(
				stderr, "Only the first event interval entry may be zero.\n");
			return CAM_ERROR;
		}

		config_stream->event_interval_time[index] = interval_time;
		index++;

		event_interval_str = strtok(NULL, event_sep);
	}

	config_stream->event_interval_count = index;
	return CAM_SUCCESS;
}

static int check_interval_sum(
	struct config_stream *config_stream,
	unsigned int processing_period)
{
	unsigned int time_sum = 0;

	assert(config_stream != NULL);

	for (unsigned int i = 0; i < config_stream->event_interval_count; i++) {
		if (config_stream->event_interval_time[i] >= UINT16_MAX) {
			fprintf(
				stderr,
				"Values in event interval must be smaller than %d\n.",
				UINT16_MAX);
			return CAM_ERROR;
		}
		time_sum += config_stream->event_interval_time[i];
	}

	if (time_sum > processing_period) {
		fprintf(
			stderr,
			"Sum of event intervals (%u ms) must be smaller than the "
			"processing period (%u ms).\n",
			time_sum,
			processing_period);
		return CAM_ERROR;
	}

	return CAM_SUCCESS;
}

static int check_fault_injection_config(struct config_app *cfg_app)
{
	assert(cfg_app != NULL);
	if (!cfg_app->enable_fault_injection) {
		return CAM_SUCCESS;
	}

	/*
	 * If fault injection happens at last processing period, it will be
	 * invalid.
	 */
	if ((cfg_app->processing_count - 1) * cfg_app->processing_period <
		cfg_app->fault_injection_time) {
		fprintf(
			stderr,
			"Fault injection time exceeds total application running"
			" time.\n");
		return CAM_ERROR;
	}

	if (cfg_app->stream_count <= cfg_app->fault_injection_stream) {
		fprintf(
			stderr,
			"Fault injection stream index exceeds total stream "
			"count.\n");
		return CAM_ERROR;
	}
	return CAM_SUCCESS;
}

int cmdline_parse(int argc, char *argv[], struct config_app *cfg_app)
{
	int opt, service_port, stream_count, processing_period, processing_count,
		fault_injection_time, fault_injection_stream;
	DIR *calibration_dir;
	char *endptr;
	cam_uuid_t uuid;
	int ret;
	int dummy_activity_id = 1;
	char *uuid_str = NULL;

	assert(cfg_app != NULL);

	while ((opt = getopt_long(
				argc, argv, "a:p:s:t:c:i:d:eCfT:S:u:h", long_options, NULL)) !=
		   -1) {
		switch (opt) {
		case 'a':
			cfg_app->service_address = optarg;
			break;
		case 'p':
			service_port = atoi(optarg);
			if ((service_port <= 0) || (service_port > UINT16_MAX)) {
				fprintf(
					stderr,
					"Service port must be a number between 1 and %d\n",
					UINT16_MAX);
				return CAM_ERROR;
			}
			cfg_app->service_port = service_port;
			break;
		case 'u':
			if (strlen(optarg) != CONFIG_UUID_BASE_LEN) {
				fprintf(stderr, "Invalid UUID base length\n");
				return CAM_ERROR;
			}

			uuid_str = cmdline_get_uuid_str(optarg, dummy_activity_id);
			if (uuid_str == NULL) {
				fprintf(stderr, "Unable to generate UUID string\n");
				return CAM_ERROR;
			}

			ret = cam_uuid_from_string(uuid_str, uuid);
			free(uuid_str);
			if (ret != CAM_UUID_SUCCESS) {
				fprintf(stderr, "Invalid UUID base format\n");
				return CAM_ERROR;
			}

			cfg_app->uuid_base = optarg;
			break;
		case 's':
			stream_count = atoi(optarg);
			if (stream_count <= 0) {
				fprintf(stderr, "Stream count must be a positive number\n");
				return CAM_ERROR;
			}
			if (stream_count > CONFIG_MAX_STREAM_COUNT) {
				fprintf(
					stderr,
					"Maximum stream count is %d\n",
					CONFIG_MAX_STREAM_COUNT);
				return CAM_ERROR;
			}
			cfg_app->stream_count = stream_count;
			break;
		case 't':
			processing_period = atoi(optarg);
			if (processing_period <= 0) {
				fprintf(stderr, "Event period must be a positive number\n");
				return CAM_ERROR;
			}
			cfg_app->processing_period = processing_period;
			break;
		case 'c':
			processing_count = atoi(optarg);
			if (processing_count <= 0) {
				fprintf(stderr, "Processing count must be a positive number\n");
				return CAM_ERROR;
			}
			cfg_app->processing_count = processing_count;
			break;
		case 'i':
			if (cmdline_process_event_interval(
					optarg, &(cfg_app->config_stream)) != CAM_SUCCESS) {
				return CAM_ERROR;
			}
			break;
		case 'e':
			cfg_app->enable_multiple_connection = true;
			break;
		case 'C':
			cfg_app->enable_calibration_mode = true;
			break;
		case 'd':
			calibration_dir = opendir(optarg);
			if (calibration_dir) {
				cfg_app->calibration_dir = optarg;
				closedir(calibration_dir);
			} else {
				fprintf(stderr, "Calibration directory does not exist.\n");
				return CAM_ERROR;
			}
			break;
		case 'f':
			cfg_app->enable_fault_injection = true;
			break;
		case 'T':
			fault_injection_time = strtol(optarg, &endptr, 10);
			if (*endptr != '\0') {
				fprintf(stderr, "Invalid fault injection time format.\n");
				return CAM_ERROR;
			}
			if (fault_injection_time < 0) {
				fprintf(
					stderr,
					"Fault injection time must be a non-negative "
					"number\n");
				return CAM_ERROR;
			}
			cfg_app->fault_injection_time = fault_injection_time;
			endptr = NULL;
			break;
		case 'S':
			fault_injection_stream = strtol(optarg, &endptr, 10);
			if (*endptr != '\0') {
				fprintf(stderr, "Invalid fault injection stream format.\n");
				return CAM_ERROR;
			}
			if (fault_injection_stream < 0) {
				fprintf(
					stderr,
					"Fault injection stream must be a non-negative "
					"number\n");
				return CAM_ERROR;
			}
			cfg_app->fault_injection_stream = fault_injection_stream;
			endptr = NULL;
			break;
		case 'h':
			cmdline_print_help(argv[0]);
			exit(EXIT_SUCCESS);
		default:
			return CAM_ERROR;
		}
	}

	/* Check if the sum of all event periods fit into the processing period */
	if (check_interval_sum(
			&(cfg_app->config_stream), cfg_app->processing_period) !=
		CAM_SUCCESS) {
		return CAM_ERROR;
	}

	if (check_fault_injection_config(cfg_app) != CAM_SUCCESS) {
		return CAM_ERROR;
	}

	cmdline_show_config(cfg_app);
	return CAM_SUCCESS;
}

char *cmdline_get_uuid_str(const char *uuid_base, unsigned int id)
{
	char *uuid_str = NULL;

	if (uuid_base != NULL) {
		uuid_str = malloc(CAM_UUID_STR_LEN);
		if (uuid_str != NULL) {
			int ret;

			ret = snprintf(uuid_str, CAM_UUID_STR_LEN, "%s%04u", uuid_base, id);
			if (ret < 0) {
				free(uuid_str);
				uuid_str = NULL;
			}
		}
	} else {
		fprintf(stderr, "Expecting non-NULL uuid_base\n");
	}
	return uuid_str;
}
