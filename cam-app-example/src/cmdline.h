/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef CMDLINE_H
#define CMDLINE_H

#include "config.h"

int cmdline_parse(int argc, char *argv[], struct config_app *cfg_app);
char *cmdline_get_uuid_str(const char *uuid_base, unsigned int id);

#endif /* CMDLINE_H */
