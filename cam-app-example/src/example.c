/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */
#define _GNU_SOURCE

#include "cmdline.h"
#include "config.h"

#include <pthread.h>
#include <semaphore.h>
#include <unistd.h>

#include <cam.h>

#include <assert.h>
#include <limits.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <threads.h>
#include <time.h>

#define MAX(i, j) (((i) > (j)) ? (i) : (j))

/* Timeout in milliseconds */
#define INIT_TO_START_TIMEOUT  CONFIG_DEFAULT_INIT_TO_START_TIME * 1.5
#define START_TO_EVENT_TIMEOUT CONFIG_DEFAULT_START_TO_EVENT_TIME * 1.5

#define CAM_CSEL_FILE_EXTENSION ".csel"

/*
 * Will append a four-character stream index(1,2....) at the end
 * of stream_name_base.
 */
static const char *stream_name_base = "CAM STREAM ";

static struct config_app cfg_app = {
	.service_address = CONFIG_DEFAULT_SERVICE_ADDRESS,
	.service_port = CAM_DEFAULT_PORT,
	.uuid_base = CONFIG_DEFAULT_UUID_BASE,
	.stream_count = CONFIG_DEFAULT_STREAM_COUNT,
	.enable_multiple_connection = CONFIG_DEFAULT_MULTIPLE_CONNECTION_ENABLED,
	.processing_count = CONFIG_DEFAULT_PROCESSING_COUNT,
	.processing_period = CONFIG_DEFAULT_PROCESSING_PERIOD,
	.enable_calibration_mode = CONFIG_DEFAULT_CALIBRATION_MODE_ENABLED,
	.calibration_dir = "./",
	.enable_fault_injection = CONFIG_DEFAULT_FAULT_INJECT_ENABLED,
	.fault_injection_time = CONFIG_DEFAULT_FAULT_INJECT_TIME,
	.fault_injection_stream = CONFIG_DEFAULT_FAULT_INJECT_STREAM,
	.config_stream = {
		.event_interval_count = CONFIG_DEFAULT_EVENT_INTERVAL_COUNT,
		.event_interval_time = {
			[0] = CONFIG_DEFAULT_EVENT_INTERVAL_TIME,
		},
	},
};

struct stream_activity_context {
	unsigned int activity_id;
	struct cam_stream_config stream_config;
	unsigned int processing_count;
	timer_t processing_timer;
	cam_stream_ctx_t stream_ctx;
	sem_t sem_done;
	int return_value;
	mtx_t mtx;
	bool fault_injection;
	timer_t fault_injection_timer;
};

static void msleep(unsigned int time)
{
	usleep(1000 * time);
}

/* Convert a value from milliseconds to 'timespec' */
static void ms_to_timespec(unsigned int value_ms, struct timespec *timeval)
{
	assert(timeval != NULL);

	timeval->tv_sec = value_ms / 1000;
	timeval->tv_nsec = (value_ms % 1000) * 1000000;
}

static void do_stream_processing(union sigval v)
{
	struct stream_activity_context *ctx =
		(struct stream_activity_context *)v.sival_ptr;
	assert(ctx != NULL);

	mtx_lock(&ctx->mtx);
	if (!ctx->fault_injection) {
		/* Ensure timeout is enough to trigger fault on service */
		for (unsigned int i = 0; i < cfg_app.config_stream.event_interval_count;
			 i++) {
			/* Add a synthetic delay (configurable) before each event */
			msleep(cfg_app.config_stream.event_interval_time[i]);

			printf("    Stream %u sends event %u\n", ctx->activity_id, i);

			/* Send CAM Stream Event */
			if (cam_stream_event(&ctx->stream_ctx, i) != CAM_SUCCESS) {
				fprintf(stderr, "Failed to send stream event\n");
				ctx->return_value = CAM_ERROR;
				if (sem_post(&ctx->sem_done) != 0) {
					exit(EXIT_FAILURE);
				}
				mtx_unlock(&ctx->mtx);
				return;
			}
		}
	}

	ctx->processing_count--;

	/*
	 * When processing count reaches zero, signal the end of
	 * the processing activity.
	 */
	if (ctx->processing_count == 0) {
		if (sem_post(&ctx->sem_done) != 0) {
			exit(EXIT_FAILURE);
		}
	}
	mtx_unlock(&ctx->mtx);
}

static void do_fault_injection(union sigval v)
{
	struct stream_activity_context *ctx =
		(struct stream_activity_context *)v.sival_ptr;
	assert(ctx != NULL);
	mtx_lock(&ctx->mtx);
	ctx->fault_injection = true;
	mtx_unlock(&ctx->mtx);
}

static int start_fault_injection_timer(struct stream_activity_context *ctx)
{
	struct itimerspec fault_ts = { 0 };
	struct sigevent fault_evp = {
		.sigev_value.sival_ptr = ctx,
		.sigev_notify = SIGEV_THREAD,
		.sigev_notify_attributes = NULL,
		.sigev_notify_function = do_fault_injection,
	};

	if (timer_create(CLOCK_REALTIME, &fault_evp, &ctx->fault_injection_timer) <
		0) {
		return CAM_ERROR;
	}

	/*
	 * Fault inject timer will only trigger callback function once.
	 * The actual fault injection time needs to be added to the time from
	 * stream initialization to the first stream processing.
	 */
	ms_to_timespec(
		cfg_app.fault_injection_time + CONFIG_DEFAULT_INIT_TO_START_TIME +
			CONFIG_DEFAULT_START_TO_EVENT_TIME,
		&fault_ts.it_value);
	ms_to_timespec(0, &fault_ts.it_interval);

	if (timer_settime(ctx->fault_injection_timer, 0, &fault_ts, NULL) < 0) {
		fprintf(stderr, "Failed to set fault inject timer\n");
		ctx->return_value = timer_delete(ctx->fault_injection_timer);
		return CAM_ERROR;
	}
	return CAM_SUCCESS;
}

/*
 * Simulates some activity in the application by setting up a timer that
 * periodically invokes the do_stream_event() function.
 */
static void *do_stream_activity(void *arg)
{
	struct stream_activity_context *ctx = (struct stream_activity_context *)arg;
	struct cam_stream_cal_config *stream_cal_config = NULL;
	struct itimerspec ts = { 0 };
	struct sigevent evp = {
		.sigev_value.sival_ptr = ctx,
		.sigev_notify = SIGEV_THREAD,
		.sigev_notify_attributes = NULL,
		.sigev_notify_function = do_stream_processing,
	};
	struct cam_stream_cal_config cal_config = {
		.timeout_i2s = INIT_TO_START_TIMEOUT * 1000,
		.timeout_s2e = START_TO_EVENT_TIMEOUT * 1000,
	};

	cam_ctx_t cam_ctx;
	int ret;

	printf("Starting activity...\n");

	ctx->return_value = CAM_SUCCESS;
	ctx->processing_count = cfg_app.processing_count;

	/*
	 * Initialize the Critical Application Monitoring library
	 */
	if (cfg_app.enable_multiple_connection &&
		!cfg_app.enable_calibration_mode) {
		if (cam_init(&cam_ctx, cfg_app.service_address, cfg_app.service_port) !=
			CAM_SUCCESS) {
			fprintf(stderr, "Failed to initialize CAM library\n");
			ret = CAM_ERROR;
			goto exit;
		}
		ctx->stream_config.ctx = &cam_ctx;
	}

	/* Set stream config name */
	ret = snprintf(
		ctx->stream_config.name,
		CAM_STREAM_NAME_LEN,
		"%s %u",
		stream_name_base,
		ctx->activity_id);
	if (ret < 0) {
		ctx->return_value = CAM_ERROR;
		goto exit;
	}

	/* Set stream UUID */
	ctx->stream_config.uuid_str =
		cmdline_get_uuid_str(cfg_app.uuid_base, ctx->activity_id);
	if (ctx->stream_config.uuid_str == NULL) {
		ctx->return_value = CAM_ERROR;
		goto exit;
	}

	/* Set thread name based on the stream name */
	assert(strnlen(ctx->stream_config.name, CAM_STREAM_NAME_LEN) < 16);
	pthread_setname_np(pthread_self(), ctx->stream_config.name);

	/*
	 * Initialize the event stream
	 */
	if (cfg_app.enable_calibration_mode == true) {
		char csel_file_name[PATH_MAX] = { 0 };
		ret = snprintf(
			csel_file_name,
			PATH_MAX,
			"%s/%s%s",
			cfg_app.calibration_dir,
			ctx->stream_config.uuid_str,
			CAM_CSEL_FILE_EXTENSION);

		if (ret < 0) {
			fprintf(stderr, "Failed to format csel filename.\n");
			ctx->return_value = CAM_ERROR;
			goto exit;
		}

		cal_config.file_name = csel_file_name;
		stream_cal_config = &cal_config;
	}

	if (cam_stream_init(
			&ctx->stream_ctx, &ctx->stream_config, stream_cal_config) !=
		CAM_SUCCESS) {
		fprintf(stderr, "Failed to initialize stream\n");
		ctx->return_value = CAM_ERROR;
		goto exit;
	}

	/* Add a synthetic delay period between the Init and Start calls */
	msleep(CONFIG_DEFAULT_INIT_TO_START_TIME);

	/*
	 * Start CAM stream
	 */
	if (cam_stream_start(&ctx->stream_ctx) != CAM_SUCCESS) {
		fprintf(stderr, "Failed to start CAM stream\n");
		ctx->return_value = CAM_ERROR;
		goto stream_end;
	}

	if (sem_init(&ctx->sem_done, 0, 0) != 0) {
		ctx->return_value = CAM_ERROR;
		pthread_exit(NULL);
		goto stream_stop;
	}

	if (mtx_init(&ctx->mtx, mtx_plain) != thrd_success) {
		fprintf(stderr, "Failed to initialize mutex\n");
		ctx->return_value = CAM_ERROR;
		goto sem_destroy;
	}

	if (timer_create(CLOCK_REALTIME, &evp, &ctx->processing_timer) < 0) {
		fprintf(stderr, "Failed to initialize timer\n");
		ctx->return_value = CAM_ERROR;
		goto mtx_destroy;
	}

	/* Set a synthetic delay period between the Stream Start and the events */
	ms_to_timespec(CONFIG_DEFAULT_START_TO_EVENT_TIME, &ts.it_value);

	/*
	 * Set the processing period from where the Stream Events will be sent from
	 */
	ms_to_timespec(cfg_app.processing_period, &ts.it_interval);

	/*
	 * Set and arm the timer. All further processing activities will be done via
	 * the timer triggered thread.
	 */
	if (timer_settime(ctx->processing_timer, 0, &ts, NULL) < 0) {
		fprintf(stderr, "Failed to set timer\n");
		ctx->return_value = CAM_ERROR;
		goto timer_delete;
	}

	/* Await all activity processing to finish */
	if (sem_wait(&ctx->sem_done) < 0) {
		fprintf(stderr, "Failed to wait for activity processing to finish\n");
		ctx->return_value = CAM_ERROR;
	}

timer_delete:
	if (timer_delete(ctx->processing_timer) != 0) {
		fprintf(stderr, "Failed to delete timer\n");
		ctx->return_value = CAM_ERROR;
	}

mtx_destroy:
	mtx_destroy(&ctx->mtx);

sem_destroy:
	if (sem_destroy(&ctx->sem_done) != 0) {
		fprintf(stderr, "Failed to destroy semaphore\n");
		ctx->return_value = CAM_ERROR;
	}

	/*
	 * Ensure the time before cam_stream_stop is enough for service side to
	 * detect fault.
	 */
	if (ctx->fault_injection) {
		msleep(MAX(cfg_app.processing_period, START_TO_EVENT_TIMEOUT));
	}

stream_stop:
	/*
	 * Indicate to the CAM Service the event stream stopped
	 */
	if (cam_stream_stop(&ctx->stream_ctx) != CAM_SUCCESS) {
		fprintf(stderr, "Failed to stop CAM stream\n");
		ctx->return_value = CAM_ERROR;
	}

stream_end:
	/*
	 * End the event stream
	 */
	if (cam_stream_end(&ctx->stream_ctx) != CAM_SUCCESS) {
		fprintf(stderr, "Failed to end the stream\n");
		ctx->return_value = CAM_ERROR;
	}

exit:
	/*
	 * Terminate the CAM connection
	 */
	if (cfg_app.enable_multiple_connection &&
		!cfg_app.enable_calibration_mode) {
		if (cam_end(&cam_ctx) != CAM_SUCCESS) {
			fprintf(stderr, "Failed to terminate CAM connection\n");
			ret = CAM_ERROR;
		}
	}

	pthread_exit(NULL);
}

int main(int argc, char *argv[])
{
	int ret = EXIT_SUCCESS;
	cam_ctx_t ctx;
	unsigned int major_version, minor_version;
	struct stream_activity_context *stream_activity_ctx_list;
	struct stream_activity_context *fault_stream_activity_ctx = NULL;
	pthread_t *threads;
	unsigned int num_stream_fail = 0;

	/* Parse cmdline input */
	if (cmdline_parse(argc, argv, &cfg_app) != CAM_SUCCESS) {
		exit(EXIT_FAILURE);
	}

	/*
	 * Create stream_activity_ctx_list to contain context for stream activity.
	 */
	stream_activity_ctx_list =
		calloc(cfg_app.stream_count, sizeof(struct stream_activity_context));
	if (stream_activity_ctx_list == NULL) {
		fprintf(
			stderr,
			"Failed to allocate memory for the context list of streams\n");
		exit(EXIT_FAILURE);
	}

	/*
	 * Get libcam version
	 */
	if (cam_version(&major_version, &minor_version) != CAM_SUCCESS) {
		fprintf(stderr, "Failed to retrieve libcam version\n");
		ret = EXIT_FAILURE;
		goto free_stream_mem;
	}
	printf("Using libcam v%u.%u\n", major_version, minor_version);

	/*
	 * Initialize the Critical Application Monitoring library
	 */
	if (!cfg_app.enable_multiple_connection &&
		!cfg_app.enable_calibration_mode) {
		if (cam_init(&ctx, cfg_app.service_address, cfg_app.service_port) !=
			CAM_SUCCESS) {
			fprintf(stderr, "Failed to initialize CAM library\n");
			ret = EXIT_FAILURE;
			goto cam_end;
		}
	}

	/*
	 * Initialize threads for stream activity
	 */
	threads = malloc(cfg_app.stream_count * sizeof(pthread_t));
	if (threads == NULL) {
		fprintf(stderr, "Failed to allocate memory for threads\n");
		ret = EXIT_FAILURE;
		goto cam_end;
	}

	/*
	 * Start fault injection
	 */
	for (unsigned int i = 0; i < cfg_app.stream_count; i++) {
		stream_activity_ctx_list[i].fault_injection = false;
	}
	if (cfg_app.enable_fault_injection) {
		fault_stream_activity_ctx =
			&stream_activity_ctx_list[cfg_app.fault_injection_stream];
		if (cfg_app.fault_injection_time != 0) {
			if (start_fault_injection_timer(fault_stream_activity_ctx) !=
				CAM_SUCCESS) {
				ret = CAM_ERROR;
				goto free_threads;
			}
		} else {
			mtx_lock(&fault_stream_activity_ctx->mtx);
			fault_stream_activity_ctx->fault_injection = true;
			mtx_unlock(&fault_stream_activity_ctx->mtx);
		}
	}

	/*
	 * Create stream activity thread and run it
	 */
	for (unsigned int i = 0; i < cfg_app.stream_count; i++) {
		stream_activity_ctx_list[i].activity_id = i;
		if (!cfg_app.enable_multiple_connection) {
			stream_activity_ctx_list[i].stream_config.ctx = &ctx;
		}

		if (pthread_create(
				&threads[i],
				NULL,
				do_stream_activity,
				&(stream_activity_ctx_list[i])) != 0) {
			fprintf(stderr, "Failed to create stream activity thread\n");
			ret = EXIT_FAILURE;
			goto fault_injection_timer_delete;
		}
	}

	/*
	 * Wait all stream activity threads to finish
	 */
	for (unsigned int i = 0; i < cfg_app.stream_count; i++) {
		if (pthread_join(threads[i], NULL) != 0) {
			fprintf(stderr, "Failed to join stream activity thread\n");
			ret = EXIT_FAILURE;
			goto fault_injection_timer_delete;
		}

		if (stream_activity_ctx_list[i].return_value == CAM_ERROR) {
			fprintf(stderr, "Stream %u exited with failure\n", i);
			num_stream_fail++;
		}
	}

	if (num_stream_fail > 0) {
		printf(
			"%u streams out of %u exited with failure.\n",
			num_stream_fail,
			cfg_app.stream_count);
		ret = EXIT_FAILURE;
	}

fault_injection_timer_delete:
	if (cfg_app.enable_fault_injection && cfg_app.fault_injection_time != 0) {
		if (timer_delete(fault_stream_activity_ctx->fault_injection_timer) !=
			CAM_SUCCESS) {
			ret = EXIT_FAILURE;
		}
	}

free_threads:
	free(threads);

cam_end:
	/*
	 * Terminate the CAM connection
	 */
	if (!cfg_app.enable_multiple_connection &&
		!cfg_app.enable_calibration_mode) {
		if (cam_end(&ctx) != CAM_SUCCESS) {
			fprintf(stderr, "Failed to terminate CAM connection\n");
			ret = EXIT_FAILURE;
		}
	}

free_stream_mem:
	free(stream_activity_ctx_list);

	exit(ret);
}
