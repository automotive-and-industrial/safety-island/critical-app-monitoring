..
 #
 # Critical Application Monitoring (CAM)
 #
 # SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited
 # and/or its affiliates <open-source-office@arm.com></text>
 #
 # SPDX-License-Identifier: BSD-3-Clause
 #

.. _CSC_File:

################################
Stream Configuration File Format
################################

The CAM Stream Configuration (CSC) file contains human readable settings used
for the deployment phase of a critical application in the CAM project.

Files should use the extension ``.csc.yml``.

**************
Header Section
**************

The file format uses `Yaml <https://yaml.org/>` data serialization language
version 1.2.

Configuration files should explicitly indicate the version using the `YAML`
directive before the document contents:

.. code-block:: yaml

  %YAML 1.2
  ---

The mapping node is used to determine what file format version the file is
compatible with. This document is specifies the version "1.0".

.. code-block:: yaml

  cam_stream_conf_version: "1.0"


**************
Metadata block
**************

The ``metadata`` block collection describes the target stream using two
mappings:

.. code-block:: yaml

  metadata:
    uuid: "00000000-0000-0000-0000-000000000000"
    name: "Stream name is up to 63 characters long"

The ``uuid`` string is used to uniquely identify the stream and must follow the
exact format (36 characters including 4 hyphens) as the example shown. This file
format does not enforce any UUID variant. UUIDs are used as opaque data values
to ensure streams are uniquely identified.

The ``name`` field is used to give the stream a textual name and might be
present in logs. It is only used for convenience and it is not used to uniquely
identify the stream. The configuration file may use `utf-8` encoding for the
stream name and should not be longer than 63 characters.

*******************
State control block
*******************

The ``state_control`` block collection is used to specify stream state timeouts.
The values must be unsigned integers (32-bit) representing microseconds.

.. code-block:: yaml

  state_control:
    timeout_init_to_start: 300000
    timeout_start_to_event: 450000

The ``timeout_init_to_start`` mapping specifies the timeout between the
`stream init` and the `stream start` messages. Zero indicates no timeout.

The ``timeout_start_to_event`` mapping specifies the timeout between the
`stream start` and the first stream event message in microseconds. Must not be
zero.

******************
Event alarms block
******************

The ``event_alarms`` is a block sequence containing one or more mappings that
identify each alarm in the stream.

.. code-block:: yaml

  event_alarms:
  - event_id: 0
    mode: 0
    timeout: 3500105

.. _event_id:

The ``event_id`` mapping is an unsigned integer (16-bit) which specifies the
event within the stream. Must start from 0 and increments by one without any
gaps.

The ``mode`` mapping specifies the alarm mode. There are 3 possible alarm modes,
both the string and the integer values are allowed:

  * ``"ondemand_from_arrival"`` or ``0``
  * ``"ondemand_from_app"`` or ``1``
  * ``"periodic"`` or ``2``

The ``timeout`` mapping is an unsigned integer (32-bit) value which specifies
the timeout.

*******
Example
*******

Here is an example `.csc.yml` file with three event alarms:

.. code-block:: yaml

  %YAML 1.2
  ---
  cam_stream_conf_version: "1.0"
  metadata:
    uuid: "00000000-0000-0000-0000-000000000000"
    name: "Stream name is up to 63 characters long"
  state_control:
    timeout_init_to_start: 300000
    timeout_start_to_event: 450000
  event_alarms:
  - event_id: 0
    mode: "ondemand_from_arrival"
    timeout: 3500105
  - event_id: 1
    mode: 1
    timeout: 3500205
  - event_id: 2
    mode: "periodic"
    timeout: 3500305
