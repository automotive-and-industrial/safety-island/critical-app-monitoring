..
 #
 # Critical Application Monitoring (CAM)
 #
 # SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited
 # and/or its affiliates <open-source-office@arm.com></text>
 #
 # SPDX-License-Identifier: BSD-3-Clause
 #

.. _manual:

##################
Development Manual
##################

.. toctree::
   :maxdepth: 2

   libcam_api
   message_protocol
   file_csel
   file_csc
   file_csd
