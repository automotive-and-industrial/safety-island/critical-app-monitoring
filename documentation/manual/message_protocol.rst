..
 #
 # Critical Application Monitoring (CAM)
 #
 # SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited
 # and/or its affiliates <open-source-office@arm.com></text>
 #
 # SPDX-License-Identifier: BSD-3-Clause
 #

.. _manual_message_protocol:

################
Message Protocol
################

This section specifies the message protocol used to communicate with
**cam-service**. The primary goal of the protocol is to allow critical
applications to initialize and send stream events to be monitored.

**libcam** in this project provides an implementation of this protocol relevant
to critical applications allowing them to easily integrate with the CAM project.

All multi-byte values are transmitted as big-endian. All bitmap values have bit
0 defined as the least significant bit.

*Reserved* fields are not assumed to have any specific value.

.. _message_protocol_timestamp:

About the timestamp fields:
  * The protocol does not assume or enforce an epoch.
  * The timestamps must come from clocks synchronized between participants
    (**cam-service**, **cam-tool** and critical applications).

.. _message_protocol_uuid:

About the Universally Unique Identifier (UUID) fields:
  * UUIDs are 16-byte long fields representing the 128-bit UUID in binary
    format.
  * The protocol does not enforce any UUID variant. **cam-service** uses the
    UUID as a whole opaque data value and does not process the sub-fields of
    the UUID.

.. _message_protocol_handler:

About the handler fields:
  * To avoid having to transmit 128-bit UUIDs on each message, a 32-bit
    handler is used once a stream is established with the
    :ref:`stream_initialization_message`.
  * Handlers are valid only on the connection the stream was initialized.
  * The protocol assumes the handler is an opaque data and does not enforce
    how they are created. Any obfuscation or non-guessable mechanism must be
    defined by **cam-service**.

.. _message_protocol_message_header:

**************
Message Header
**************

The message header contains a number of fields and is present at the beginning
of each message.


.. list-table::
  :header-rows: 1

  * - Field
    - Size (in bytes)
    - Description
  * - Protocol version - major
    - 1
    - ``0x01`` for this specification
  * - Protocol version - minor
    - 1
    - ``0x00`` for this specification
  * - Message size
    - 2
    - Unsigned integer, the whole message size includes the header size
  * - Message ID
    - 2
    - | · Unsigned integer
      | · Message IDs are per message direction (to and from cam-service)
      | · Different for each message type, see details in each message
      | description
      | · ``0`` is not a valid message ID
  * - *Reserved*
    - 2
    -

.. _stream_initialization_message:

*****************************
Stream Initialization Message
*****************************

Application requests the creation of a new monitoring stream. **cam-service**
will allocate the required resources and load the configuration it has which
matches the UUID received. If the stream initialization in **cam-service**
succeeds, **cam-service** will return a 32-bit handler for critical applications
to use in further messages. After the initialization, the stream is in halt
mode. Before the monitoring starts, applications must "start" the stream using
the :ref:`stream_start_message`

Critical Application to **cam-service**
=======================================

.. list-table::
  :header-rows: 1

  * - Field
    - Size (in bytes)
    - Description
  * - :ref:`message_protocol_message_header`
    - 8
    - Message ID = ``1``
  * - :ref:`Timestamp <message_protocol_timestamp>`
    - 8
    - Unsigned integer, message timestamp
  * - :ref:`UUID <message_protocol_uuid>`
    - 16
    - Stream UUID

**cam-service** to Critical Application
=======================================

.. list-table::
  :header-rows: 1

  * - Field
    - Size (in bytes)
    - Description
  * - :ref:`message_protocol_message_header`
    - 8
    - Message ID = ``1``
  * - :ref:`Timestamp <message_protocol_timestamp>`
    - 8
    - Unsigned integer, message timestamp
  * - :ref:`UUID <message_protocol_uuid>`
    - 16
    - Stream UUID, same as the UUID from the stream that requested the
      initialization
  * - Status
    - 2
    - | Unsigned integer,
      | ``0`` - Success,
      | ``1`` - Service is out of resources,
      | ``2`` - Service has no configuration for the requested UUID
  * - *Reserved*
    - 2
    -
  * - :ref:`Handler <message_protocol_handler>`
    - 4
    - Unsigned integer, identifies the stream

.. _stream_start_message:

********************
Stream Start Message
********************

Application starts the stream monitoring by sending a stream start message. The
stream handler must match an initialized stream which is currently in "halt"
mode.

The start message serves two purposes:

* Decouple the initialization from the starting of the monitoring allowing an
  application to take as long as it needs in the initialization phase (which is
  assumed as non-critical). It is only after the "start" message that
  **cam-service** will start to monitor events.

* Together with the :ref:`stream_stop_message`, the stream start message allows
  a stream to be started/stopped multiple times without the application having
  to disconnect.

Critical Application to **cam-service**
=======================================

.. list-table::
  :header-rows: 1

  * - Field
    - Size (in bytes)
    - Description
  * - :ref:`message_protocol_message_header`
    - 8
    - Message ID = ``2``
  * - :ref:`Timestamp <message_protocol_timestamp>`
    - 8
    - Unsigned integer, message timestamp
  * - :ref:`Handler <message_protocol_handler>`
    - 4
    - Unsigned integer, identifies the stream
  * - *Reserved*
    - 4
    -

.. _stream_stop_message:

*******************
Stream Stop Message
*******************

Application indicates to **cam-service** that the stream identified by the
stream handler must be stopped. No further stream events are going to be sent to
**cam-service**. **cam-service** must ensure all watchdogs related to this
stream are stopped.

Critical Application to **cam-service**
=======================================

.. list-table::
  :header-rows: 1

  * - Field
    - Size (in bytes)
    - Description
  * - :ref:`message_protocol_message_header`
    - 8
    - Message ID = ``3``
  * - :ref:`Timestamp <message_protocol_timestamp>`
    - 8
    - Unsigned integer, message timestamp
  * - :ref:`Handler <message_protocol_handler>`
    - 4
    - Unsigned integer, identifies the stream
  * - *Reserved*
    - 4
    -

********************
Stream Event Message
********************

Application sends stream events identified by the stream handler to the
**cam-service**.

Critical Application to **cam-service**
=======================================

.. list-table::
  :header-rows: 1

  * - Field
    - Size (in bytes)
    - Description
  * - :ref:`message_protocol_message_header`
    - 8
    - Message ID = ``4``
  * - :ref:`Timestamp <message_protocol_timestamp>`
    - 8
    - Unsigned integer, message timestamp
  * - :ref:`Handler <message_protocol_handler>`
    - 4
    - Unsigned integer, identifies the stream
  * - Sequence ID
    - 4
    - Unsigned integer, identifies the event sequence
  * - Event ID
    - 2
    - Unsigned integer, identifies the :ref:`event <event_id>` within the stream
  * - *Reserved*
    - 6
    -

*********************
Stream Deploy Message
*********************

The stream deploy message is used to send CAM Stream Deployment (CSD) files.

This support is mainly used for convenience as it allows updating the CSD files
in the same subsystem the **cam-service** is running without the need of a
re-deployment of the all images or even without having to reboot the systems.
This may impact security and safety aspects of the system hence this message
should not be enabled in production and should be limited to development life
cycles only.

In order to transfer a full deployment file, the sender (deployment application)
may send multiple 'chunks' of the file using multiple invocations of this
message.

Deployment Application to **cam-service**
=========================================

.. list-table::
  :header-rows: 1

  * - Field
    - Size (in bytes)
    - Description
  * - :ref:`message_protocol_message_header`
    - 8
    - Message ID = ``5``
  * - :ref:`Timestamp <message_protocol_timestamp>`
    - 8
    - Unsigned integer, message timestamp
  * - :ref:`UUID <message_protocol_uuid>`
    - 16
    - Stream UUID
  * - Attributes
    - 2
    - | Bitmap,
      | Bit [0] - Overwrite previous file
      | Bit [15:1] - *Reserved*
  * - File size in bytes
    - 2
    - | Unsigned integer, The actual maximum data file size is implementation
      | specific
  * - *Reserved*
    - 1
    -
  * - Chunk ID
    - 1
    - Unsigned integer starts from 0. Used to control the number of chunks sent
      when the file is sent via multiple parts
  * - Chunk size
    - 2
    - | · Allowed to be zero for the first chunk (when 'chunk ID' is zero)
      | · Maximum chunk size is implementation specific
      | · Chunk size is allowed to be different for each invocation of this
        message
  * - Chunk data
    - <variable>
    -

**cam-service** to Deployment Application
=========================================

.. list-table::
  :header-rows: 1

  * - Field
    - Size (in bytes)
    - Description
  * - :ref:`message_protocol_message_header`
    - 8
    - Message ID = ``4``
  * - :ref:`Timestamp <message_protocol_timestamp>`
    - 8
    - Unsigned integer, message timestamp
  * - :ref:`UUID <message_protocol_uuid>`
    - 16
    - Stream UUID, same as the UUID from the stream that requested the
      deployment
  * - Status
    - 1
    - | ``0x00`` - Success
      | ``0x01`` - Deployment message is disable. Examples include production
        mode where the deployment files are deployed directly as part
        of the firmware
      | ``0x02`` - Failed to write chunk. Cam-service failed to write the data.
        Examples include:
      |   · Storage I/O error
      |   · Out of space
      | ``0x03`` - Deployment file already exists for this stream UUID.
      |   · When the Cam Deploy Message from the application to cam-service
        indicates the file can be overwritten, this error can still happen when
        multiple deployments are detected as happening at the same time
      | ``0x04`` - Invalid file size. Examples include:
      |   · File size is zero
      |   · File size is bigger than the maximum allowed
      |   · File size is smaller than expected
      | ``0x05`` - Invalid chunk order
      | ``0x06`` - Invalid chunk size. Examples include:
      |   · The chunk size is ``0``
      |   · The chunk size exceeds the maximum allowed size
      |   · Mismatch between the message size and the chunk size
      | ``0x07`` - Exceeded maximum number of deployment sessions. This is
        returned by cam-service when it is unable to start new deployment
        sessions due to resource limitations. The number of deployment sessions
        is implementation specific.
      |
      | For any non-success status, the deployment process is cancelled and any
        data received by the cam-service is discarded
  * - *Reserved*
    - 7
    -
