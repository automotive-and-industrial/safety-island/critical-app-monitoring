..
 #
 # Critical Application Monitoring (CAM)
 #
 # SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited
 # and/or its affiliates <open-source-office@arm.com></text>
 #
 # SPDX-License-Identifier: BSD-3-Clause
 #

.. _CSD_File:

#############################
Stream Deployment File Format
#############################

The CAM Stream Deployment (CSD) data is mostly a serialized version of the
:ref:`CSC_File` which is easier to be consumed by **cam-service**.

Files should use the extension ``.csd``.

All multi-byte types are little-endian.

.. _CSD_UUID:

About the Universally Unique Identifier (UUID) fields:
  * UUIDs are 16-byte long fields representing the 128-bit UUID in binary format
  * | The file format does not enforce any UUID variant. UUIDs are used as an
    | opaque data value and does not process the sub-fields of the UUID.


**************
Header Section
**************

.. list-table::
  :header-rows: 1

  * - Field
    - Size (in bytes)
    - Description
  * - Signature
    - 4
    - ``0x59, 0x4a, 0x58, 0x46``
  * - Format version
    - 2
    - ``0x01, 0x00`` for the ``1.0`` version
  * - *Reserved*
    - 2
    -

****************
Metadata Section
****************

.. list-table::
  :header-rows: 1

  * - Field
    - Size (in bytes)
    - Description
  * - :ref:`UUID <CSD_UUID>`
    - 16
    - Stream UUID
  * - Stream name
    - 64
    - 64-byte string (including null-character)

*********************
State Control Section
*********************

.. list-table::
  :header-rows: 1

  * - Field
    - Size (in bytes)
    - Description
  * - Target timeout between init and start
    - 4
    - Unsigned integer, 0 indicates no timeout
  * - Target timeout between start and the first event
    - 4
    - Unsigned integer, cannot be zero

********************
Event Alarms Section
********************

The remaining of the data is composed by one or more entries describing the
event alarms.

.. list-table::
  :header-rows: 1

  * - Field
    - Size (in bytes)
    - Description
  * - Timeout
    - 4
    - Unsigned integer
  * - Event identifier
    - 2
    - Unsigned integer
  * - Mode
    - 1
    - | ``0x00`` - Ondemand from arrival timestamp,
      | ``0x01`` - Ondemand from application timestamp,
      | ``0x02`` - Periodic
  * - *Reserved*
    - 1
    -
