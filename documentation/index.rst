..
 #
 # Critical Application Monitoring (CAM)
 #
 # SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited
 # and/or its affiliates <open-source-office@arm.com></text>
 #
 # SPDX-License-Identifier: BSD-3-Clause
 #

###################################################
Critical Application Monitoring (CAM) Documentation
###################################################

.. toctree::
  :maxdepth: 3

  overview
  getting_started
  manual/index
  development/index
  license_link
  release_notes
