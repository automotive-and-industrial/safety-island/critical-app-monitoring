..
 #
 # Critical Application Monitoring (CAM)
 #
 # SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
 # and/or its affiliates <open-source-office@arm.com></text>
 #
 # SPDX-License-Identifier: BSD-3-Clause
 #

.. include:: ../license.rst
