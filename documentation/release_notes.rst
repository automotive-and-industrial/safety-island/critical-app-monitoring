..
 #
 # Critical Application Monitoring (CAM)
 #
 # SPDX-FileCopyrightText: <text>Copyright 2024 Arm Limited
 # and/or its affiliates <open-source-office@arm.com></text>
 #
 # SPDX-License-Identifier: BSD-3-Clause
 #

#############
Release Notes
#############

**********
Unreleased
**********

New Features
============

A new Kconfig parameter (CAM_SERVICE_CLOCK_TOLERANCE_NS) has been introduced to handle
clock tolerance in the cam-service.

****
v1.1
****

New Features
============

None.

Changed
=======

None.

Limitations
===========

Resolved and Known Issues
=========================

Resolved Issues:

* Resolved the transient timing issue observed during the processing of
  :ref:`stream_start_message` by ``cam-service`` where a timeout causes an
  unrecoverable error in cam-service, the issue is reported in
  :ref:`known issue<changelog_v1.0_known_issues>`.

Known Issues

* When cam-service is deployed as a Zephyr application
  (:ref:`CAM Integration with Kronos
  <getting_started_cam_integration_with_kronos>`), a transient timing issue
  is observed after a timeout (either stream state or temporal faults). In this
  case, the timeout causes an unrecoverable error in cam-service with the
  following error string:

  .. code-block:: console

      <err> pthread_mutex: Attempt to relock non-recursive mutex

  This issue is only seen on specific hardware and in a small subset of cases.

****
v1.0
****

New Features
============

Implementation of Critical Application Monitoring (CAM) as described in
:ref:`overview`.

Changed
=======

- Initial release.

Limitations
===========

Known Issues
============

.. _changelog_v1.0_known_issues:

* When cam-service is deployed as a Zephyr application
  (:ref:`CAM Integration with Kronos
  <getting_started_cam_integration_with_kronos>`), a transient timing issue
  is observed after a timeout (either stream state or temporal faults). In this
  case, the timeout causes an unrecoverable error in cam-service with the
  following error string:

  .. code-block:: console

      <err> pthread_mutex: Attempt to relock non-recursive mutex

  This issue is only seen on specific hardware and in a small subset of cases.

* When cam-service is deployed as a Linux or Zephyr application
  (:ref:`CAM Integration with Kronos
  <getting_started_cam_integration_with_kronos>`), a transient timing issue
  is observed during the processing of
  :ref:`stream_start_message` by ``cam-service``. In this case, the timeout
  causes an unrecoverable error in cam-service with the following error string:

  .. code-block:: console

      ERROR: Stream state timeout error:
      ERROR: stream_name: CAM STREAM 1
      ERROR: stream_uuid: 84085ddc-bc10-11ed-9a44-7ef9696e0001
      ERROR: error: state start to event timeout

  This issue is only seen in a small subset of cases.
