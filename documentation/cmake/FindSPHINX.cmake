#
# Critical Application Monitoring (CAM)
#
# SPDX-FileCopyrightText: <text>Copyright 2023
#   Arm Limited and/or its affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause
#

#
# CMake module to find Sphinx
#
# This module defines SPHINX_FOUND YES if Sphinx has been found.
# This module will set the following variables:
#
# SPHINX_FOUND
#   Set to TRUE/YES if Sphinx has been found. Otherwise, set to FALSE/NO.
#
# SPHINX_BUILD
#   Full path to the sphinx-build binary. Only valid when SPHINX_FOUND is TRUE.
#

find_program(SPHINX_BUILD
    NAMES sphinx-build
    DOC "Full path to the sphinx-build binary.")

include(FindPackageHandleStandardArgs)

find_package_handle_standard_args(SPHINX
    DEFAULT_MSG
    SPHINX_BUILD)
