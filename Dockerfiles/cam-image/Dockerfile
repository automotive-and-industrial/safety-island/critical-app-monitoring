# SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: BSD-3-Clause

ARG DOCKERHUB_MIRROR=

FROM ${DOCKERHUB_MIRROR}ubuntu:focal

RUN apt-get update \
    && apt-get install -y locales \
    && localedef -i en_US -c -f UTF-8 -A /usr/share/locale/locale.alias en_US.UTF-8
ENV LANG=en_US.utf8
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get install -y --no-install-recommends \
    build-essential \
    cmake \
    doxygen \
    git \
    pkg-config \
    python3 \
    python3-venv \
    python3-pip \
    libcunit1-dev \
    tcpdump \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

CMD ["/bin/bash"]
