<!--
#
# Critical Application Monitoring (CAM)
#
# SPDX-FileCopyrightText: <text>Copyright 2024
#   Arm Limited and/or its affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause
#
-->

# Dockerfiles

Build a docker image using the _Dockerfile_ located under _cam-image_ or
_cam-image-zephyr_:

```
cd <folder> # cam-image or cam-image-zephyr
docker build -t cam:v1 .
```

Create and run a new container from the image created:

```
docker run -it --name cam cam:v1
```

In order to gain another terminal to the running container:

```
docker exec -it cam bash
```
