/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef CAM_UUID_H
#define CAM_UUID_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <stdint.h>

#define CAM_UUID_STR_LEN 37
#define CAM_UUID_LEN     16

#define CAM_UUID_SUCCESS    0
#define CAM_UUID_E_PARAM    -1
#define CAM_UUID_E_FORMAT   -2
#define CAM_UUID_E_MISMATCH -3

typedef uint8_t cam_uuid_t[CAM_UUID_LEN];

int cam_uuid_to_string(const cam_uuid_t uuid, char *output);
int cam_uuid_from_string(const char *str, cam_uuid_t uuid);
void cam_uuid_copy(cam_uuid_t dest, const cam_uuid_t source);
int cam_uuid_compare(const cam_uuid_t uuid1, const cam_uuid_t uuid2);

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* CAM_UUID_H */
