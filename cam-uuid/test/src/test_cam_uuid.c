/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "test_suite.h"

#include <CUnit/Basic.h>

#include <cam_uuid.h>

#include <stdlib.h>

#define TEST_CAM_UUID_STR1         "a4085ddc-bc10-11ed-9a44-7ef9696e9dd3"
#define TEST_CAM_UUID_STR2         "94085ddc-bc10-11ed-9a44-7ef9696e9dd3"
#define TEST_CAM_UUID_INVALID_STR1 "ZZ085ddc-bc10-11ed-9a44-7ef9696e9dd3"
#define TEST_CAM_UUID_INVALID_STR2 "a4085ddc-bc10"
#define TEST_CAM_UUID_INVALID_STR3 "94085ddc-bc10-11ed-9a44-7ef9696e9dd3-a1"

static int test_cam_uuid_suite_init(void)
{
	return 0;
}

static int test_cam_uuid_suite_clean(void)
{
	return 0;
}

static void test_cam_uuid_to_from_string(void)
{
	int ret;
	cam_uuid_t uuid;
	char uuid_str[CAM_UUID_STR_LEN];

	ret = cam_uuid_from_string(NULL, uuid);
	CU_ASSERT(ret == CAM_UUID_E_PARAM);

	ret = cam_uuid_from_string(TEST_CAM_UUID_INVALID_STR1, uuid);
	CU_ASSERT(ret == CAM_UUID_E_FORMAT);

	ret = cam_uuid_from_string(TEST_CAM_UUID_INVALID_STR2, uuid);
	CU_ASSERT(ret == CAM_UUID_E_FORMAT);

	ret = cam_uuid_from_string(TEST_CAM_UUID_INVALID_STR3, uuid);
	CU_ASSERT(ret == CAM_UUID_E_FORMAT);

	ret = cam_uuid_to_string(uuid, NULL);
	CU_ASSERT(ret == CAM_UUID_E_PARAM);

	ret = cam_uuid_from_string(TEST_CAM_UUID_STR1, uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	ret = cam_uuid_to_string(uuid, uuid_str);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	CU_ASSERT(strcmp(uuid_str, TEST_CAM_UUID_STR1) == 0);
}

static void test_cam_uuid_copy(void)
{
	int ret;
	cam_uuid_t uuid, uuid2;
	char uuid_str[CAM_UUID_STR_LEN];

	ret = cam_uuid_from_string(TEST_CAM_UUID_STR1, uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	cam_uuid_copy(uuid2, uuid);

	ret = cam_uuid_to_string(uuid2, uuid_str);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	CU_ASSERT(strcmp(uuid_str, TEST_CAM_UUID_STR1) == 0);
}

static void test_cam_uuid_compare(void)
{
	int ret;
	cam_uuid_t uuid, uuid2;

	ret = cam_uuid_from_string(TEST_CAM_UUID_STR1, uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	cam_uuid_copy(uuid2, uuid);

	ret = cam_uuid_compare(uuid, uuid2);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	ret = cam_uuid_from_string(TEST_CAM_UUID_STR2, uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	ret = cam_uuid_compare(uuid, uuid2);
	CU_ASSERT(ret == CAM_UUID_E_MISMATCH);
}

static CU_TestInfo tests_cam_uuid[] = {
	{ "UUID to_string from_string", test_cam_uuid_to_from_string },
	{ "UUID copy", test_cam_uuid_copy },
	{ "UUID compare", test_cam_uuid_compare },
	CU_TEST_INFO_NULL,
};

static CU_SuiteInfo suites[] = {
	{ "uuid",
	  test_cam_uuid_suite_init,
	  test_cam_uuid_suite_clean,
	  NULL,
	  NULL,
	  tests_cam_uuid },
	CU_SUITE_INFO_NULL,
};

int main(int argc, char *argv[])
{
	(void)argc;
	(void)argv;

	return test_suite_run(suites);
}
