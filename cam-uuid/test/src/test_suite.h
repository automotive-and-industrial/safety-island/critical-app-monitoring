/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef TEST_SUITE_H
#define TEST_SUITE_H

#include <CUnit/Basic.h>

int test_suite_run(CU_SuiteInfo suites[]);

#endif /* TEST_SUITE_H */
