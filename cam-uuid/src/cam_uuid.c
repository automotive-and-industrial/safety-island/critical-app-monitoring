/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <cam_uuid.h>

#include <assert.h>
#include <ctype.h>
#include <stddef.h>
#include <string.h>

int cam_uuid_to_string(const cam_uuid_t uuid, char *output)
{
	unsigned int i;
	const char hex[] = "0123456789abcdef";

	if (output == NULL) {
		return CAM_UUID_E_PARAM;
	}

	for (i = 0; i < CAM_UUID_LEN; i++) {
		/* Generating UUID based on the following format
		 * 32 hexadecimal characters with four hyphens:
		 * XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX */
		if ((i == 4) || (i == 6) || (i == 8) || (i == 10)) {
			*output++ = '-';
		}

		*output++ = hex[uuid[i] >> 4];
		*output++ = hex[uuid[i] & 0x0f];
	}

	*output = '\0';

	return CAM_UUID_SUCCESS;
}

static int hex_to_int(const char c)
{
	int value = 0;

	assert(isxdigit(c) != 0);

	if (c <= '9')
		value = c - '0';
	else if (c <= 'F')
		value = c - 'A' + 10;
	else
		value = c - 'a' + 10;

	return value;
}

static int str_to_byte(const char str[2], uint8_t *byte)
{
	/* Checking for hex input before calling hex_to_int */
	if ((isxdigit(str[0]) == 0) || (isxdigit(str[1]) == 0))
		return CAM_UUID_E_FORMAT;

	assert(byte != NULL);

	*byte = hex_to_int(str[0]) << 4;
	*byte |= hex_to_int(str[1]);

	return CAM_UUID_SUCCESS;
}

int cam_uuid_from_string(const char *uuid_str, cam_uuid_t uuid)
{
	unsigned int i, j;
	uint8_t *uuid_p = uuid;
	const char *s = uuid_str;

	if (s == NULL) {
		return CAM_UUID_E_PARAM;
	}

	/* Expecting a NULL terminated string at CAM_UUID_STR_LEN */
	if (*(s + (CAM_UUID_STR_LEN - 1)) != '\0') {
		return CAM_UUID_E_FORMAT;
	}

	/* Converting UUID based on the following format
	 * 32 hexadecimal characters with four hyphens:
	 * XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX */
	for (i = 0; i < 4; i++) {
		if (str_to_byte(s, uuid_p) != CAM_UUID_SUCCESS)
			return CAM_UUID_E_FORMAT;
		s += 2;
		uuid_p++;
	}

	if (*s++ != '-') {
		return CAM_UUID_E_FORMAT;
	}

	for (j = 0; j < 3; j++) {
		for (i = 0; i < 2; i++) {
			if (str_to_byte(s, uuid_p) != CAM_UUID_SUCCESS)
				return CAM_UUID_E_FORMAT;
			s += 2;
			uuid_p++;
		}

		if (*s++ != '-') {
			return CAM_UUID_E_FORMAT;
		}
	}

	for (i = 0; i < 6; i++) {
		if (str_to_byte(s, uuid_p) != CAM_UUID_SUCCESS)
			return CAM_UUID_E_FORMAT;
		s += 2;
		uuid_p++;
	}

	return CAM_UUID_SUCCESS;
}

void cam_uuid_copy(cam_uuid_t dest, const cam_uuid_t source)
{
	memcpy(dest, source, CAM_UUID_LEN);
}

int cam_uuid_compare(const cam_uuid_t uuid1, const cam_uuid_t uuid2)
{
	if (memcmp(uuid1, uuid2, CAM_UUID_LEN) == 0)
		return CAM_UUID_SUCCESS;
	else
		return CAM_UUID_E_MISMATCH;
}
