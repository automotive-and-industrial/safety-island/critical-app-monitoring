/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

/*!
 * @file cam.h
 * Source code for the CAM library.
 */

#ifndef CAM_H
#define CAM_H

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#include <pthread.h>

#include <cam_uuid.h>
#include <cam_version.h>

#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

/*!
 * \defgroup libcam CAM Library
 *
 * \details The CAM library API implements a number of functions used to
 * communicate with the CAM Service.
 *
 * \{
 */

/*!
 * \brief Default CAM service port
 *
 * \details This macro defines the default port CAM Service will be listening
 * on.
 */
#define CAM_DEFAULT_PORT 21604

/*!
 * \brief The length of stream name
 *
 * \details The macro defines the length of stream name including the \0
 * terminator.
 */
#define CAM_STREAM_NAME_LEN 64

/*!
 * \brief The error return value of CAM API
 *
 * \details The error return value if an error occurs in CAM library API.
 */
#define CAM_ERROR -1

/*!
 * \brief The success return value of CAM API
 *
 * \details The success return value if no error occurs in CAM library API.
 */
#define CAM_SUCCESS 0

/*! @cond */
struct cam_ctx {
	int socket_fd;
	pthread_mutex_t socket_mutex;
};
/*! @endcond */

/*!
 * \brief CAM context
 *
 * \details The CAM context is an opaque data type used by the CAM library to
 * identify a CAM connection.
 */
typedef struct cam_ctx cam_ctx_t;

/*!
 * \brief CAM stream configuration descriptor for monitor mode
 *
 * \details The CAM stream configuration descriptor is used to initialize the
 * event stream.
 */
struct cam_stream_config {
	/*!
	 * \brief Stream name
	 *
	 * \details Stream name (64 characters including the \0 terminator). The
	 * name is used to identify the stream in different contexts like in log
	 * files and deployment assets. It may be an empty string.
	 */
	char name[CAM_STREAM_NAME_LEN];

	/*!
	 * \brief Stream UUID string
	 *
	 * \details Uniquely identify a stream. Must be in the form
	 * "1b4e28ba-2fa1-11d2-883f-b9a761bde3fb".
	 */
	char *uuid_str;

	/*!
	 * \brief Context descriptor
	 *
	 * \details Context descriptor with the data for opening a connection with
	 * a CAM Service.
	 */
	cam_ctx_t *ctx;
};

/*!
 * \brief CAM stream configuration descriptor for calibration mode
 *
 * \details The CAM stream configuration descriptor is used to initialize the
 * event stream in calibration mode.
 */
struct cam_stream_cal_config {
	/*!
	 * \brief Stream event log file name
	 *
	 * \details Stream event log (CSEL) file name. The event log data will be
	 * saved to this file. If this variable is NULL, the file name will be
	 * {uuid}.csel in default.
	 */
	char *file_name;

	/*!
	 * \brief Timeout between init and start
	 *
	 * \details Timeout between the stream init and its first start.
	 * 0 indicates no timeout.
	 */
	unsigned int timeout_i2s;

	/*!
	 * \brief Timeout between start and first event
	 *
	 * \details Timeout between the stream start and its first event. It is
	 * mandatory and cannot be zero.
	 */
	unsigned int timeout_s2e;
};

/*! @cond */
struct cam_stream_ctx {
	struct cam_stream_config *config;
	struct cam_stream_cal_config *cal_config;
	int socket_fd;
	FILE *csel_fp;
	cam_uuid_t uuid;
	uint32_t handler_id;
	uint32_t sequence_id;
	pthread_mutex_t *socket_mutex;
	pthread_mutex_t csel_mutex;
};
/*! @endcond */

/*!
 * \brief Stream context
 *
 * \details The Stream context is an opaque data type used by the CAM library to
 * identify an initialized Stream.
 */
typedef struct cam_stream_ctx cam_stream_ctx_t;

/*!
 * \brief Retrieve the library version.
 *
 * \details  This function retrieves the current version of the library.
 *
 * \param[out] major Pointer to the major version number.
 *
 * \param[out] minor Pointer to the minor version number.
 *
 * \retval CAM_SUCCESS The version is retrieved with success.
 * \retval CAM_ERROR Failed to retrieve the library version. The returned major
 * and minor are invalid.
 */
int cam_version(unsigned int *major, unsigned int *minor);

/*!
 * \brief Initialize the CAM connection.
 *
 * \details This function initializes the connection with CAM service. It
 * can be called multiple times by an application to initialize a cam_ctx_t
 * descriptor used by further API functions that operate on the connection
 * level. It must be called before any other CAM library function that requires
 * the cam_ctx_t.
 *
 * \param[out] ctx Output context to be used with other functions in the CAM
 * library.
 *
 * \param service_address String containing the CAM Service IP (v4).
 *
 * \param service_port String containing the networking port the CAM Service is
 * listening. This maybe \ref CAM_DEFAULT_PORT or any port the CAM Service was
 * configure with.
 *
 * \retval CAM_SUCCESS The connection initialization succeeded.
 * \retval CAM_ERROR Failed to initialize the CAM connection. The returned
 * content in the ctx is invalid.
 */
int cam_init(
	cam_ctx_t *ctx,
	const char *service_address,
	unsigned int service_port);

/*!
 * \brief Initialize the event stream.
 *
 * \details This function initializes the event stream and must be called
 * before any other CAM library function that requires a stream context. It is
 * responsible for internal data structure initialization for a stream.
 *
 * \param[out] stream_ctx Output stream context to be used with other functions
 * in the CAM library.
 *
 * \param config Pointer to a configuration descriptor.
 *
 * \param cal_config Pointer to a configuration descriptor for calibration mode.
 * If the value is NULL, the calibration will not be enabled.
 *
 * \retval CAM_SUCCESS The stream initialization succeeded.
 * \retval CAM_ERROR Failed to initialize the stream. The returned content in
 * the ctx is invalid.
 */
int cam_stream_init(
	cam_stream_ctx_t *stream_ctx,
	struct cam_stream_config *config,
	struct cam_stream_cal_config *cal_config);

/*!
 * \brief Mark the start of an event stream
 *
 * \details  This function indicates to the CAM Service an event stream is about
 * to start.
 *
 * \param ctx Pointer to a stream context the 'stream' relates to.
 *
 * \retval CAM_SUCCESS The start message was sent with success.
 * \retval CAM_ERROR Failed to send the start message.
 */
int cam_stream_start(cam_stream_ctx_t *ctx);

/*!
 * \brief Send an event message
 *
 * \details  This function sends an event message to the CAM Service.
 *
 * \param ctx Pointer to the stream context the 'stream' relates to.
 *
 * \param event_id The unique event identifier within the stream.
 *
 * \retval CAM_SUCCESS The event message was sent with success.
 * \retval CAM_ERROR Failed to send the event message.
 */
int cam_stream_event(cam_stream_ctx_t *ctx, uint16_t event_id);

/*!
 * \brief Indicate to the CAM Service the event stream stopped.
 *
 * \details  This function sends an stop message to the CAM Service to indicate
 * the event stream stopped.
 *
 * \param ctx Pointer to the stream context the 'stream' relates to.
 *
 * \retval CAM_SUCCESS The stop message was sent with success.
 * \retval CAM_ERROR Failed to stop the stream.
 */
int cam_stream_stop(cam_stream_ctx_t *ctx);

/*!
 * \brief Terminate the event stream.
 *
 * \details Terminate the event stream. It is responsible for internal data
 * structure destruction for a stream.
 *
 * \param ctx Pointer to the stream context the 'stream' relates to.
 *
 * \retval CAM_SUCCESS The stream was terminated with success.
 * \retval CAM_ERROR Failed to terminate the stream.
 */
int cam_stream_end(cam_stream_ctx_t *ctx);

/*!
 * \brief Terminate the CAM connection.
 *
 * \details Terminate the CAM connection. The connection with the CAM service
 * will be closed.
 *
 * \param ctx Pointer to the CAM context this call relates to.
 *
 * \retval CAM_SUCCESS The CAM connection was terminated with success.
 * \retval CAM_ERROR Failed to terminate the CAM connection.
 */
int cam_end(cam_ctx_t *ctx);

/*!
 * \}
 */

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* CAM_H */
