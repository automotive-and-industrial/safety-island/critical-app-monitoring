/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef CAM_CSEL
#define CAM_CSEL

#include <cam.h>

#include <stdint.h>

#define CAM_CSEL_VERSION_MAJOR 1
#define CAM_CSEL_VERSION_MINOR 0

static const uint8_t cam_csel_signature[4] = { 0x4d, 0x46, 0x4d, 0x4e };

enum cam_csel_entry_id {
	CAM_CSEL_ENTRY_ID_START = 1,
	CAM_CSEL_ENTRY_ID_STOP,
	CAM_CSEL_ENTRY_ID_END,
	CAM_CSEL_ENTRY_ID_EVENT,
	CAM_CSEL_ENTRY_ID_COUNT,
};

struct __attribute((packed, aligned(8))) cam_csel_header {
	/* CSEL file signature */
	uint8_t signature[4];
	/* CSEL format major version */
	uint8_t version_major;
	/* CSEL format minor version */
	uint8_t version_minor;
	/* Reserved */
	uint16_t padding;
};

struct __attribute((packed, aligned(8))) cam_csel_metadata {
	/* Stream identification */
	cam_uuid_t uuid;
	/* Stream name */
	uint8_t name[CAM_STREAM_NAME_LEN];
	/* Metadata create timestamp */
	uint64_t timestamp;
};

struct __attribute((packed, aligned(8))) cam_csel_state_ctrl {
	/* Target timeout between init and start */
	uint32_t init_to_start_timeout;
	/* Target timeout between start and first event */
	uint32_t start_to_event_timeout;
};

/* Entry structure for start, stop and end */
struct __attribute((packed, aligned(1))) cam_csel_common_entry {
	/* Section identifier */
	uint8_t entry_id;
	/* Timestamp of the entry */
	uint64_t timestamp;
};

struct __attribute((packed, aligned(1))) cam_csel_event_entry {
	/* Section identifier */
	uint8_t entry_id;
	/* Timestamp of the entry */
	uint64_t timestamp;
	/* Sequence identifier */
	uint32_t sequence_id;
	/* Event identifier */
	uint16_t event_id;
	/* Reserved */
	uint16_t padding;
};

#endif /* CAM_CSEL */
