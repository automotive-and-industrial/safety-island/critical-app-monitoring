/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "cam_csel.h"

#include <arpa/inet.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <unistd.h>

#include <cam.h>
#include <cam_message.h>

#include <assert.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <time.h>

#define CAM_EVENT_LOG_FILE_EXTENSION ".csel"

static uint64_t htonll(uint64_t n)
{
	return (htonl(1) == 1) ? n : ((uint64_t)htonl(n) << 32) | htonl(n >> 32);
}

static int cam_timestamp_now(uint64_t *timestamp)
{
	struct timespec tp;

	assert(timestamp);

	if (clock_gettime(CLOCK_REALTIME, &tp) == -1) {
		return CAM_ERROR;
	}

	/* Convert timespec to microseconds */
	*timestamp = (((uint64_t)tp.tv_sec) * 1000000) + (tp.tv_nsec / 1000);

	return CAM_SUCCESS;
}

/*
 * This call disables TCP Delayed Ack feature, it needs to be called after every
 * send and receive because the flag resets after these events.
 */
static int disable_delayed_tcp_ack(int socket_fd)
{
	const int enable = 1;

	return setsockopt(
		socket_fd, IPPROTO_TCP, TCP_QUICKACK, &enable, sizeof(enable));
}

static int cam_msg_recv(int fd, uint8_t *buffer, uint16_t size)
{
	unsigned int bytes_recv = 0u;

	/* Check if received data covers a complete message */
	while (bytes_recv < size) {
		ssize_t count = recv(fd, buffer + bytes_recv, size - bytes_recv, 0);
		if ((disable_delayed_tcp_ack(fd) != 0) || (count <= 0)) {
			return CAM_ERROR;
		}

		bytes_recv += count;
	}

	if (bytes_recv > size) {
		return CAM_ERROR;
	}

	return CAM_SUCCESS;
}

static int cam_msg_send(int fd, uint8_t *buffer, uint16_t size)
{
	unsigned int bytes_sent = 0u;

	/* Check if sent the complete message */
	while (bytes_sent < size) {
		ssize_t count =
			send(fd, buffer + bytes_sent, size - bytes_sent, MSG_NOSIGNAL);
		if ((disable_delayed_tcp_ack(fd) != 0) || (count < 0)) {
			return CAM_ERROR;
		}

		bytes_sent += count;
	}

	if (bytes_sent > size) {
		return CAM_ERROR;
	}

	return CAM_SUCCESS;
}

static int cam_msg_header_init(
	struct cam_msg_header *header,
	enum cam_msg_a2s_id msg_id)
{
	static const uint16_t msg_size[CAM_MSG_A2S_ID_COUNT] = {
		0,
		sizeof(struct cam_msg_a2s_init),
		sizeof(struct cam_msg_a2s_start),
		sizeof(struct cam_msg_a2s_stop),
		sizeof(struct cam_msg_a2s_event),
	};

	if (msg_id >= CAM_MSG_A2S_ID_COUNT || msg_id == 0) {
		return CAM_ERROR;
	}

	header->version_major = CAM_MSG_VERSION_MAJOR;
	header->version_minor = CAM_MSG_VERSION_MINOR;
	header->msg_id = htons(msg_id);
	header->size = htons(msg_size[msg_id]);

	return CAM_SUCCESS;
}

static int cam_msg_header_check(struct cam_msg_header *header)
{
	if ((header->version_major != CAM_MSG_VERSION_MAJOR) ||
		(header->version_minor != CAM_MSG_VERSION_MINOR)) {
		return CAM_ERROR;
	}

	switch (ntohs(header->msg_id)) {
	case CAM_MSG_S2A_ID_INIT:
		if (ntohs(header->size) != sizeof(struct cam_msg_s2a_init)) {
			return CAM_ERROR;
		}
		break;
	default:
		return CAM_ERROR;
	}

	return CAM_SUCCESS;
}

int cam_init(
	cam_ctx_t *ctx,
	const char *service_address,
	unsigned int service_port)
{
	struct sockaddr_in addr = { 0 };
	const int enable_flag = 1;
	const int tos = IPTOS_LOWDELAY;

	if (!ctx || !service_address) {
		return CAM_ERROR;
	}

	memset(ctx, 0, sizeof(*ctx));

	ctx->socket_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (ctx->socket_fd == -1) {
		return CAM_ERROR;
	}

	addr.sin_family = AF_INET;
	addr.sin_port = htons(service_port);
	addr.sin_addr.s_addr = inet_addr(service_address);

	/* Set low delay priority as Type Of Service */
	if (setsockopt(ctx->socket_fd, IPPROTO_IP, IP_TOS, &tos, sizeof(tos)) < 0) {
		return CAM_ERROR;
	}

	/* Disable Nagle's algorithm */
	if (setsockopt(
			ctx->socket_fd,
			IPPROTO_TCP,
			TCP_NODELAY,
			&enable_flag,
			sizeof(enable_flag)) < 0) {
		return CAM_ERROR;
	}

	if (disable_delayed_tcp_ack(ctx->socket_fd) < 0) {
		return CAM_ERROR;
	}

	if (connect(ctx->socket_fd, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
		return CAM_ERROR;
	}

	if (pthread_mutex_init(&ctx->socket_mutex, NULL) != 0) {
		return CAM_ERROR;
	}

	return CAM_SUCCESS;
}

static int cam_stream_cal_init(
	cam_stream_ctx_t *stream_ctx,
	struct cam_stream_config *config,
	struct cam_stream_cal_config *cal_config,
	uint64_t timestamp)
{
	struct cam_csel_header header;
	struct cam_csel_metadata metadata;
	struct cam_csel_state_ctrl state_ctrl;
	int len_fn = CAM_UUID_STR_LEN + strlen(CAM_EVENT_LOG_FILE_EXTENSION);
	char default_file_name[len_fn];
	char *file_name = cal_config->file_name;
	size_t ret_fp;
	int ret = CAM_SUCCESS;

	if (pthread_mutex_init(&stream_ctx->csel_mutex, NULL) != 0) {
		return CAM_ERROR;
	}

	if (pthread_mutex_lock(&stream_ctx->csel_mutex) != 0) {
		return CAM_ERROR;
	}

	if (file_name == NULL) {
		snprintf(
			default_file_name,
			len_fn,
			"%s%s",
			config->uuid_str,
			CAM_EVENT_LOG_FILE_EXTENSION);
		file_name = default_file_name;
	}

	stream_ctx->csel_fp = fopen(file_name, "wb");
	if (stream_ctx->csel_fp == NULL) {
		ret = CAM_ERROR;
		goto exit_mutex;
	}

	stream_ctx->cal_config = cal_config;

	stream_ctx->socket_fd = -1;
	stream_ctx->handler_id = 0;
	stream_ctx->socket_mutex = NULL;

	memcpy(header.signature, cam_csel_signature, sizeof(header.signature));
	header.version_major = CAM_CSEL_VERSION_MAJOR;
	header.version_minor = CAM_CSEL_VERSION_MINOR;
	header.padding = 0;

	ret_fp =
		fwrite(&header, sizeof(struct cam_csel_header), 1, stream_ctx->csel_fp);
	if (ret_fp != 1) {
		ret = CAM_ERROR;
		goto exit_fp;
	}

	cam_uuid_copy(metadata.uuid, stream_ctx->uuid);
	memcpy(metadata.name, config->name, sizeof(metadata.name));
	metadata.timestamp = htole64(timestamp);

	ret_fp = fwrite(
		&metadata, sizeof(struct cam_csel_metadata), 1, stream_ctx->csel_fp);
	if (ret_fp != 1) {
		ret = CAM_ERROR;
		goto exit_fp;
	}

	state_ctrl.init_to_start_timeout = htole32(cal_config->timeout_i2s);
	state_ctrl.start_to_event_timeout = htole32(cal_config->timeout_s2e);

	ret_fp = fwrite(
		&state_ctrl,
		sizeof(struct cam_csel_state_ctrl),
		1,
		stream_ctx->csel_fp);
	if (ret_fp != 1) {
		ret = CAM_ERROR;
	}

exit_fp:
	if (ret == CAM_ERROR) {
		fclose(stream_ctx->csel_fp);
		stream_ctx->csel_fp = NULL;
	}

exit_mutex:
	if (pthread_mutex_unlock(&stream_ctx->csel_mutex) != 0) {
		ret = CAM_ERROR;
	}

	return ret;
}

int cam_stream_init(
	cam_stream_ctx_t *stream_ctx,
	struct cam_stream_config *config,
	struct cam_stream_cal_config *cal_config)
{
	struct cam_msg_a2s_init send_msg;
	struct cam_msg_s2a_init recv_msg;
	uint8_t *p_send = (uint8_t *)&send_msg;
	uint8_t *p_recv = (uint8_t *)&recv_msg;
	uint64_t timestamp;
	int ret = CAM_SUCCESS;

	if ((stream_ctx == NULL) || (config == NULL)) {
		return CAM_ERROR;
	}

	ret = cam_timestamp_now(&timestamp);
	if (ret != CAM_SUCCESS) {
		return CAM_ERROR;
	}

	stream_ctx->config = config;
	stream_ctx->sequence_id = 0;

	if (cam_uuid_from_string(config->uuid_str, stream_ctx->uuid) !=
		CAM_UUID_SUCCESS) {
		return CAM_ERROR;
	}

	if (cal_config != NULL) {
		return cam_stream_cal_init(stream_ctx, config, cal_config, timestamp);
	}

	if (config->ctx == NULL) {
		return CAM_ERROR;
	}

	if (pthread_mutex_lock(&config->ctx->socket_mutex) != 0) {
		return CAM_ERROR;
	}

	stream_ctx->socket_fd = config->ctx->socket_fd;
	stream_ctx->socket_mutex = &config->ctx->socket_mutex;

	stream_ctx->cal_config = NULL;
	stream_ctx->csel_fp = NULL;

	ret = cam_msg_header_init(&send_msg.header, CAM_MSG_A2S_ID_INIT);
	if (ret != CAM_SUCCESS) {
		goto exit;
	}

	send_msg.timestamp = htonll(timestamp);
	cam_uuid_copy(send_msg.uuid, stream_ctx->uuid);

	ret = cam_msg_send(stream_ctx->socket_fd, p_send, sizeof(send_msg));
	if (ret != CAM_SUCCESS) {
		goto exit;
	}

	ret = cam_msg_recv(stream_ctx->socket_fd, p_recv, sizeof(recv_msg));
	if (ret != CAM_SUCCESS) {
		goto exit;
	}

	ret = cam_msg_header_check((struct cam_msg_header *)p_recv);
	if (ret != CAM_SUCCESS) {
		goto exit;
	}

	if (cam_uuid_compare(recv_msg.uuid, stream_ctx->uuid)) {
		ret = CAM_ERROR;
		goto exit;
	}

	if (ntohs(recv_msg.status) != CAM_MSG_S2A_INIT_STATUS_SUCCESS) {
		ret = CAM_ERROR;
		goto exit;
	}

	stream_ctx->handler_id = ntohl(recv_msg.handler_id);

exit:
	if (pthread_mutex_unlock(&config->ctx->socket_mutex) != 0) {
		return CAM_ERROR;
	}
	return ret;
}

static int cam_stream_cal_start(cam_stream_ctx_t *ctx, uint64_t timestamp)
{
	struct cam_csel_common_entry entry;
	FILE *csel_fp = ctx->csel_fp;
	size_t ret_fp;
	int ret = CAM_SUCCESS;

	if (csel_fp == NULL) {
		return CAM_ERROR;
	}

	if (pthread_mutex_lock(&ctx->csel_mutex) != 0) {
		return CAM_ERROR;
	}

	ctx->sequence_id = 0;

	entry.entry_id = CAM_CSEL_ENTRY_ID_START;
	entry.timestamp = htole64(timestamp);

	ret_fp = fwrite(&entry, sizeof(struct cam_csel_common_entry), 1, csel_fp);
	if (ret_fp != 1) {
		ret = CAM_ERROR;
	}

	if (pthread_mutex_unlock(&ctx->csel_mutex) != 0) {
		ret = CAM_ERROR;
	}

	return ret;
}

int cam_stream_start(cam_stream_ctx_t *ctx)
{
	struct cam_msg_a2s_start send_msg;
	uint64_t timestamp;
	int ret = CAM_SUCCESS;

	if (ctx == NULL) {
		return CAM_ERROR;
	}

	if (cam_timestamp_now(&timestamp) != CAM_SUCCESS) {
		return CAM_ERROR;
	}

	if (ctx->cal_config != NULL) {
		return cam_stream_cal_start(ctx, timestamp);
	}

	if (cam_msg_header_init(&send_msg.header, CAM_MSG_A2S_ID_START) !=
		CAM_SUCCESS) {
		return CAM_ERROR;
	}

	send_msg.timestamp = htonll(timestamp);
	send_msg.handler_id = htonl(ctx->handler_id);

	if (pthread_mutex_lock(ctx->socket_mutex) != 0) {
		return CAM_ERROR;
	}

	ctx->sequence_id = 0;

	if (cam_msg_send(ctx->socket_fd, (uint8_t *)&send_msg, sizeof(send_msg)) !=
		CAM_SUCCESS) {
		ret = CAM_ERROR;
	}

	if (pthread_mutex_unlock(ctx->socket_mutex) != 0) {
		return CAM_ERROR;
	}

	return ret;
}

static int cam_stream_cal_event(
	cam_stream_ctx_t *ctx,
	uint16_t event_id,
	uint64_t timestamp)
{
	struct cam_csel_event_entry entry;
	FILE *csel_fp = ctx->csel_fp;
	size_t ret_fp;
	int ret = CAM_SUCCESS;

	if (csel_fp == NULL) {
		return CAM_ERROR;
	}

	if (pthread_mutex_lock(&ctx->csel_mutex) != 0) {
		return CAM_ERROR;
	}

	entry.entry_id = CAM_CSEL_ENTRY_ID_EVENT;
	entry.timestamp = htole64(timestamp);
	entry.sequence_id = htole32(ctx->sequence_id++);
	entry.event_id = htole16(event_id);
	entry.padding = 0;

	ret_fp = fwrite(&entry, sizeof(struct cam_csel_event_entry), 1, csel_fp);
	if (ret_fp != 1) {
		ret = CAM_ERROR;
	}

	if (pthread_mutex_unlock(&ctx->csel_mutex) != 0) {
		ret = CAM_ERROR;
	}

	return ret;
}

int cam_stream_event(cam_stream_ctx_t *ctx, uint16_t event_id)
{
	struct cam_msg_a2s_event send_msg;
	uint64_t timestamp;
	int ret = CAM_SUCCESS;

	if (ctx == NULL) {
		return CAM_ERROR;
	}

	if (cam_timestamp_now(&timestamp) != CAM_SUCCESS) {
		return CAM_ERROR;
	}

	if (ctx->cal_config != NULL) {
		return cam_stream_cal_event(ctx, event_id, timestamp);
	}

	if (cam_msg_header_init(&send_msg.header, CAM_MSG_A2S_ID_EVENT) !=
		CAM_SUCCESS) {
		return CAM_ERROR;
	}

	send_msg.timestamp = htonll(timestamp);
	send_msg.handler_id = htonl(ctx->handler_id);
	send_msg.event_id = htons(event_id);
	send_msg.sequence_id = htonl(ctx->sequence_id);

	if (pthread_mutex_lock(ctx->socket_mutex) != 0) {
		return CAM_ERROR;
	}

	if (cam_msg_send(ctx->socket_fd, (uint8_t *)&send_msg, sizeof(send_msg)) !=
		CAM_SUCCESS) {
		ret = CAM_ERROR;
	} else {
		ctx->sequence_id++;
	}

	if (pthread_mutex_unlock(ctx->socket_mutex) != 0) {
		ret = CAM_ERROR;
	}
	return ret;
}

static int cam_stream_cal_stop(cam_stream_ctx_t *ctx, uint64_t timestamp)
{
	struct cam_csel_common_entry entry;
	FILE *csel_fp = ctx->csel_fp;
	size_t ret_fp;
	int ret = CAM_SUCCESS;

	if (csel_fp == NULL) {
		return CAM_ERROR;
	}

	if (pthread_mutex_lock(&ctx->csel_mutex) != 0) {
		return CAM_ERROR;
	}

	entry.entry_id = CAM_CSEL_ENTRY_ID_STOP;
	entry.timestamp = htole64(timestamp);

	ret_fp = fwrite(&entry, sizeof(struct cam_csel_common_entry), 1, csel_fp);
	if (ret_fp != 1) {
		ret = CAM_ERROR;
	}

	if (pthread_mutex_unlock(&ctx->csel_mutex) != 0) {
		ret = CAM_ERROR;
	}

	return ret;
}

int cam_stream_stop(cam_stream_ctx_t *ctx)
{
	struct cam_msg_a2s_stop send_msg;
	uint64_t timestamp;
	int ret = CAM_SUCCESS;

	if (ctx == NULL) {
		return CAM_ERROR;
	}

	if (cam_timestamp_now(&timestamp) != CAM_SUCCESS) {
		return CAM_ERROR;
	}

	if (ctx->cal_config != NULL) {
		return cam_stream_cal_stop(ctx, timestamp);
	}

	if (cam_msg_header_init(&send_msg.header, CAM_MSG_A2S_ID_STOP) !=
		CAM_SUCCESS) {
		return CAM_ERROR;
	}

	send_msg.timestamp = htonll(timestamp);
	send_msg.handler_id = htonl(ctx->handler_id);

	if (pthread_mutex_lock(ctx->socket_mutex) != 0) {
		return CAM_ERROR;
	}

	if (cam_msg_send(ctx->socket_fd, (uint8_t *)&send_msg, sizeof(send_msg)) !=
		CAM_SUCCESS) {
		ret = CAM_ERROR;
	}

	if (pthread_mutex_unlock(ctx->socket_mutex) != 0) {
		ret = CAM_ERROR;
	}
	return ret;
}

static int cam_stream_cal_end(cam_stream_ctx_t *ctx, uint64_t timestamp)
{
	struct cam_csel_common_entry entry;
	FILE *csel_fp = ctx->csel_fp;
	size_t ret_fp;
	int ret = CAM_SUCCESS;

	if (csel_fp == NULL) {
		return CAM_ERROR;
	}

	if (pthread_mutex_lock(&ctx->csel_mutex) != 0) {
		return CAM_ERROR;
	}

	entry.entry_id = CAM_CSEL_ENTRY_ID_END;
	entry.timestamp = htole64(timestamp);

	ret_fp = fwrite(&entry, sizeof(struct cam_csel_common_entry), 1, csel_fp);
	if (ret_fp != 1) {
		ret = CAM_ERROR;
		goto exit_mutex;
	}

	ret_fp = fflush(csel_fp);
	if (ret_fp != 0) {
		ret = CAM_ERROR;
		goto exit_mutex;
	}

	ret_fp = fclose(csel_fp);
	if (ret_fp != 0) {
		ret = CAM_ERROR;
	}

	ctx->csel_fp = NULL;

exit_mutex:
	if (pthread_mutex_unlock(&ctx->csel_mutex) != 0) {
		ret = CAM_ERROR;
	}

	return ret;
}

int cam_stream_end(cam_stream_ctx_t *ctx)
{
	uint64_t timestamp;

	if (ctx == NULL) {
		return CAM_ERROR;
	}

	if (cam_timestamp_now(&timestamp) != CAM_SUCCESS) {
		return CAM_ERROR;
	}

	if (ctx->cal_config != NULL) {
		return cam_stream_cal_end(ctx, timestamp);
	}

	return CAM_SUCCESS;
}

int cam_end(cam_ctx_t *ctx)
{
	int ret = CAM_SUCCESS;

	if (ctx == NULL) {
		return CAM_ERROR;
	}

	if (pthread_mutex_lock(&ctx->socket_mutex) != 0) {
		return CAM_ERROR;
	}

	if (close(ctx->socket_fd) == -1) {
		ret = CAM_ERROR;
	}

	if (pthread_mutex_unlock(&ctx->socket_mutex) != 0) {
		ret = CAM_ERROR;
	}

	if (pthread_mutex_destroy(&ctx->socket_mutex) != 0) {
		ret = CAM_ERROR;
	}

	return ret;
}

int cam_version(unsigned int *major, unsigned int *minor)
{
	if ((major == NULL) || (minor == NULL)) {
		return CAM_ERROR;
	}

	*major = CAM_VERSION_MAJOR;
	*minor = CAM_VERSION_MINOR;

	return CAM_SUCCESS;
}
