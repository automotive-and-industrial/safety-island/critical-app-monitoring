/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "test_suite.h"

#include <CUnit/Automated.h>
#include <CUnit/Basic.h>
#include <unistd.h>

#include <stdlib.h>

int test_suite_run(CU_SuiteInfo suites[])
{
	int ret = 0;

	/* Initialize the CUnit test registry */
	if (CUE_SUCCESS != CU_initialize_registry()) {
		return -1;
	}

	/* Add a suite to the registry */
	if (CU_register_suites(suites) != CUE_SUCCESS) {
		ret = -1;
		goto exit;
	}

#ifdef ENABLE_TEST_OUTPUT_XML
	CU_set_output_filename("libcam");
	CU_list_tests_to_file();
	CU_automated_run_tests();
#else
	CU_basic_set_mode(CU_BRM_VERBOSE);
	CU_basic_run_tests();
	printf("\n");
	CU_basic_show_failures(CU_get_failure_list());
	printf("\n");
#endif /* ENABLE_TEST_OUTPUT_XML */

	if (CU_get_number_of_tests_failed() > 0) {
		ret = -1;
	}

exit:
	CU_cleanup_registry();
	return ret;
}
