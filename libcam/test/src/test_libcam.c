/*
 * Critical Application Monitoring (CAM)
 *
 * SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
 * and/or its affiliates <open-source-office@arm.com></text>
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "test_suite.h"

#include <CUnit/Basic.h>
#include <arpa/inet.h>
#include <netinet/ip.h>
#include <unistd.h>

#include <cam.h>
#include <cam_message.h>

#include <sys/socket.h>

#define TEST_LIBCAM_IP_ADDR          "127.0.0.1"
#define TEST_LIBCAM_STREAM_NAME      "Test Example"
#define TEST_LIBCAM_UUID_STR         "84085ddc-bc10-11ed-9a44-7ef9696e9dd3"
#define TEST_LIBCAM_INVALID_UUID_STR "84085ddc-bc10-11ed-9a447ef9696e9dd3"
#define TEST_LIBCAM_TIMEOUT          50000
#define TEST_LIBCAM_CAM_LOGFILE      "ctest.csel"
#define TEST_LIBCAM_CSEL_EXPECT      "expected_output.csel"
#define TEST_LIBCAM_CSEL_DEFAULT_NAME \
	"84085ddc-bc10-11ed-9a44-7ef9696e9dd3.csel"

struct test_libcam_ctx {
	int server_fd;
	int server_peer_fd;

	const char *address;
	unsigned int port;

	struct cam_msg_a2s_init recv_msg;

	cam_ctx_t ctx;
	cam_stream_ctx_t stream_ctx;

	struct cam_stream_config stream_cfg;
	struct cam_stream_cal_config cal_stream_cfg;
};

static struct test_libcam_ctx test_ctx = {
	.address = TEST_LIBCAM_IP_ADDR,
	.port = CAM_DEFAULT_PORT,
	.stream_cfg.name = TEST_LIBCAM_STREAM_NAME,
	.stream_cfg.uuid_str = TEST_LIBCAM_UUID_STR,
	.cal_stream_cfg.file_name = TEST_LIBCAM_CAM_LOGFILE,
	.cal_stream_cfg.timeout_i2s = TEST_LIBCAM_TIMEOUT,
	.cal_stream_cfg.timeout_s2e = TEST_LIBCAM_TIMEOUT,
};

static int server_msg_recv(int fd, uint8_t *buffer, uint16_t size)
{
	unsigned int bytes_recv = 0u;

	if (buffer == NULL) {
		return -1;
	}

	while (bytes_recv < size) {
		ssize_t count = recv(fd, buffer + bytes_recv, size - bytes_recv, 0);
		CU_ASSERT_FATAL(count != -1);
		/* Connection is closed */
		if (count == 0) {
			return -1;
		}
		bytes_recv += count;
	}

	CU_ASSERT(bytes_recv == size);

	return 0;
}

static void *server_accept_thread(void *arg)
{
	(void)arg;
	struct sockaddr_in peer_addr = { 0 };
	socklen_t peer_addr_size = sizeof(peer_addr);

	test_ctx.server_peer_fd = accept(
		test_ctx.server_fd, (struct sockaddr *)&peer_addr, &peer_addr_size);

	return NULL;
}

static int test_libcam_suite_init(void)
{
	struct sockaddr_in addr;

	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = inet_addr(test_ctx.address);
	addr.sin_port = htons(test_ctx.port);

	/* Server socket */
	test_ctx.server_fd = socket(AF_INET, SOCK_STREAM, 0);
	if (test_ctx.server_fd == -1) {
		perror("Error creating server socket");
		return -1;
	}

	if (bind(test_ctx.server_fd, (struct sockaddr *)&addr, sizeof(addr)) ==
		-1) {
		perror("Error binding server socket");
		close(test_ctx.server_fd);
		return -1;
	}

	if (listen(test_ctx.server_fd, 1) == -1) {
		perror("Error listening to server socket");
		close(test_ctx.server_fd);
		return -1;
	}

	return 0;
}

static int test_libcam_suite_clean(void)
{
	if (close(test_ctx.server_peer_fd) == -1) {
		return -1;
	}

	if (close(test_ctx.server_fd) == -1) {
		return -1;
	}

	return 0;
}

static void test_cam_version(void)
{
	unsigned int major, minor;
	int ret;

	ret = cam_version(NULL, &minor);
	CU_ASSERT(ret == CAM_ERROR);

	ret = cam_version(&major, NULL);
	CU_ASSERT(ret == CAM_ERROR);

	ret = cam_version(&major, &minor);
	CU_ASSERT(ret == CAM_SUCCESS);
	CU_ASSERT(major == CAM_VERSION_MAJOR);
	CU_ASSERT(minor == CAM_VERSION_MINOR);
}

static void test_cam_init(void)
{
	pthread_t tid;
	int ret;

	ret = cam_init(NULL, test_ctx.address, test_ctx.port);
	CU_ASSERT(ret == CAM_ERROR);

	ret = cam_init(&test_ctx.ctx, NULL, test_ctx.port);
	CU_ASSERT(ret == CAM_ERROR);

	ret = pthread_create(&tid, NULL, server_accept_thread, NULL);
	CU_ASSERT_FATAL(ret == 0);

	/* Wait 1ms for the accept thread to initialize. */
	usleep(1000);

	ret = cam_init(&test_ctx.ctx, test_ctx.address, test_ctx.port);
	CU_ASSERT(ret == CAM_SUCCESS);
	CU_ASSERT(test_ctx.ctx.socket_fd != -1);

	ret = pthread_join(tid, NULL);
	CU_ASSERT(ret == 0);

	CU_ASSERT_FATAL(test_ctx.server_peer_fd != -1);
}

static void *test_server_init_msg_thread(void *arg)
{
	(void)arg;
	struct cam_msg_s2a_init send_msg;
	struct cam_msg_header *header;
	uint16_t msg_size;
	uint8_t *buffer;
	unsigned int bytes_sent = 0u;
	int ret;

	buffer = (uint8_t *)&test_ctx.recv_msg;
	ret = server_msg_recv(
		test_ctx.server_peer_fd, buffer, sizeof(struct cam_msg_a2s_init));
	if (ret != 0) {
		return NULL;
	}

	buffer = (uint8_t *)&send_msg;
	msg_size = sizeof(struct cam_msg_s2a_init);
	header = &send_msg.header;

	header->version_major = CAM_MSG_VERSION_MAJOR;
	header->version_minor = CAM_MSG_VERSION_MINOR;
	header->msg_id = htons(CAM_MSG_S2A_ID_INIT);
	header->size = htons(msg_size);

	send_msg.timestamp = test_ctx.recv_msg.timestamp;
	send_msg.status = htons(CAM_MSG_S2A_INIT_STATUS_SUCCESS);
	send_msg.handler_id = htonl(0);
	cam_uuid_copy(send_msg.uuid, test_ctx.recv_msg.uuid);

	while (bytes_sent < msg_size) {
		ssize_t count = send(
			test_ctx.server_peer_fd,
			buffer + bytes_sent,
			msg_size - bytes_sent,
			0);
		if (count < 0) {
			return NULL;
		}
		bytes_sent += count;
	}

	return NULL;
}

static void test_cam_stream_init(void)
{
	struct cam_msg_header *header;
	cam_uuid_t uuid;
	pthread_t tid;
	int ret;

	ret = pthread_create(&tid, NULL, test_server_init_msg_thread, NULL);
	CU_ASSERT(ret == 0);

	ret = cam_stream_init(NULL, &test_ctx.stream_cfg, NULL);
	CU_ASSERT(ret == CAM_ERROR);

	ret = cam_stream_init(&test_ctx.stream_ctx, NULL, NULL);
	CU_ASSERT(ret == CAM_ERROR);

	test_ctx.stream_cfg.ctx = NULL;
	ret = cam_stream_init(&test_ctx.stream_ctx, &test_ctx.stream_cfg, NULL);
	CU_ASSERT(ret == CAM_ERROR);

	test_ctx.stream_cfg.ctx = &test_ctx.ctx;
	test_ctx.stream_cfg.uuid_str = TEST_LIBCAM_INVALID_UUID_STR;
	ret = cam_stream_init(&test_ctx.stream_ctx, &test_ctx.stream_cfg, NULL);
	CU_ASSERT(ret == CAM_ERROR);

	test_ctx.stream_cfg.uuid_str = TEST_LIBCAM_UUID_STR;
	ret = cam_stream_init(&test_ctx.stream_ctx, &test_ctx.stream_cfg, NULL);
	CU_ASSERT_FATAL(ret == CAM_SUCCESS);

	ret = pthread_join(tid, NULL);
	CU_ASSERT(ret == 0);

	header = &test_ctx.recv_msg.header;
	CU_ASSERT(header->version_major == CAM_MSG_VERSION_MAJOR);
	CU_ASSERT(header->version_minor == CAM_MSG_VERSION_MINOR);
	CU_ASSERT(header->size == ntohs(sizeof(struct cam_msg_a2s_init)));

	ret = cam_uuid_from_string(test_ctx.stream_cfg.uuid_str, uuid);
	CU_ASSERT(ret == CAM_UUID_SUCCESS);

	ret = cam_uuid_compare(test_ctx.recv_msg.uuid, uuid);
	CU_ASSERT(ret == 0);

	CU_ASSERT(test_ctx.stream_ctx.socket_fd != -1);
	CU_ASSERT(test_ctx.stream_ctx.handler_id == 0);
	CU_ASSERT(test_ctx.stream_ctx.sequence_id == 0);
	ret = cam_uuid_compare(test_ctx.stream_ctx.uuid, uuid);
	CU_ASSERT(ret == 0);
}

static void test_cam_stream_start(void)
{
	struct cam_msg_a2s_start recv_msg;
	uint8_t *buffer = (uint8_t *)&recv_msg;
	struct cam_msg_header *header;
	int ret;

	ret = cam_stream_start(NULL);
	CU_ASSERT(ret == CAM_ERROR);

	ret = cam_stream_start(&test_ctx.stream_ctx);
	CU_ASSERT_FATAL(ret == CAM_SUCCESS);

	ret = server_msg_recv(test_ctx.server_peer_fd, buffer, sizeof(recv_msg));
	CU_ASSERT(ret == 0);

	header = &recv_msg.header;
	CU_ASSERT(header->version_major == CAM_MSG_VERSION_MAJOR);
	CU_ASSERT(header->version_minor == CAM_MSG_VERSION_MINOR);
	CU_ASSERT(header->size == ntohs(sizeof(struct cam_msg_a2s_start)));

	CU_ASSERT(ntohl(recv_msg.handler_id) == test_ctx.stream_ctx.handler_id);

	CU_ASSERT(test_ctx.stream_ctx.socket_fd != -1);
	CU_ASSERT(test_ctx.stream_ctx.handler_id == 0);
	CU_ASSERT(test_ctx.stream_ctx.sequence_id == 0);
}

static void test_cam_stream_event(void)
{
	struct cam_msg_a2s_event recv_msg;
	uint8_t *buffer = (uint8_t *)&recv_msg;
	uint32_t i;
	const uint16_t event_num = 20;
	const uint16_t event_id_num = 5;
	int ret;

	ret = cam_stream_event(NULL, 0);
	CU_ASSERT_FATAL(ret == CAM_ERROR);

	for (i = 0; i < event_num; i++) {
		struct cam_msg_header *header;
		uint16_t event_id = i % event_id_num;
		ret = cam_stream_event(&test_ctx.stream_ctx, event_id);
		CU_ASSERT_FATAL(ret == CAM_SUCCESS);

		ret =
			server_msg_recv(test_ctx.server_peer_fd, buffer, sizeof(recv_msg));
		CU_ASSERT(ret == 0);

		header = &recv_msg.header;
		CU_ASSERT(header->version_major == CAM_MSG_VERSION_MAJOR);
		CU_ASSERT(header->version_minor == CAM_MSG_VERSION_MINOR);
		CU_ASSERT(header->size == ntohs(sizeof(struct cam_msg_a2s_event)));

		CU_ASSERT(ntohl(recv_msg.handler_id) == test_ctx.stream_ctx.handler_id);
		CU_ASSERT(ntohl(recv_msg.sequence_id) == i);
		CU_ASSERT(ntohs(recv_msg.event_id) == event_id);

		CU_ASSERT(test_ctx.stream_ctx.socket_fd != -1);
		CU_ASSERT(test_ctx.stream_ctx.handler_id == 0);
		CU_ASSERT(test_ctx.stream_ctx.sequence_id == (i + 1));
	}
}

static void test_cam_stream_stop(void)
{
	struct cam_msg_a2s_stop recv_msg;
	uint8_t *buffer = (uint8_t *)&recv_msg;
	struct cam_msg_header *header;
	int ret;

	ret = cam_stream_stop(NULL);
	CU_ASSERT(ret == CAM_ERROR);

	ret = cam_stream_stop(&test_ctx.stream_ctx);
	CU_ASSERT_FATAL(ret == CAM_SUCCESS);

	ret = server_msg_recv(test_ctx.server_peer_fd, buffer, sizeof(recv_msg));
	CU_ASSERT(ret == 0);

	header = &recv_msg.header;
	CU_ASSERT(header->version_major == CAM_MSG_VERSION_MAJOR);
	CU_ASSERT(header->version_minor == CAM_MSG_VERSION_MINOR);
	CU_ASSERT(header->size == ntohs(sizeof(struct cam_msg_a2s_stop)));

	CU_ASSERT(ntohl(recv_msg.handler_id) == test_ctx.stream_ctx.handler_id);
}

static void test_cam_end(void)
{
	int ret;

	ret = cam_end(NULL);
	CU_ASSERT(ret == CAM_ERROR);

	ret = cam_end(&test_ctx.ctx);
	CU_ASSERT_FATAL(ret == CAM_SUCCESS);
}

static void test_cam_calibration_invalid_case(void)
{
	struct cam_stream_ctx *stream_ctx = &test_ctx.stream_ctx;
	int ret;

	ret = cam_stream_init(NULL, &test_ctx.stream_cfg, &test_ctx.cal_stream_cfg);
	CU_ASSERT(ret == CAM_ERROR);

	ret = cam_stream_init(&test_ctx.stream_ctx, NULL, &test_ctx.cal_stream_cfg);
	CU_ASSERT(ret == CAM_ERROR);

	ret = cam_stream_end(NULL);
	CU_ASSERT(ret == CAM_ERROR);

	ret = cam_stream_init(
		&test_ctx.stream_ctx, &test_ctx.stream_cfg, &test_ctx.cal_stream_cfg);
	CU_ASSERT_FATAL(ret == CAM_SUCCESS);

	fclose(stream_ctx->csel_fp);
	stream_ctx->csel_fp = NULL;

	ret = cam_stream_start(&test_ctx.stream_ctx);
	CU_ASSERT(ret == CAM_ERROR);

	cam_stream_event(&test_ctx.stream_ctx, 0);
	CU_ASSERT(ret == CAM_ERROR);

	ret = cam_stream_stop(&test_ctx.stream_ctx);
	CU_ASSERT(ret == CAM_ERROR);

	ret = cam_stream_end(&test_ctx.stream_ctx);
	CU_ASSERT(ret == CAM_ERROR);
}

static bool is_identical_files(FILE *fp1, FILE *fp2)
{
	char c1, c2;

	while (1) {
		c1 = getc(fp1);
		c2 = getc(fp2);
		/*
		 * The excepted_output.csel uses FF bytes to fill the timestamp fields,
		 * compare and skip it.
		 */
		if (c2 == (char)255) {
			continue;
		}
		if (c1 != c2 || c1 == '\0') {
			break;
		}
	}

	if (c1 == c2) {
		return true;
	}

	return false;
}

static void test_cam_calibration(void)
{
	const uint16_t event_num = 20;
	const uint16_t event_id_num = 5;
	int ret;

	ret = cam_stream_init(
		&test_ctx.stream_ctx, &test_ctx.stream_cfg, &test_ctx.cal_stream_cfg);
	CU_ASSERT_FATAL(ret == CAM_SUCCESS);

	ret = cam_stream_start(&test_ctx.stream_ctx);
	CU_ASSERT_FATAL(ret == CAM_SUCCESS);

	for (uint32_t i = 0; i < event_num; i++) {
		uint16_t event_id = i % event_id_num;
		ret = cam_stream_event(&test_ctx.stream_ctx, event_id);
		CU_ASSERT_FATAL(ret == CAM_SUCCESS);

		CU_ASSERT(test_ctx.stream_ctx.sequence_id == (i + 1));
	}

	ret = cam_stream_stop(&test_ctx.stream_ctx);
	CU_ASSERT_FATAL(ret == CAM_SUCCESS);

	ret = cam_stream_end(&test_ctx.stream_ctx);
	CU_ASSERT_FATAL(ret == CAM_SUCCESS);

	FILE *test_output = fopen(TEST_LIBCAM_CAM_LOGFILE, "rb");
	FILE *expected_output = fopen(TEST_LIBCAM_CSEL_EXPECT, "rb");
	CU_ASSERT(is_identical_files(test_output, expected_output));

	test_ctx.cal_stream_cfg.file_name = NULL;
	ret = cam_stream_init(
		&test_ctx.stream_ctx, &test_ctx.stream_cfg, &test_ctx.cal_stream_cfg);
	CU_ASSERT_FATAL(ret == CAM_SUCCESS);

	fclose(test_ctx.stream_ctx.csel_fp);

	FILE *test_exist = fopen(TEST_LIBCAM_CSEL_DEFAULT_NAME, "r");
	CU_ASSERT_FATAL(test_exist != NULL)
}

static CU_TestInfo tests_libcam[] = {
	{ "version", test_cam_version },
	{ "init", test_cam_init },
	{ "stream init", test_cam_stream_init },
	{ "stream start", test_cam_stream_start },
	{ "stream event", test_cam_stream_event },
	{ "stream stop", test_cam_stream_stop },
	{ "end", test_cam_end },
	{ "calibration invalid case", test_cam_calibration_invalid_case },
	{ "calibration", test_cam_calibration },
	CU_TEST_INFO_NULL,
};

static CU_SuiteInfo suites[] = {
	{ "libcam",
	  test_libcam_suite_init,
	  test_libcam_suite_clean,
	  NULL,
	  NULL,
	  tests_libcam },
	CU_SUITE_INFO_NULL,
};

int main(int argc, char *argv[])
{
	(void)argc;
	(void)argv;

	return test_suite_run(suites);
}
