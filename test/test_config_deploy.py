#
# Critical Application Monitoring (CAM)
#
# SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited
# and/or its affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: BSD-3-Clause
#

import pytest
import filecmp
from component import *
from from_root import from_here


def get_path(file_name):
    return from_here('fixture', file_name)


class TestDeployCommand():

    def test_deploy_command(self, service, tool, test_dir):
        expected_filename = "84085ddc-bc10-11ed-9a44-7ef9696e9dd3.csd"
        expected_filepath = test_dir / expected_filename
        valid_input_filepath = get_path('config_deploy_valid.csd')

        service.spawn(params=f"-c {test_dir}")
        assert (tool.run(params=f"deploy -i {valid_input_filepath} -s 8 -o")
                == EXIT_SUCCESS)
        assert expected_filepath.exists()
        assert filecmp.cmp(valid_input_filepath, expected_filepath)

    def test_invalid_input(self, service, tool, test_dir):
        invalid_input_filepath = get_path('config_deploy_invalid.csd')

        service.spawn(params=f"-c {test_dir}")
        assert (tool.run(params=f"deploy -i {invalid_input_filepath} -s 8 -o")
                != EXIT_SUCCESS)

    def test_invalid_chunk_size(self, service, tool, test_dir):
        valid_input_filepath = get_path('config_deploy_valid.csd')

        service.spawn(params=f"-c {test_dir}")
        assert (tool.run(params=f"deploy -i {valid_input_filepath} -s 0 -o")
                != EXIT_SUCCESS)
