#
# Critical Application Monitoring (CAM)
#
# SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited
# and/or its affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: BSD-3-Clause
#

import pytest
import os
from component import *


def pytest_addoption(parser):
    parser.addoption('--install-dir', action='store')
    parser.addoption('--with-tcpdump', action='store')


def component(component_type, install_dir, test_dir, instance_num=1):
    c = CAMComponent(component_type, install_dir, test_dir, instance_num)
    yield c
    c.stop()


@pytest.fixture(scope='function')
def service(install_dir, test_dir):
    yield from component(Component.Service, install_dir, test_dir)


@pytest.fixture(scope='function')
def app(install_dir, test_dir):
    yield from component(Component.App, install_dir, test_dir)


@pytest.fixture(scope='function')
def app2(install_dir, test_dir):
    yield from component(Component.App, install_dir, test_dir, 2)


@pytest.fixture(scope='function')
def tool(install_dir, test_dir):
    yield from component(Component.Tool, install_dir, test_dir)


@pytest.fixture(scope='function')
def tcpdump(request, test_dir):
    if request.config.getoption("--with-tcpdump"):
        tcpdump_path = request.config.getoption("--with-tcpdump")
        tcpdump_path = os.path.expanduser(tcpdump_path)
        if not os.path.isfile(tcpdump_path):
            raise pytest.UsageError(f"Invalid tcpdump path: {tcpdump_path}")
        yield from component(Component.TcpDump, tcpdump_path, test_dir)
        return
    # Fixture with function scope must yield something, None is not an option
    yield 0


@pytest.fixture(scope='session', autouse=True)
def install_dir(request):
    install_dir = "/"
    if request.config.getoption("--install-dir"):
        install_dir = request.config.getoption("--install-dir")
        install_dir = os.path.expanduser(install_dir)
        if not os.path.isdir(install_dir):
            raise pytest.UsageError(f"Invalid install dir: {install_dir}")
    return install_dir


@pytest.fixture(scope='function')
def test_dir(request, tmp_path_factory):
    test_name = request.node.name
    return tmp_path_factory.mktemp(test_name, numbered=False)


@pytest.fixture(scope='class', autouse=True)
def csd_file_dir(install_dir):
    csd_file_dir = f"{install_dir}/share/cam-data"
    return csd_file_dir
