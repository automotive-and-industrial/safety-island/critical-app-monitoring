#
# Critical Application Monitoring (CAM)
#
# SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited
# and/or its affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: BSD-3-Clause
#

import os
from component import *
from from_root import from_here
from time import sleep


class TestStaticConfigClass:
    def start_tcpdump(self, tcpdump_obj):
        # tcpdump is optional, when it's not enabled tcpdump_obj is not a
        # CAMComponent object
        if isinstance(tcpdump_obj, CAMComponent):
            tcpdump_file = str(tcpdump_obj.test_dir / "dump.pcap")
            tcpdump_obj.spawn(params=f"-w {tcpdump_file} -i lo -U")

    def stop_tcpdump(self, tcpdump_obj):
        # tcpdump is optional, when it's not enabled tcpdump_obj is not a
        # CAMComponent object
        if isinstance(tcpdump_obj, CAMComponent):
            tcpdump_file = str(tcpdump_obj.test_dir / "dump.pcap")
            # Give more time to tcpdump to finish
            sleep(5)
            tcpdump_obj.stop()
            tcpdump_obj.run(params=f"-r {tcpdump_file} -v -K -ttttt -X")

    def test_cam_app_invalid_parameter(self, app, service, csd_file_dir):
        service.spawn(params=f"-c {csd_file_dir}")
        # Check with invalid port input
        assert app.run(params="--service-port=1") == EXIT_FAILURE

        # Check with invalid service address
        assert app.run(params="--service-address=a") == EXIT_FAILURE

        # Check with invalid UUID Base
        invalid_uuid = "84085ddc-bc10-11ed-9a44-7ef9696e0000"
        assert app.run(params=f"--uuid-base={invalid_uuid}") == EXIT_FAILURE
        non_zero_uuid = "84085ddc-bc10-11ed-9a44_7ef9696e"
        assert app.run(params=f"--uuid-base={non_zero_uuid}") == EXIT_FAILURE

        # Check with invalid stream count
        assert app.run(params="--stream-count=0") == EXIT_FAILURE

        # Check with invalid processing period
        assert app.run(params="--processing-period=0") == EXIT_FAILURE

        # Check with invalid processing count
        assert app.run(params="--processing-count=0") == EXIT_FAILURE

        # Check with invalid event interval
        assert app.run(params="--event-interval='0,0'") == EXIT_FAILURE

        # Check with invalid fault inject
        assert app.run(params="--enable-fault-injection "
                              "--fault-injection-time=6000") == EXIT_FAILURE

        # Check with invalid fault inject
        assert app.run(params="--enable-fault-injection "
                              "--fault-injection-stream=1") == EXIT_FAILURE

        assert "ERROR:" not in service.read_logs()

    def test_cam_service_invalid_parameter(self, service, csd_file_dir):
        # Check with invalid port input
        service.spawn(params=f"-c {csd_file_dir} -a 1")
        assert service.is_running() is False

    def test_cam_app_single_stream(self, app, service, tcpdump, csd_file_dir):
        self.start_tcpdump(tcpdump)
        service.spawn(params=f"-c {csd_file_dir}")
        assert app.run(params="--processing-count=4") == EXIT_SUCCESS
        assert service.is_running() is True
        self.stop_tcpdump(tcpdump)
        assert "ERROR:" not in service.read_logs()

    def test_cam_app_multiple_stream(self, app, service, tcpdump,
                                     csd_file_dir):
        self.start_tcpdump(tcpdump)
        service.spawn(params=f"-c {csd_file_dir}")
        assert (app.run(params="--processing-count=4 --stream-count=4")
                == EXIT_SUCCESS)
        assert service.is_running() is True
        self.stop_tcpdump(tcpdump)
        assert "ERROR:" not in service.read_logs()

    def test_cam_app_multiple_connection(self, app, service, tcpdump,
                                         csd_file_dir):
        self.start_tcpdump(tcpdump)
        service.spawn(params=f"-c {csd_file_dir}")
        assert app.run(params="--processing-count=4 --stream-count=4"
                              " --enable-multiple-connection") == EXIT_SUCCESS
        assert service.is_running() is True
        self.stop_tcpdump(tcpdump)
        assert "ERROR:" not in service.read_logs()

    def test_cam_app_multiple_application(self, app, app2, service, tool,
                                          tcpdump, csd_file_dir):
        self.start_tcpdump(tcpdump)
        # This means the applications should not run for more than 2 minutes
        failsafe_countdown = 24
        uuid_base = '99085ddc-bc10-11ed-9a44-7ef9696e'
        service.spawn(params=f"-c {csd_file_dir}")
        for i in range(2):
            uuid = from_here('fixture', f'{uuid_base}{i:04d}.csd')
            assert (tool.run(params=f"deploy -i {uuid} -o") == EXIT_SUCCESS)
        app.spawn(params="--processing-count=4 --stream-count=4", delay_s=0)
        app2.spawn(params=f"-u {uuid_base} --processing-count=8 \
                   --stream-count=2", delay_s=0)
        while app.is_running() or app2.is_running():
            sleep(5)
            failsafe_countdown -= 1
            if failsafe_countdown <= 0:
                break
        assert app.is_running() is False
        assert app2.is_running() is False
        assert app.stop() == EXIT_SUCCESS
        assert app2.stop() == EXIT_SUCCESS
        assert service.is_running() is True
        self.stop_tcpdump(tcpdump)
        assert "ERROR:" not in service.read_logs()

    def test_cam_app_fault_injection(self, app, service, tcpdump,
                                     csd_file_dir):
        # Check with start to event timeout
        self.start_tcpdump(tcpdump)
        service.spawn(params=f"-c {csd_file_dir}")
        assert app.run(params="--enable-fault-injection") == EXIT_SUCCESS
        assert service.is_running() is True
        self.stop_tcpdump(tcpdump)
        service_logs = service.read_logs()
        assert "ERROR:" in service_logs
        assert "state start to event timeout" in service_logs

    def test_cam_app_fault_injection_time(self, app, service, tcpdump,
                                          csd_file_dir):
        # Check with event temporal error
        self.start_tcpdump(tcpdump)
        service.spawn(params=f"-c {csd_file_dir}")
        assert (app.run(params="--enable-fault-injection "
                        "--fault-injection-time=200 "
                        "--processing-count=2") == EXIT_SUCCESS)
        assert service.is_running() is True
        self.stop_tcpdump(tcpdump)
        service_logs = service.read_logs()
        assert "ERROR:" in service_logs
        assert "Stream temporal error" in service_logs

    def test_cam_app_invalid_event_logic(self, app, service, tcpdump,
                                         csd_file_dir):
        self.start_tcpdump(tcpdump)
        service.spawn(params=f"-c {csd_file_dir}")
        event_interval = "0,100"
        assert (app.run(params=f"--event-interval={event_interval}")
                == EXIT_SUCCESS)
        assert service.is_running() is True
        self.stop_tcpdump(tcpdump)
        service_logs = service.read_logs()
        assert "ERROR:" in service_logs
        assert "Stream logical error" in service_logs

    def test_cam_service_exit_fault_action(self, app, service, csd_file_dir):
        # Trigger an error to make the service stop
        service.spawn(params=f"-c {csd_file_dir} -e exit")
        assert app.run(params="--enable-fault-injection") == EXIT_SUCCESS
        assert service.is_running() is False
        # Service is already stopped, but this closes the log file
        service.stop()
        service_logs = service.read_logs()
        # Check that the setting is enabled
        assert "fault action: exit" in service_logs
        assert "ERROR:" in service_logs
        # The triggered fault is "state start to event timeout" which is
        # encoded as exit code 17
        assert "Exit code: 17" in service_logs
