#
# Critical Application Monitoring (CAM)
#
# SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited
# and/or its affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: BSD-3-Clause
#

from enum import Enum, unique
import os
import subprocess
import time
import pytest

# Binary return code
EXIT_SUCCESS = 0
EXIT_FAILURE = 1


@unique
class Component(Enum):
    Service = 0
    App = 1
    Tool = 2
    TcpDump = 3


class CAMComponent():
    """The CAMComponent() class is used to provide basic test functions."""

    __common_params = ""
    __proc = None
    __default_delay_s = 3
    __default_timeout_s = 20
    component_names = {
        Component.Service: 'cam-service',
        Component.App: 'cam-app-example',
        Component.Tool: 'cam-tool',
        Component.TcpDump: 'tcpdump'
    }

    def __init__(self, component, dir, tmp_test_dir, instance_num=1):
        if not isinstance(instance_num, int):
            raise pytest.UsageError("Component parameter 'instance_num' must"
                                    " be integer.")

        if component not in self.component_names.keys():
            raise pytest.UsageError(f"Invalid component: {component}")

        name = self.component_names[component]

        # For tcpdump the dir variable contains the full path to the binary
        self.__exec_file = dir
        if component is not Component.TcpDump:
            self.__exec_file = os.path.join(dir, 'bin', name)

        if not os.path.isfile(self.__exec_file):
            raise pytest.UsageError(f"Invalid exec file path: \
                                     {self.__exec_file}")
        if instance_num > 1:
            name += f'_{instance_num}'

        self.log_filename = tmp_test_dir / f"{name}.log"
        self.test_dir = tmp_test_dir

    def set_params(self, params):
        """
        Set common parameter in the object which is used when invoking run()
        and spawn().
        """
        self.__common_params = params

    def __open_log_file(self, command):
        self.log_file = open(self.log_filename, "a")
        self.log_file.write(command + "\n")
        self.log_file.flush()

    def __close_log_file(self):
        if self.log_file.closed:
            return
        if self.__proc is not None:
            self.log_file.write(
                "Exit code: " + str(self.__proc.returncode) + "\n")
        # Add an empty line at the end of the file
        self.log_file.write("\n")
        self.log_file.close()

    def __proc_open(self, params="", background=False):
        command = f"{self.__exec_file} {self.__common_params} {params}"
        self.__open_log_file(command)

        # When spawning a command without knowing if it will run until
        # completion, call it with stdout buffered by line, this is because
        # without that, we could not get its output when we terminate it using
        # a signal.
        if background:
            command = 'stdbuf -oL ' + command

        return subprocess.Popen(f'{command}'.split(),
                                stdout=self.log_file, stderr=subprocess.STDOUT)

    def run(self, timeout_s=__default_timeout_s, params=""):
        """Run app example exec file.

        This function launch the exec file and the function will only return
        once the execution has finished. In case of time out, an Exception will
        be raised.

        Arguments:
            timeout_s(int): Timeout in seconds.
            params(str): Parameters used to launch the exec file. It is
                         appended to any common parameters already set via
                         set_params(). Must use long parameter names because
                         space in short parameter will cause split error of
                         params_list.

        Returns:
            int: The return code of exec file.

        Raises:
            TimeoutExpired: If the running time of app exceeds the timeout.
        """
        self.__proc = self.__proc_open(params)
        self.__proc.communicate(timeout=timeout_s)
        self.__close_log_file()
        return self.__proc.returncode

    def spawn(self, delay_s=__default_delay_s, params=""):
        """Run service exec file.

        First check if there has started a service process. This function
        launch the exec file and assign value to __proc which provides pid and
        poll() method for stop and is_running function.

        If this function is called, service will keep running until test_case
        ends or another spawn() is called.

        Arguments:
            delay_s(int): Delay time in seconds when process is started.
            parameter(str): Parameters used to launch the exec file. It is
                            appended to any common parameters already set via
                            set_params(). Must use long parameter names because
                            space in short parameter will cause split error of
                            params_list.
        """
        # Stop running process when the test cases call spawn multiple times.
        if self.__proc is not None:
            self.stop()

        self.__proc = self.__proc_open(params, True)

        time.sleep(delay_s)

    def stop(self):
        """Stop the running service process.

        This function can be called to stop a process, it returns also its exit
        code. If no process was started, it will return None.
        """
        if self.__proc is None:
            return None

        if self.is_running():
            self.__proc.terminate()
            try:
                self.__proc.wait(10)
            except subprocess.TimeoutExpired:
                self.__proc.kill()
                self.__proc.wait()

        self.__close_log_file()

        return self.__proc.returncode

    def read_logs(self):
        file_content = ''
        with open(self.log_filename, 'r') as file:
            lines = file.readlines()
            file_content = ''.join(lines)

        return file_content

    def is_running(self):
        """Check if service process is still running.

        This function can only be called to check service process status after
        spawn() is called.

        Returns:
            bool: The return value. True for is running, False otherwise.
        """
        if self.__proc is not None and self.__proc.poll() is None:
            return True
        else:
            return False
