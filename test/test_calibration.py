#
# Critical Application Monitoring (CAM)
#
# SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited
# and/or its affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: BSD-3-Clause
#

from component import *
from from_root import from_here


class TestCalibration:
    test_input_dir = 'fixture'

    def test_cam_app_cal_mode_invalid_dir(self, app):
        # Check with invalid calibration directory
        assert (app.run(params="--calibration-directory=./false_dir")
                == EXIT_FAILURE)

    def test_cam_app_cal_mode(self, app, test_dir):
        expected_filename = '84085ddc-bc10-11ed-9a44-7ef9696e0000.csel'
        expected_file_directory = test_dir
        expected_filepath = expected_file_directory / expected_filename
        assert expected_filepath.exists() is False

        # Check with case of enable calibration mode
        assert (app.run(params="--enable-calibration-mode "
                               "--calibration-directory="
                               f"{expected_file_directory}")
                == EXIT_SUCCESS)

        assert expected_filepath.exists() is True

    def test_cam_tool_analyze_invalid_input(self, tool, test_dir):
        expected_filename = 'calibration_invalid_analyze.csc.yml'
        input_filename = 'calibration_invalid_analyze.csel'
        expected_filepath = test_dir / expected_filename
        input_csel_path = from_here(self.test_input_dir, input_filename)

        assert expected_filepath.exists() is False
        assert (tool.run(params=f"analyze -i {input_csel_path} "
                                f"-o {expected_filepath}")
                == EXIT_FAILURE)
        assert expected_filepath.exists() is False

    def test_cam_tool_analyze(self, tool, test_dir):
        expected_filename = 'calibration_analyze.csc.yml'
        input_filename = 'calibration_analyze.csel'
        expected_filepath = test_dir / expected_filename
        input_csel_path = from_here(self.test_input_dir, input_filename)

        assert expected_filepath.exists() is False
        assert (tool.run(params=f"analyze -i {input_csel_path} "
                                f"-o {expected_filepath}")
                == EXIT_SUCCESS)
        assert expected_filepath.exists() is True

    def test_cam_tool_pack_invalid_input(self, tool, test_dir):
        expected_filename = 'calibration_invalid_pack.csd'
        input_filename = 'calibration_invalid_analyze.csel'
        expected_filepath = test_dir / expected_filename
        input_csel_path = from_here(self.test_input_dir, input_filename)

        assert expected_filepath.exists() is False
        assert (tool.run(params=f"pack -i {input_csel_path} "
                                f"-o {expected_filepath}")
                == EXIT_FAILURE)
        assert expected_filepath.exists() is False

    def test_cam_tool_pack(self, tool, test_dir):
        expected_filename = 'calibration_pack.csd'
        input_filename = 'calibration_pack.csc.yml'
        expected_filepath = test_dir / expected_filename
        input_csc_path = from_here(self.test_input_dir, input_filename)

        assert expected_filepath.exists() is False
        assert (tool.run(params=f"pack -i {input_csc_path} "
                                f"-o {expected_filepath}")
                == EXIT_SUCCESS)
        assert expected_filepath.exists() is True

    def test_cam_tool_analyze_margin(self, tool, test_dir):
        expected_filename = 'calibration_analyze.csc.yml'
        input_filename = 'calibration_analyze.csel'
        timeout = 1500000
        expected_filepath = test_dir / expected_filename
        input_csel_path = from_here(self.test_input_dir, input_filename)

        assert expected_filepath.exists() is False
        assert (tool.run(params=f"analyze -i {input_csel_path} "
                                f"-o {expected_filepath} "
                                f"-m {timeout}")
                == EXIT_SUCCESS)
        assert expected_filepath.exists() is True
