<!--
#
# Critical Application Monitoring (CAM)
#
# SPDX-FileCopyrightText: <text>Copyright 2023-2024
#   Arm Limited and/or its affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause
#
-->

# Critical Application Monitoring (CAM)

The Critical Application Monitoring (CAM) project implements a solution for
monitoring applications using a service running on a higher safety level system.
The approach aims to take advantage of running heavy workloads in
high-performance cores whilst adding an extra layer of safety by monitoring such
applications via a higher safety subsystem.

For more details on the Critical Application Monitoring project, visit
 [Critical Application Monitoring (CAM) Project’s Documentation](https://critical-app-monitoring.docs.arm.com/).

## Build Host Environment Setup

To generate a local version of the documentation from source in your work
environment, follow the steps below.

### Dependencies

- CMake (3.16 or later)
- GCC (9.4 or later)
- Python (3.8.10 or later)
- Python-venv (3.8.10 or later)
- Pip (20.0 or later)
- pkg-config (0.29.1 or later)
- CUnit (2.1.3 or later)
- Doxygen (1.8.17 or later): Generate API documentation from source code.
- Sphinx (6.1.3 or later): Build the project documentation.
- Git version control system.

> **_NOTE:_**
> CAM project has Dockerfiles to build docker images which will contain all
> dependencies needed by CAM. Refer to `Dockerfiles/cam-image/Dockerfile` for
> more information on the Dockerfiles.

### System Requirements

- AArch64 or x86_64 host to generate the documentation
- Ubuntu Desktop or Server 20.04 Linux distribution

### Install Dependencies

```
sudo apt-get install -y --no-install-recommends \
     git \
     build-essential \
     cmake \
     doxygen \
     pkg-config \
     python3 \
     python3-venv \
     python3-pip \
     libcunit1-dev
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

### Run cmake:

```
mkdir build
cd build
cmake ../
```

### Generate CAM documentation:

```
make cam-doc
```

To render and explore the documentation, simply open
`build/documentation/cam-doc/index.html` in a web browser.
