#
# Critical Application Monitoring (CAM)
#
# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
# and/or its affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: BSD-3-Clause
#

'''
CAM Stream Configuration (CSC) file module
'''

from enum import Enum
import re
import yaml


class CamStreamConfigException(Exception):
    "CAM Stream Configuration (CSC) Exception"

    def __init__(self, message="Error detected"):
        self.message = message
        super().__init__(f"CAM Stream Configuration: {self.message}")


class CamStreamConfig():
    "The CamStreamConfig() class is used to manage a .csc.yml file."

    __FILE_VERSION = "1.0"
    __STREAM_NAME_BYTE_COUNT = 63
    __TIMEOUT_UPPER_LIMIT = 2**32-1
    __EVENT_UPPER_LIMIT = 2**16-1

    class AlarmMode(Enum):
        ONDEMAND_FROM_ARRIVAL = 0
        ONDEMAND_FROM_APP = 1
        PERIODIC = 2

    def __init__(self, filename=None):
        if filename is None:
            self.data = {"cam_stream_conf_version": "1.0", "event_alarms": []}

        else:
            with open(filename, 'r', encoding='utf-8') as input_file:
                self.data = self.__load_data(input_file)

                self.__validate_fields(self.data)
                self.__validate_data(self.data)

    def __load_data(self, input_file):
        try:
            data = yaml.load(input_file, yaml.FullLoader)
        except yaml.parser.ParserError:
            raise CamStreamConfigException("Failed to load the yaml file")

        return data

    def __validate_fields_in_metadata(self, metadata):
        for key in ["uuid", "name"]:
            if key not in metadata:
                raise CamStreamConfigException(f"Metadata.{key} not found")

    def __validate_fields_in_state_control(self, state_control):
        for key in ["timeout_init_to_start", "timeout_start_to_event"]:
            if key not in state_control:
                raise CamStreamConfigException(f"State_Control.{key}"
                                               " not found")

    def __validate_fields_in_event_alarm(self, event_alarm):
        if not isinstance(event_alarm, dict):
            raise CamStreamConfigException("Invalid item found in"
                                           " Event_Alarms")

        if set(event_alarm.keys()) != {"timeout", "event_id", "mode"}:
            raise CamStreamConfigException("Missing section in Event_Alarm"
                                           f": {event_alarm}")

    def __validate_fields_in_event_alarms(self, event_alarms):
        if not isinstance(event_alarms, list):
            raise CamStreamConfigException("Event_Alarms section should be a"
                                           " non-empty list")

        for event_alarm in event_alarms:
            self.__validate_fields_in_event_alarm(event_alarm)

    def __validate_fields(self, data):
        if data is None:
            raise CamStreamConfigException("The file is empty")

        # Check file version
        if "cam_stream_conf_version" not in data:
            raise CamStreamConfigException("File version not found")

        # Check metadata section
        if "metadata" not in data:
            raise CamStreamConfigException("Metadata section not found")

        self. __validate_fields_in_metadata(data["metadata"])

        # Check state control section
        if "state_control" not in data:
            raise CamStreamConfigException("State_Control section not found")

        self.__validate_fields_in_state_control(data["state_control"])

        # Check Event alarms section
        if "event_alarms" not in data:
            raise CamStreamConfigException("Event_Alarms section not found")

        self.__validate_fields_in_event_alarms(data["event_alarms"])

    def __validate_data_version(self, version):
        if version != self.__FILE_VERSION:
            raise CamStreamConfigException(f"File version '{version}'"
                                           " is not supported")

    def __validate_data_uuid(self, uuid):
        if not isinstance(uuid, str):
            raise CamStreamConfigException("Metadata.uuid should be a string")
        regex_uuid = r"^[0-9a-fA-F]{8}(?:-[0-9a-fA-F]{4}){3}-[0-9a-fA-F]{12}$"
        if not re.match(regex_uuid, uuid):
            raise CamStreamConfigException(f"Metadata.uuid format '{uuid}'"
                                           " is incorrect")

    def __validate_data_name(self, name):
        if not isinstance(name, str):
            raise CamStreamConfigException("Metadata.name should be a string")
        if len(name) > self.__STREAM_NAME_BYTE_COUNT:
            raise CamStreamConfigException("Metadata.name should be no longer"
                                           " than 63 characters, find"
                                           f" {name}")

    def __validate_data_timeout(self, timeout, name):
        if (not isinstance(timeout, int) or timeout < 0
                or timeout > self.__TIMEOUT_UPPER_LIMIT):
            raise CamStreamConfigException(f"{name} value {timeout}"
                                           " is invalid")

    def __validate_data_event_id(self, event_id):
        if (not isinstance(event_id, int) or event_id < 0
                or event_id > self.__EVENT_UPPER_LIMIT):
            raise CamStreamConfigException("Event_alarms.event_id value"
                                           f" {event_id} is invalid")

    def __validate_data_mode(self, mode):
        alarm_names = [alarm.name.lower() for alarm in self.AlarmMode]
        alarm_values = [alarm.value for alarm in self.AlarmMode]
        if mode not in alarm_names and mode not in alarm_values:
            raise CamStreamConfigException(f"Event_alarms.mode {mode}"
                                           " is not supported")

    def __validate_data(self, data):
        version = data["cam_stream_conf_version"]
        self.__validate_data_version(version)

        uuid = data["metadata"]["uuid"]
        self.__validate_data_uuid(uuid)

        name = data["metadata"]["name"]
        self.__validate_data_name(name)

        timeout_i2s = data["state_control"]["timeout_init_to_start"]
        self.__validate_data_timeout(timeout_i2s,
                                     "State_Control.timeout_init_to_start")

        timeout_s2e = data["state_control"]["timeout_start_to_event"]
        self.__validate_data_timeout(timeout_s2e,
                                     "State_Control.timeout_start_to_event")

        for event_alarm in data["event_alarms"]:
            timeout = event_alarm["timeout"]
            self.__validate_data_timeout(timeout, "Event_alarms.timeout")

            event_id = event_alarm["event_id"]
            self.__validate_data_event_id(event_id)

            mode = event_alarm["mode"]
            self.__validate_data_mode(mode)

    def set_metadata(self, uuid, name):
        self.__validate_data_uuid(uuid)
        self.__validate_data_name(name)

        metadata = {"uuid": uuid, "name": name}
        self.data["metadata"] = metadata

    def set_state_control(self, timeout_i2s, timeout_s2e):
        self.__validate_data_timeout(timeout_i2s,
                                     "State_Control.timeout_init_to_start")
        self.__validate_data_timeout(timeout_s2e,
                                     "State_Control.timeout_start_to_event")

        state_control = {"timeout_init_to_start": timeout_i2s,
                         "timeout_start_to_event": timeout_s2e}
        self.data["state_control"] = state_control

    def add_event_alarm(self, timeout, event_id, mode):
        self.__validate_data_timeout(timeout, "Event_alarms.timeout")
        self.__validate_data_event_id(event_id)
        self.__validate_data_mode(mode)

        event_alarm = {"timeout": timeout, "event_id": event_id, "mode": mode}
        self.data["event_alarms"].append(event_alarm)

    def file_save(self, filename):
        self.__validate_fields(self.data)

        with open(filename, 'w', encoding='utf-8') as output:
            output.write("%YAML 1.2\n---\n")
            yaml.dump(self.data, output)
