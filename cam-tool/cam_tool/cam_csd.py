#
# Critical Application Monitoring (CAM)
#
# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
# and/or its affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: BSD-3-Clause
#

'''
CAM Stream Deployment (CSD) file module

The CSD is a file format used for the deployment of critical application
streams in a cam-service.

This module implements a class capable of generating and saving .csd files.
'''

from abc import ABC, abstractmethod
from enum import Enum
import struct


class CamStreamDeploymentException(Exception):
    "CAM Stream Deployment (CSD) Exception"

    def __init__(self, message="Error detected"):
        self.message = message
        super().__init__(f"CAM Stream Deployment: {self.message}")


class Section(ABC):
    _struct_format = ""

    @abstractmethod
    def __init__(self, csd_data, offset):
        pass

    def __len__(self):
        return struct.calcsize(self._struct_format)

    def _is_parsing_data_exist(self, csd_data, offset):
        if offset + len(self) > len(csd_data):
            raise CamStreamDeploymentException("Invalid format, file"
                                               " is incomplete")

    @abstractmethod
    def serialize(self):
        pass


class Header(Section):
    _struct_format = "4s2s2x"

    __FILE_SIGNATURE = bytes([0x59, 0x4a, 0x58, 0x46])
    __FILE_VERSION = bytes([1, 0])

    def __init__(self, csd_data=None, offset=None):
        if csd_data is not None:
            self._is_parsing_data_exist(csd_data, offset)
            data = struct.unpack_from(self._struct_format, csd_data, offset)

            self.__signature = data[0]
            self.__version = data[1]

        else:
            self.__signature = self.__FILE_SIGNATURE
            self.__version = self.__FILE_VERSION

    def validate(self):
        if self.__signature != self.__FILE_SIGNATURE:
            raise CamStreamDeploymentException("Invalid file signature")

        if self.__version != self.__FILE_VERSION:
            raise CamStreamDeploymentException("Invalid CSD file version")

    def serialize(self):
        return struct.pack(self._struct_format,
                           self.__signature,
                           self.__version)


class Metadata(Section):
    _struct_format = "16s64s"

    __UUID_BYTE_COUNT = 16
    __STREAM_NAME_BYTE_COUNT = 64

    def __init__(self, csd_data=None, offset=None):
        if csd_data is not None:
            self._is_parsing_data_exist(csd_data, offset)
            data = struct.unpack_from(self._struct_format, csd_data, offset)

            self.__uuid = data[0]

            name_byte = data[1]
            name = name_byte.strip(b'\0')
            try:
                self.__name = name.decode()
            except UnicodeDecodeError:
                raise CamStreamDeploymentException("Invalid Metadata.name,"
                                                   " expected a 'utf-8'"
                                                   " string")

        else:
            # The uuid is in bytes
            self.__uuid = ""
            self.__name = ""

    def __validate_uuid(self, uuid):
        if not isinstance(uuid, bytes) and not isinstance(uuid, bytearray):
            raise CamStreamDeploymentException("Invalid UUID data type."
                                               " Expecting bytes or bytearray")

        if len(uuid) != self.__UUID_BYTE_COUNT:
            raise CamStreamDeploymentException("Invalid UUID size")

    def __validate_name(self, name):
        if not isinstance(name, str) or not name.isascii():
            raise CamStreamDeploymentException("Invalid 'name' data."
                                               " It must be ASCII string")

        if len(name) >= (self.__STREAM_NAME_BYTE_COUNT):
            raise CamStreamDeploymentException("Invalid 'name' size")

    def set_uuid(self, uuid):
        self.__validate_uuid(uuid)
        self.__uuid = uuid

    def set_name(self, name):
        self.__validate_name(name)
        self.__name = name

    def get_uuid(self):
        return self.__uuid

    def serialize(self):
        return struct.pack(self._struct_format,
                           self.__uuid,
                           bytes(self.__name, 'ascii'))


class StateControl(Section):
    _struct_format = "<II"

    __TIMEOUT_UPPER_LIMIT = 2 ** (struct.Struct('I').size * 8) - 1

    def __init__(self, csd_data=None, offset=None):
        if csd_data is not None:
            self._is_parsing_data_exist(csd_data, offset)
            data = struct.unpack_from(self._struct_format, csd_data, offset)

            self.__init_to_start_timeout = data[0]
            self.__start_to_event_timeout = data[1]

        else:
            self.__init_to_start_timeout = 0
            self.__start_to_event_timeout = 0

    def __validate_timeout(self, timeout, name):
        if not isinstance(timeout, int):
            raise CamStreamDeploymentException(f"Invalid '{name}' data type."
                                               " Expecting int")

        if timeout < 0 or timeout > self.__TIMEOUT_UPPER_LIMIT:
            raise CamStreamDeploymentException(f"Invalid '{name}' size")

    def set_init_to_start_timeout(self, timeout):
        self.__validate_timeout(timeout, "init_to_start_timeout")
        self.__init_to_start_timeout = timeout

    def set_start_to_event_timeout(self, timeout):
        self.__validate_timeout(timeout, "start_to_event_timeout")
        self.__start_to_event_timeout = timeout

    def serialize(self):
        return struct.pack(self._struct_format,
                           self.__init_to_start_timeout,
                           self.__start_to_event_timeout)


class AlarmMode(Enum):
    ONDEMAND_FROM_ARRIVAL = 0
    ONDEMAND_FROM_APP = 1
    PERIODIC = 2


class EventAlarm(Section):
    _struct_format = "<IHBx"

    __TIMEOUT_UPPER_LIMIT = 2 ** (struct.Struct('I').size * 8) - 1
    __EVENT_UPPER_LIMIT = 2 ** (struct.Struct('H').size * 8) - 1

    def __init__(self, csd_data=None, offset=None):
        if csd_data is not None:
            self._is_parsing_data_exist(csd_data, offset)
            data = struct.unpack_from(self._struct_format, csd_data, offset)

            self.__timeout = data[0]
            self.__event_id = data[1]
            self.__mode = data[2]

        else:
            self.__timeout = 0
            self.__event_id = 0
            self.__mode = 0

    def __validate_timeout(self, timeout):
        if not isinstance(timeout, int):
            raise CamStreamDeploymentException("Invalid event_alarms.timeout"
                                               " data type. Expecting int")

        if timeout < 0 or timeout > self.__TIMEOUT_UPPER_LIMIT:
            raise CamStreamDeploymentException("Invalid event_alarms.timeout"
                                               " size")

    def __validate_event_id(self, event_id):
        if not isinstance(event_id, int):
            raise CamStreamDeploymentException("Invalid 'event_id' data type."
                                               " Expecting int")

        if event_id < 0 or event_id > self.__EVENT_UPPER_LIMIT:
            raise CamStreamDeploymentException("Invalid event_id size")

    def __validate_mode_id(self, mode):
        if not isinstance(mode, str) and not isinstance(mode, int):
            raise CamStreamDeploymentException("Invalid mode_id data type."
                                               " Expecting str or int")

        alarm_names = [alarm.name.lower() for alarm in AlarmMode]
        alarm_values = [alarm.value for alarm in AlarmMode]
        if mode not in alarm_names and mode not in alarm_values:
            raise CamStreamDeploymentException("Invalid mode_id value")

    def set_event_alarm(self, timeout, event_id, mode):
        self.__validate_timeout(timeout)
        self.__validate_event_id(event_id)
        self.__validate_mode_id(mode)

        if isinstance(mode, str):
            mode = AlarmMode[mode.upper()].value

        self.__timeout = timeout
        self.__event_id = event_id
        self.__mode = mode

    def serialize(self):
        return struct.pack(self._struct_format,
                           self.__timeout,
                           self.__event_id,
                           self.__mode)


class CamStreamDeploy():
    def __init__(self, filename=None):
        if filename is not None:
            with open(filename, 'rb') as input_file:
                csd_data = input_file.read()

            offset = 0

            # Load and validate the header
            self.__header = Header(csd_data, offset)
            self.__header.validate()
            offset += len(self.__header)

            # Load the metadata
            self.__metadata = Metadata(csd_data, offset)
            offset += len(self.__metadata)

            # Load the state control
            self.__state_control = StateControl(csd_data, offset)
            offset += len(self.__state_control)

            # Load the event alarm
            self.__alarms = list()
            while offset < len(csd_data):
                event_alarm = EventAlarm(csd_data, offset)
                self.__alarms.append(event_alarm.serialize())
                offset += len(event_alarm)

        else:
            self.__header = Header()
            self.__metadata = Metadata()
            self.__state_control = StateControl()
            self.__alarms = list()

    def set_uuid(self, uuid):
        self.__metadata.set_uuid(uuid)

    def get_uuid(self):
        return self.__metadata.get_uuid()

    def set_name(self, name):
        self.__metadata.set_name(name)

    def set_init_to_start_timeout(self, timeout):
        self.__state_control.set_init_to_start_timeout(timeout)

    def set_start_to_event_timeout(self, timeout):
        self.__state_control.set_start_to_event_timeout(timeout)

    def add_event_alarm(self, timeout, event_id, mode):
        event_alarm = EventAlarm()
        event_alarm.set_event_alarm(timeout, event_id, mode)
        self.__alarms.append(event_alarm.serialize())

    def file_save(self, filename):
        with open(filename, 'wb') as output:
            output.write(self.__header.serialize())
            output.write(self.__metadata.serialize())
            output.write(self.__state_control.serialize())

            for alarm in self.__alarms:
                output.write(alarm)
