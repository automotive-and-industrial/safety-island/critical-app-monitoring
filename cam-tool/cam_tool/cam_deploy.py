#
# Critical Application Monitoring (CAM)
#
# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
# and/or its affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: BSD-3-Clause
#

'''
CAM Deployment command
'''

from bitmap import BitMap
from enum import Enum
import socket
import sys

from cam_tool import cam_csd
from cam_tool import cam_message

cam_service_default_ip_addr = "127.0.0.1"
cam_service_default_port = 21604

attr_bits = 16


class MsgAttr(Enum):
    Overwrite = 0


status_dict = {cam_message.DeployStatus.DeploymentMessageDisabled.value:
               "deployment message is disabled",
               cam_message.DeployStatus.WriteChunkFailure.value:
               "failed to write chunk",
               cam_message.DeployStatus.DeployFileExists.value:
               "deployment file already exists for this UUID",
               cam_message.DeployStatus.InvalidFIleSize.value:
               "invalid file size",
               cam_message.DeployStatus.InvalidChunkOrder.value:
               "invalid chunk order",
               cam_message.DeployStatus.InvalidChunkSize.value:
               "invalid chunk size",
               cam_message.DeployStatus.ExceedMaxSessions.value:
               "exceeded maximum number of deployment sessions"}


def set_attr(overwrite):
    attr = BitMap(attr_bits)
    if overwrite:
        attr[MsgAttr.Overwrite.value] = True

    return int(str(attr))


def receive_msg(client, size):
    data = bytearray(0)
    try:
        while len(data) < size:
            data.extend(client.recv(size - len(data)))
    except socket.error:
        print("CAM Tool: Fail to receive response")
        return None

    return data


def send_msg(client, msg):
    try:
        client.send(msg.get_message())
    except socket.error:
        print("CAM Tool: Fail to send message")
        return False

    return True


def get_rsp(client, uuid, path):
    rcv_data = receive_msg(client, cam_message.CamMessageS2aDeploy.size())
    if not rcv_data:
        return False

    try:
        rcv_msg = cam_message.CamMessageS2aDeploy(rcv_data)
        rcv_msg.validate(uuid)
        status = rcv_msg.status()
    except cam_message.CamMessageException as e:
        print(e)
        return False

    if status != cam_message.DeployStatus.Success.value:
        print(f"CAM Tool: Fail to deploy {path}, {status_dict[status]}")
        return False

    return True


def run_deploy(args):
    ret = True

    try:
        csd = cam_csd.CamStreamDeploy(args.filename_input)
        uuid = csd.get_uuid()

        attr = set_attr(args.overwrite)
        csd_file = open(args.filename_input, "rb")
    except cam_csd.CamStreamDeploymentException as e:
        print(e)
        sys.exit(1)
    except FileNotFoundError:
        print(f"CAM Tool: Cannot find target file {args.filename_input}")
        sys.exit(1)
    except Exception as e:
        print(f"CAM Tool: Error occurs when opening"
              f" '{args.filename_input}'. {repr(e)}")
        sys.exit(1)

    client = socket.socket()
    try:
        client.connect((args.service_ip_address, args.service_port))
    except socket.error:
        print("CAM Tool: Failed to connect service address"
              f" {args.service_ip_address}:{args.service_port}")
        ret = False

    try:
        msg = cam_message.CamMessageA2sDeploy(
                uuid, attr, args.size, csd_file)
    except cam_message.CamMessageException as e:
        print(e)
        ret = False

    while ret and not msg.is_end():
        ret = (send_msg(client, msg) and
               get_rsp(client, msg.uuid(), args.filename_input))

    client.close()
    csd_file.close()
    if not ret:
        sys.exit(1)


def args_setup(subparsers):
    help_line = ("Deploy a stream deployment file (.csd) to the CAM"
                 " service.")
    command_description = ("This command deploys the CAM Stream Deployment"
                           " file (.csd) to the CAM service.")

    parser_deploy = subparsers.add_parser('deploy',
                                          description=command_description,
                                          help=help_line)

    parser_deploy.set_defaults(func=run_deploy)

    parser_deploy.add_argument('-i', '--input',
                               type=str,
                               required=True,
                               dest='filename_input',
                               help='stream deployment file (.csd)')

    parser_deploy.add_argument('-a', '--address',
                               type=str,
                               default=cam_service_default_ip_addr,
                               dest='service_ip_address',
                               help='CAM service ip address.'
                                    f' Default: {cam_service_default_ip_addr}')

    parser_deploy.add_argument('-p', '--port',
                               type=int,
                               default=cam_service_default_port,
                               dest='service_port',
                               help='CAM service port.'
                                    f' Default: {cam_service_default_port}')

    parser_deploy.add_argument('-o', '--overwrite',
                               action='store_true',
                               dest='overwrite',
                               help='overwrite previous file. Default: False')

    parser_deploy.add_argument('-s', '--size',
                               type=int,
                               default=512,
                               choices=range(1, 1025),
                               metavar="[1~1024]",
                               help='chunk size in each message'
                                    ' (in bytes). Default: 512')
