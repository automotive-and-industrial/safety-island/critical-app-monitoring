#
# Critical Application Monitoring (CAM)
#
# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
# and/or its affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: BSD-3-Clause
#

'''
CAM Stream Message module
'''

from abc import ABC, abstractmethod
from enum import Enum
import os
import struct
import time


class CamMessageException(Exception):
    "CAM Stream Message Exception"

    def __init__(self, msg_type="", message="Error detected"):
        self.msg_type = msg_type + " " if msg_type != "" else msg_type
        self.message = message
        super().__init__(f"CAM Stream {self.msg_type}Message: {self.message}")


class Section(ABC):
    _struct_format = ""

    @abstractmethod
    def __init__(self, msg_data, offset):
        pass

    def __len__(self):
        return struct.calcsize(self._struct_format)


class CommandId(Enum):
    DEPLOY_S2A = 4
    DEPLOY_A2S = 5


class CamMessageA2sHeader(Section):
    _struct_format = "!2sHH2x"

    __PROTOCOL_VERSION = bytes([1, 0])

    __version = None
    msg_size = 0
    __commandId = 0

    def __init__(self, cmdId):
        self.__version = self.__PROTOCOL_VERSION
        self.__commandId = cmdId

    def serialize(self):
        return struct.pack(self._struct_format,
                           self.__version,
                           self.msg_size,
                           self.__commandId)


class CamMessageS2aHeader(Section):
    _struct_format = "!2sHH2x"

    __PROTOCOL_VERSION = bytes([1, 0])

    __version = None
    __msg_size = 0
    __commandId = 0

    def __init__(self, msg_data):
        data = struct.unpack_from(self._struct_format, msg_data)

        self.__version = data[0]
        self.__msg_size = data[1]
        self.__commandId = data[2]

    def validate(self, cmdId, len_payload):
        if self.__version != self.__PROTOCOL_VERSION:
            raise CamMessageException("Deployment", "Invalid protocol version")

        if self.__msg_size != len(self) + len_payload:
            raise CamMessageException("Deployment", "Invalid message size")

        if self.__commandId != cmdId:
            raise CamMessageException("Deployment", "Invalid command ID")


def get_ts():
    " CAM uses realtime clock time as timestamp and accurate to "
    " the microsecond "
    return int(1000000 * time.clock_gettime(time.CLOCK_REALTIME))


class CamMessageA2sDeploy(Section):
    __payload_format = "!Q16sHHxBH"
    # len(self) will return size of the payload format except chunk data
    _struct_format = __payload_format

    __header = None

    __uuid = ""
    __attr = 0
    __chunk_id = 0
    __chunk_size = 512
    __offset = 0
    __csd_file = None
    __file_size = 0

    def __init__(self, uuid, attr, chunk_size, csd_file):
        self.__header = CamMessageA2sHeader(CommandId.DEPLOY_A2S.value)

        self.__uuid = uuid
        self.__attr = attr
        self.__chunk_size = chunk_size
        self.__csd_file = csd_file
        if not self.__csd_file.readable():
            raise CamMessageException("Deployment",
                                      f"'{csd_file}' is not a readable file")
        self.__file_size = os.fstat(self.__csd_file.fileno()).st_size
        if self.__file_size >= 2 ** 16:
            raise CamMessageException("Deployment",
                                      "Input file is too big. (Max: 64K)")

    def uuid(self):
        return self.__uuid

    def get_message(self):
        chunk_data = self.__csd_file.read(self.__chunk_size)

        self.__header.msg_size = (len(self.__header)
                                  + len(self) + len(chunk_data))

        byte_str = bytearray(0)
        byte_str.extend(self.__header.serialize())

        timestamp = get_ts()
        if timestamp >= 2 ** 64:
            raise CamMessageException("Deployment", "Invalid timestamp")

        # Extend the metadata
        byte_str.extend(struct.pack(self._struct_format,
                                    timestamp,
                                    self.__uuid,
                                    self.__attr,
                                    self.__file_size,
                                    self.__chunk_id,
                                    len(chunk_data)))

        # Extend the chunk data
        byte_str.extend(chunk_data)

        self.__chunk_id += 1
        # Wrapping around case
        if self.__chunk_id >= 2 ** 8:
            self.__chunk_id -= 2 ** 8

        self.__offset += len(chunk_data)

        return byte_str

    def is_end(self):
        return self.__offset >= self.__file_size


class DeployStatus(Enum):
    Success = 0
    DeploymentMessageDisabled = 1
    WriteChunkFailure = 2
    DeployFileExists = 3
    InvalidFIleSize = 4
    InvalidChunkOrder = 5
    InvalidChunkSize = 6
    ExceedMaxSessions = 7


class CamMessageS2aDeploy(Section):
    __payload_format = "!Q16sB7x"
    # len(self) will return size of the payload format except chunk data
    _struct_format = __payload_format

    __header = None

    def __init__(self, msg_data):
        if len(msg_data) != self.size():
            raise CamMessageException("Deployment", "Invalid message received")

        self.__header = CamMessageS2aHeader(msg_data)

        data = struct.unpack_from(
            self._struct_format, msg_data, len(self.__header))

        self.__timestamp = data[0]
        self.__uuid = data[1]
        self.__status = data[2]

    def validate(self, uuid):
        self.__header.validate(CommandId.DEPLOY_S2A.value, len(self))

        if self.__uuid != uuid:
            raise CamMessageException("Deployment", "Inconsistent uuid")

    def status(self):
        status_values = [status.value for status in DeployStatus]
        if self.__status not in status_values:
            raise CamMessageException("Deployment", "Unexpected status type")

        return self.__status

    @staticmethod
    def size():
        return (struct.calcsize(CamMessageS2aHeader._struct_format) +
                struct.calcsize(CamMessageS2aDeploy._struct_format))
