#!/usr/bin/env python3

#
# Critical Application Monitoring (CAM)
#
# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
# and/or its affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: BSD-3-Clause
#

'''
CAM tool
'''

import argparse
import importlib
import sys

# A list for command modules
import_modules = ["cam_tool.cam_analyze",
                  "cam_tool.cam_pack",
                  "cam_tool.cam_deploy"]


class CamToolException(Exception):
    "CAM Tool Exception"

    def __init__(self, message="Error detected"):
        self.message = message
        super().__init__(f"CAM Tool: {self.message}")


def get_subparsers(module_name):
    "Get the args_setup function from the command module"

    module = importlib.import_module(module_name)
    func = getattr(module, "args_setup")
    if func is None:
        raise CamToolException(f"Failed to load the module: {module_name}")
    return func


def main():
    tool_description = ("Critical Application Monitoring (CAM) tool\n\n"
                        "CAM tool is a unified tool for three main tasks:\n"
                        "* Analysis of the captured data produced during the"
                        " calibration process: cam-tool analyze <OPTIONS>\n"
                        "* Packing configs by converting CAM Stream"
                        " Configuration data to CAM Stream Deployment data:"
                        " cam-tool pack <OPTIONS>\n"
                        "* Deployment of CAM Stream Deployment data into the"
                        " CAM service: cam-tool deploy <OPTIONS>")

    parser = argparse.ArgumentParser(
        prog="cam-tool",
        formatter_class=argparse.RawDescriptionHelpFormatter,
        description=tool_description)

    help_line = "Commands. Use \"%(prog)s <command> --help\" for more details."
    subparsers = parser.add_subparsers(help=help_line,
                                       dest='command',
                                       required=True)

    # Run all the "args_setup" function in command modules
    for module_name in import_modules:
        try:
            sub_args_setup = get_subparsers(module_name)
        except CamToolException as e:
            print(e)
            sys.exit(1)
        sub_args_setup(subparsers)

    args = parser.parse_args()
    args.func(args)


if __name__ == "__main__":
    main()
