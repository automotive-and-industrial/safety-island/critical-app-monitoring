#
# Critical Application Monitoring (CAM)
#
# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
# and/or its affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: BSD-3-Clause
#

'''
CAM Stream Event Log (CSEL) file module
'''

from abc import ABC, abstractmethod
from enum import Enum
import math
import struct
import uuid


class CamStreamEventLogException(Exception):
    "CAM Stream Event Log (CSEL) Exception"

    def __init__(self, message="Error detected"):
        self.message = message
        super().__init__(f"CAM Stream Event Log: {self.message}")


class Section(ABC):
    _struct_format = ""

    @abstractmethod
    def __init__(self, csel_data, offset):
        pass

    def __len__(self):
        return struct.calcsize(self._struct_format)

    def _is_parsing_data_exist(self, csel_data, offset):
        if offset + len(self) > len(csel_data):
            raise CamStreamEventLogException("Invalid format, file"
                                             "is incomplete")


class Header(Section):
    _struct_format = "4s2s2x"

    signature = None
    version = None

    def __init__(self, csel_data, offset):
        self._is_parsing_data_exist(csel_data, offset)
        data = struct.unpack_from(self._struct_format, csel_data, offset)

        self.signature = data[0]
        self.version = data[1]

    def validate(self):
        if self.signature != b'\x4d\x46\x4d\x4e':
            raise CamStreamEventLogException("Invalid file signature")

        if self.version != b'\x01\x00':
            raise CamStreamEventLogException("Invalid CSEL file version")


class Metadata(Section):
    _struct_format = "<16s64sQ"

    uuid = ""
    name = ""
    timestamp = 0

    def __init__(self, csel_data, offset):
        self._is_parsing_data_exist(csel_data, offset)
        data = struct.unpack_from(self._struct_format, csel_data, offset)

        uu_byte = data[0]
        uu = uuid.UUID(bytes=uu_byte)
        self.uuid = str(uu)

        name_byte = data[1]
        name = name_byte.strip(b'\0')
        try:
            self.name = name.decode()
        except UnicodeDecodeError:
            raise CamStreamEventLogException("Invalid Metadata.name,"
                                             " expected a 'utf-8' string")

        self.timestamp = data[2]


class StateControl(Section):
    _struct_format = "<II"

    init_to_start_timeout = 0
    start_to_event_timeout = 0

    def __init__(self, csel_data, offset):
        self._is_parsing_data_exist(csel_data, offset)
        data = struct.unpack_from(self._struct_format, csel_data, offset)

        self.init_to_start_timeout = data[0]
        self.start_to_event_timeout = data[1]


class EntryState(Enum):
    START = 1
    STOP = 2
    END = 3
    EVENT = 4


class Entry(Section):
    __common_format = "<BQ"
    __event_format = "<BQIHxx"

    entry_id = 0
    timestamp = 0
    sequence_id = 0
    event_id = 0

    def __init__(self, csel_data, offset):
        self.entry_id = int(csel_data[offset])
        self.__validate_entry_id()

        self._struct_format = (
                self.__event_format
                if self.entry_id == EntryState.EVENT.value
                else self.__common_format)

        self._is_parsing_data_exist(csel_data, offset)
        data = struct.unpack_from(self._struct_format,
                                  csel_data, offset)

        self.timestamp = data[1]
        if self.entry_id == EntryState.EVENT.value:
            self.sequence_id = data[2]
            self.event_id = data[3]

    def __validate_entry_id(self):
        state_values = [state.value for state in EntryState]
        if self.entry_id not in state_values:
            raise CamStreamEventLogException("Invalid entry type")

    def __getitem__(self, item):
        return getattr(self, item)

    def keys(self):
        if self.entry_id == EntryState.EVENT.value:
            return ('entry_id', 'timestamp', 'sequence_id', 'event_id')
        else:
            return ('entry_id', 'timestamp')


class CamStreamEventLog():
    "The CamStreamEventLog() class is used to manage a .csel file."

    header = None
    metadata = None
    state_control = None
    event_list = []
    count_period_list = []

    def __init__(self, filename):
        with open(filename, 'rb') as input_file:
            csel_data = input_file.read()

        offset = 0

        # Load and validate the header
        self.header = Header(csel_data, offset)
        self.header.validate()
        offset += len(self.header)

        # Load the metadata
        self.metadata = Metadata(csel_data, offset)
        offset += len(self.metadata)

        # Load the state control
        self.state_control = StateControl(csel_data, offset)
        offset += len(self.state_control)

        # Load the entry list
        self.__entry_list = []
        while offset < len(csel_data):
            entry = Entry(csel_data, offset)
            self.__entry_list.append(dict(entry))
            offset += len(entry)

        # Entry logic validation (LV)
        # There are 3 logic rules in csel that need to be validated:
        # 1. The entry sequence. For example: The event entries must be between
        #    a pair of start and stop entries.
        # 2. The event entries in each run (entries from a start entry and a
        #    stop entry is called a run) should in a periodic order.
        # 3. The 'sequence_id' in the event entries must be incrementing.
        self.__validate_entries_sequence()  # LV1

        self.__get_event_entries_in_runs()
        self.__validate_event_entries_period(self.event_list)  # LV2
        self.__validate_sequence_id()  # LV3

    def __validate_entries_sequence(self):
        "Validate the entries sequence, the entry section should in the"
        "correct order"

        last_state = EntryState.STOP

        state_dict = {EntryState.START:
                      [EntryState.EVENT],

                      EntryState.STOP:
                      [EntryState.START, EntryState.END],

                      EntryState.END:
                      [],

                      EntryState.EVENT:
                      [EntryState.STOP, EntryState.EVENT]}

        for entry in self.__entry_list:
            state = EntryState(entry['entry_id'])
            if state not in state_dict[last_state]:
                raise CamStreamEventLogException("Invalid entries sequence")

            last_state = state

        if last_state != EntryState.END:
            raise CamStreamEventLogException("Invalid entries sequence")

    def __get_event_entries_in_runs(self):
        "The event entries between a start entry and a stop entry will be"
        "considered in the same run, get the event entries and split them"
        "in runs"

        event_entries_list = []
        start_index = -1
        for i in range(len(self.__entry_list)):
            state = EntryState(self.__entry_list[i]['entry_id'])
            if state == EntryState.START:
                start_index = i + 1
            elif state == EntryState.STOP:
                event_entries_list.append(self.__entry_list[start_index:i])
        self.event_list = event_entries_list

    def __get_event_entries_list(self):
        return [entry for entry in self.__entry_list
                if EntryState(entry["entry_id"]) == EntryState.EVENT]

    def __validate_event_entries_period_in_run(self, event_set, once_run):
        "The event entries in each run should in a periodic order,"
        "find the expected event in a period, validate and return the"
        "period times."

        event_id_list = [entry["event_id"] for entry in once_run]
        for offset in range(0, len(event_id_list), len(event_set)):
            if (set(event_id_list[offset: offset + len(event_set)]) !=
                    event_set):
                raise CamStreamEventLogException("Event entries should be in a"
                                                 " periodic order")

        return len(event_id_list) // len(event_set)

    def __validate_event_entries_period(self, event_entries):
        "Each 'once_run' sublist is a list of event entries between a pair of"
        "start and stop entry, validate whether the event entries are in a"
        "periodic order in each sublist and count the period times."

        event_set = set(entry["event_id"] for entry in
                        self.__get_event_entries_list())
        count_period_list = []

        for once_run in event_entries:
            count_period = self.__validate_event_entries_period_in_run(
                    event_set, once_run)

            count_period_list.append(count_period)

        if max(count_period_list) < 2:
            raise CamStreamEventLogException("Insufficient sample size")

        self.count_period_list = count_period_list

    def __validate_sequence_id_in_run(self, once_run):
        seq_id = 0
        for entry in once_run:
            if entry["sequence_id"] != seq_id:
                raise CamStreamEventLogException("Invalid event entries"
                                                 " sequence")
            seq_id += 1
            # Wrapping around case
            if seq_id >= 2 ** 32:
                seq_id -= 2 ** 32

    def __validate_sequence_id(self):
        "Validate the sequence id in event entry section, the sequence id"
        "must start with 0 and continuous increasing (by 1) in each run."

        event_entries = self.event_list
        for once_run in event_entries:
            self.__validate_sequence_id_in_run(once_run)
