#
# Critical Application Monitoring (CAM)
#
# SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited
# and/or its affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: BSD-3-Clause
#

'''
CAM Analysis command
'''

import sys

from cam_tool import cam_csel
from cam_tool import cam_csc


def get_csel_data(path):
    try:
        csel = cam_csel.CamStreamEventLog(path)
    except cam_csel.CamStreamEventLogException as e:
        print(e)
        sys.exit(1)
    except FileNotFoundError:
        print(f"CAM Tool: Cannot find target file {path}")
        sys.exit(1)

    return csel


def analyze_entry(entry, last_ts, max_timeout):
    event_id = entry["event_id"]
    timestamp = entry["timestamp"]

    if event_id not in max_timeout:
        max_timeout[event_id] = -1

    if event_id in last_ts:
        max_timeout[event_id] = max(timestamp - last_ts[event_id],
                                    max_timeout[event_id])

    last_ts[event_id] = timestamp


def analyze_entries(entries):
    # Count the maximum timeout of each event by the timestamp

    max_timeout = dict()

    for once_run in entries:
        last_ts = dict()

        for entry in once_run:
            analyze_entry(entry, last_ts, max_timeout)

    return max_timeout


def print_report(args, event_log, timeouts):
    print("CAM event log analyze report:\n"
          f"Input event log file:\t\t\t{args.filename_input}\n"
          "Output configuration file:\t\t"
          f"{args.filename_output or 'analyzed.csc.yml'}\n"
          f"Stream UUID:\t\t\t\t{event_log.metadata.uuid}\n"
          f"Stream name:\t\t\t\t{event_log.metadata.name}\n"
          "Timeout between init and start:\t\t"
          f"{event_log.state_control.init_to_start_timeout}\n"
          "Timeout between start and event:\t"
          f"{event_log.state_control.start_to_event_timeout}\n"
          "Application running times:\t\t"
          f"{len(event_log.count_period_list)}\n"
          "Processing count in each run:\t\t"
          f"{event_log.count_period_list}\n"
          "\nEvent ID\ttimeout")

    for event_id, timeout in timeouts.items():
        print(f"{event_id}\t\t{timeout}")


def run_analyze(args):
    event_log = get_csel_data(args.filename_input)
    config = cam_csc.CamStreamConfig()

    try:
        metadata = event_log.metadata
        config.set_metadata(metadata.uuid, metadata.name)

        state_ctrl = event_log.state_control
        config.set_state_control(state_ctrl.init_to_start_timeout,
                                 state_ctrl.start_to_event_timeout)

        event_entries = event_log.event_list
        timeouts = analyze_entries(event_entries)

        if args.margin_value is not None:
            if args.margin_value < 0:
                raise Exception("Margin value cannot be negative")
            for keys in timeouts.keys():
                timeouts[keys] += args.margin_value

        for event_id, timeout in timeouts.items():
            config.add_event_alarm(timeout, event_id, 0)

    except cam_csel.CamStreamEventLogException as e:
        print(e)
        sys.exit(1)

    try:
        output = args.filename_output or f"{event_log.metadata.uuid}.csc.yml"
        config.file_save(output)
    except Exception as e:
        print(f"CAM Tool: Failed saving the deployment file '{output}'."
              f" {repr(e)}")
        sys.exit(1)

    print_report(args, event_log, timeouts)


def args_setup(subparsers):
    help_line = ("Analyze a stream event log file (.csel) and output a"
                 " stream configuration file (.csc.yml).")
    command_description = ("This command analyzes the captured data produced"
                           " during the calibration process from the CAM"
                           " Stream Event Log file (.csel).")

    parser_analyze = subparsers.add_parser('analyze',
                                           description=command_description,
                                           help=help_line)

    parser_analyze.set_defaults(func=run_analyze)

    parser_analyze.add_argument('-i', '--input',
                                type=str,
                                required=True,
                                dest='filename_input',
                                help='stream event log file (.csel)')

    parser_analyze.add_argument('-o', '--output',
                                type=str,
                                default=None,
                                dest='filename_output',
                                help='stream configuration file (.csc.yml)'
                                     '. Default: "{uuid}.csc.yml"')

    parser_analyze.add_argument('-m', '--margin',
                                type=int,
                                default=None,
                                dest='margin_value',
                                help='margin in microseconds that is added to '
                                     'each calibrated event alarm timeout')
