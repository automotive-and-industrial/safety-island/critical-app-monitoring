#
# Critical Application Monitoring (CAM)
#
# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
# and/or its affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: BSD-3-Clause
#

'''
CAM Packing command
'''

import sys
import uuid

from cam_tool import cam_csc
from cam_tool import cam_csd


def get_csc_data(path):
    try:
        csc = cam_csc.CamStreamConfig(path)
    except cam_csc.CamStreamConfigException as e:
        print(e)
        sys.exit(1)
    except FileNotFoundError:
        print(f"CAM Tool: Cannot find target file {path}")
        sys.exit(1)

    return csc.data


def run_pack(args):
    data = get_csc_data(args.filename_input)
    deploy = cam_csd.CamStreamDeploy()

    uuid_str = data["metadata"]["uuid"]
    uu = uuid.UUID(uuid_str)
    try:
        deploy.set_uuid(uu.bytes)

        deploy.set_name(data["metadata"]["name"])

        deploy.set_init_to_start_timeout(
                data["state_control"]["timeout_init_to_start"])
        deploy.set_start_to_event_timeout(
                data["state_control"]["timeout_start_to_event"])

        for alarm in data["event_alarms"]:
            deploy.add_event_alarm(alarm["timeout"], alarm["event_id"],
                                   alarm["mode"])

        output = args.filename_output or f"{uuid_str}.csd"
    except cam_csd.CamStreamDeploymentException as e:
        print(e)
        sys.exit(1)

    try:
        deploy.file_save(output)
    except Exception as e:
        print(f"CAM Tool: Failed saving the deployment file '{output}'."
              f" {repr(e)}")
        sys.exit(1)


def args_setup(subparsers):
    help_line = ("Pack a stream configuration file (.csc.yml) into"
                 " a stream deployment file (.csd).")
    command_description = ("This command converts the CAM Stream Configuration"
                           " file (.csc.yml) to the CAM Stream Deployment file"
                           " (.csd).")

    parser_pack = subparsers.add_parser('pack',
                                        description=command_description,
                                        help=help_line)

    parser_pack.set_defaults(func=run_pack)

    parser_pack.add_argument('-i', '--input',
                             type=str,
                             required=True,
                             dest='filename_input',
                             help='stream configuration file (.csc.yml)')

    parser_pack.add_argument('-o', '--output',
                             type=str,
                             default=None,
                             dest='filename_output',
                             help='stream deployment file (.csd).'
                                  ' Default: "{uuid}.csd"')
