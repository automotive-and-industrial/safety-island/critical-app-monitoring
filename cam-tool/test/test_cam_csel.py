#
# Critical Application Monitoring (CAM)
#
# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
# and/or its affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: BSD-3-Clause
#

from from_root import from_here
import pytest

from cam_tool import cam_csel

E_csel = cam_csel.CamStreamEventLogException


def get_path(name):
    return from_here('fixture', name)


def invalid_csel_file(name):
    with pytest.raises(E_csel):
        obj = cam_csel.CamStreamEventLog(get_path(name))


def test_new_object():
    # Empty filename
    with pytest.raises(TypeError):
        obj = cam_csel.CamStreamEventLog()

    # Invalid filename
    with pytest.raises(FileNotFoundError):
        obj = cam_csel.CamStreamEventLog('/path/to/nothing')

    # Valid filename
    obj = cam_csel.CamStreamEventLog(from_here('fixture', 'valid.csel'))
    assert obj is not None


def test_invalid_file():
    # Invalid data
    invalid_csel_file('expected_output.csd')
    invalid_csel_file('no_padding_bytes.csel')
    invalid_csel_file('invalid_entry_type.csel')
    invalid_csel_file('invalid_entry_format.csel')

    # Invalid entry logic
    invalid_csel_file('no_end_entry.csel')
    invalid_csel_file('invalid_entry_sequence.csel')
    invalid_csel_file('non_periodic_event_entry.csel')
    invalid_csel_file('out_of_period_event_id.csel')
    invalid_csel_file('invalid_sequence_id.csel')


def test_data():
    csel = cam_csel.CamStreamEventLog(get_path('valid.csel'))

    assert csel.header.version == b'\x01\x00'

    assert csel.metadata.uuid == "84085ddc-bc10-11ed-9a44-7ef9696e9dd3"
    assert csel.metadata.name == "Stream name up to 63 characters long"
    assert csel.metadata.timestamp == 1681429736

    assert csel.state_control.init_to_start_timeout == 8974
    assert csel.state_control.start_to_event_timeout == 7264

    event_lists = [{'entry_id': 4, 'timestamp': 1681431588,
                    'sequence_id': 0, 'event_id': 1},
                   {'entry_id': 4, 'timestamp': 1681431630,
                    'sequence_id': 1, 'event_id': 2},
                   {'entry_id': 4, 'timestamp': 1681431660,
                    'sequence_id': 2, 'event_id': 1},
                   {'entry_id': 4, 'timestamp': 1681431685,
                    'sequence_id': 3, 'event_id': 2},
                   {'entry_id': 4, 'timestamp': 1681431700,
                    'sequence_id': 4, 'event_id': 2},
                   {'entry_id': 4, 'timestamp': 1681431730,
                    'sequence_id': 5, 'event_id': 1},
                   {'entry_id': 4, 'timestamp': 1681442500,
                    'sequence_id': 0, 'event_id': 1},
                   {'entry_id': 4, 'timestamp': 1681442520,
                    'sequence_id': 1, 'event_id': 2},
                   {'entry_id': 4, 'timestamp': 1681442543,
                    'sequence_id': 2, 'event_id': 1},
                   {'entry_id': 4, 'timestamp': 1681442545,
                    'sequence_id': 3, 'event_id': 2}]

    for i in range(6):
        assert csel.event_list[0][i] == event_lists[i]

    for i in range(4):
        assert csel.event_list[1][i] == event_lists[i+6]
