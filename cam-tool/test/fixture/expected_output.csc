%YAML 1.2
---
cam_stream_conf_version: '1.0'
event_alarms:
- event_id: 122
  mode: 2
  timeout: 254
- event_id: 232
  mode: 1
  timeout: 212
- event_id: 122
  mode: 0
  timeout: 8172
metadata:
  name: Stream name up to 63 characters long
  uuid: 84085ddc-bc10-11ed-9a44-7ef9696e9dd3
state_control:
  timeout_init_to_start: 100
  timeout_start_to_event: 500
