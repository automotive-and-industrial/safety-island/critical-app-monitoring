#
# Critical Application Monitoring (CAM)
#
# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
# and/or its affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: BSD-3-Clause
#

import filecmp
from from_root import from_here
import os
import pytest

from cam_tool import cam_csd

E_csd = cam_csd.CamStreamDeploymentException


def get_path(name):
    return from_here('fixture', name)


def invalid_csd_file(name):
    with pytest.raises(E_csd):
        obj = cam_csd.CamStreamDeploy(get_path(name))


def test_new_object():
    obj = cam_csd.CamStreamDeploy()
    assert obj is not None

    obj = cam_csd.CamStreamDeploy(get_path('expected_output.csd'))
    assert obj is not None


def test_invalid_file():
    invalid_csd_file('valid.csel')
    invalid_csd_file('no_padding_bytes.csd')
    invalid_csd_file('invalid_event_alarm_format.csd')


def test_invalid_data_type():
    obj = cam_csd.CamStreamDeploy()

    with pytest.raises(E_csd):
        invalid_uuid = "00010203-0405-0607-0809-0a0b0c0d0e0f"
        obj.set_uuid(invalid_uuid)

    with pytest.raises(E_csd):
        invalid_name = 0
        obj.set_name(invalid_name)

    with pytest.raises(E_csd):
        invalid_timeout = "eternity"
        obj.set_start_to_event_timeout(invalid_timeout)

    with pytest.raises(E_csd):
        valid_timeout = 10
        valid_event_id = 24
        invalid_mode = ["periodic"]
        obj.add_event_alarm(valid_timeout, valid_event_id, invalid_mode)

    with pytest.raises(FileNotFoundError):
        invalid_filename = ""
        obj.file_save(invalid_filename)


def test_invalid_data_range():
    obj = cam_csd.CamStreamDeploy()

    with pytest.raises(E_csd):
        invalid_uuid = (b'\x00\x01\x02\x03\x04\x05\x06\x07'
                        b'\x08\x09\x0a\x0b\x0c\x0d\x0e')
        obj.set_uuid(invalid_uuid)

    with pytest.raises(E_csd):
        invalid_name = ("Stream name up to 63 characters long,"
                        " but it is 67 and overflow now")
        obj.set_name(invalid_name)

    with pytest.raises(E_csd):
        invalid_timeout = -1
        obj.set_start_to_event_timeout(invalid_timeout)

    with pytest.raises(E_csd):
        valid_timeout = 10
        valid_event_id = 24
        invalid_mode = 0x0a
        obj.add_event_alarm(valid_timeout, valid_event_id, invalid_mode)


def test_file_saving():
    obj = cam_csd.CamStreamDeploy()

    uuid = (b'\x84\x08\x5d\xdc\xbc\x10\x11\xed'
            b'\x9a\x44\x7e\xf9\x69\x6e\x9d\xd3')
    obj.set_uuid(uuid)

    name = "Stream name up to 63 characters long"
    obj.set_name(name)

    obj.set_init_to_start_timeout(100)
    obj.set_start_to_event_timeout(500)

    obj.add_event_alarm(254, 122, 2)
    obj.add_event_alarm(212, 232, 1)
    obj.add_event_alarm(8172, 122, 0)

    obj.file_save("test_file_saving.csd")
    expect_output = from_here('fixture', 'expected_output.csd')

    assert filecmp.cmp("test_file_saving.csd", expect_output)

    if os.path.exists("test_file_saving.csd"):
        os.remove("test_file_saving.csd")


def test_uuid_loading():
    uuid = (b'\x84\x08\x5d\xdc\xbc\x10\x11\xed'
            b'\x9a\x44\x7e\xf9\x69\x6e\x9d\xd3')

    obj = cam_csd.CamStreamDeploy(get_path('expected_output.csd'))
    assert obj.get_uuid() == uuid
