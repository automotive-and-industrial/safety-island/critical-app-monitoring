#
# Critical Application Monitoring (CAM)
#
# SPDX-FileCopyrightText: <text>Copyright 2023-2024 Arm Limited
# and/or its affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: BSD-3-Clause
#

from bitmap import BitMap
from from_root import from_here
import pytest

from cam_tool import cam_message
from shutil import copy

E_msg = cam_message.CamMessageException


def get_path(name):
    return from_here('fixture', name)


def assert_deploy_msg(msg, msg_count):
    expected_output = get_path(f'expected_msg_output-{msg_count}')

    with open(expected_output, 'rb') as expected:
        expected_msg = expected.read()

    # Compare the messages except the timestamp
    assert msg[0:7] == expected_msg[0:7]
    assert msg[16:] == expected_msg[16:]


def test_deploy_message_a2s(tmp_path_factory):
    tmp_dir = tmp_path_factory.mktemp("test_deploy_message_a2s",
                                      numbered=False)
    uuid = (b'\x84\x08\x5d\xdc\xbc\x10\x11\xed'
            b'\x9a\x44\x7e\xf9\x69\x6e\x9d\xd3')
    attr = BitMap(16)
    attr[0] = True
    chunk_size = 32

    valid_csd_file = open(get_path('expected_output.csd'), "rb")
    msg = cam_message.CamMessageA2sDeploy(
        uuid, int(str(attr)), chunk_size, valid_csd_file)
    msg_count = 0
    while not msg.is_end():
        assert_deploy_msg(msg.get_message(), msg_count)
        msg_count += 1
    valid_csd_file.close()

    with pytest.raises(E_msg):
        invalid_csd_file = open(get_path("file_size_overflow.csd"), "rb")
        cam_message.CamMessageA2sDeploy(
            uuid, int(str(attr)), chunk_size, invalid_csd_file)
        invalid_csd_file.close()

    # In order to allow this test to pass also on read-only filesystem, copy
    # the file in a temporary writable path
    copy(get_path('expected_output.csd'), tmp_dir / "expected_output.csd")
    with pytest.raises(E_msg):
        unreadable_csd_file = open(tmp_dir / "expected_output.csd", "ab")
        cam_message.CamMessageA2sDeploy(
            uuid, int(str(attr)), chunk_size, unreadable_csd_file)
        unreadable_csd_file.close()


def test_deploy_message_s2a():
    uuid = (b'\x84\x08\x5d\xdc\xbc\x10\x11\xed'
            b'\x9a\x44\x7e\xf9\x69\x6e\x9d\xd3')

    with open(get_path('success_case.msg_rcv'), 'rb') as msg_rcv_input:
        msg_data = msg_rcv_input.read()
    msg = cam_message.CamMessageS2aDeploy(msg_data)
    msg.validate(uuid)
    assert msg.status() == 0

    with open(get_path('error_case.msg_rcv'), 'rb') as msg_rcv_input:
        msg_data = msg_rcv_input.read()
    msg = cam_message.CamMessageS2aDeploy(msg_data)
    msg.validate(uuid)
    assert msg.status() == 1

    with open(get_path('invalid_protocol_version.msg_rcv'),
              'rb') as msg_rcv_input:
        msg_data = msg_rcv_input.read()
    with pytest.raises(E_msg):
        msg = cam_message.CamMessageS2aDeploy(msg_data)
        msg.validate(uuid)

    with open(get_path('invalid_message_size.msg_rcv'), 'rb') as msg_rcv_input:
        msg_data = msg_rcv_input.read()
    with pytest.raises(E_msg):
        msg = cam_message.CamMessageS2aDeploy(msg_data)
        msg.validate(uuid)

    with open(get_path('invalid_command_id.msg_rcv'), 'rb') as msg_rcv_input:
        msg_data = msg_rcv_input.read()
    with pytest.raises(E_msg):
        msg = cam_message.CamMessageS2aDeploy(msg_data)
        msg.validate(uuid)

    with open(get_path('inconsistent_uuid.msg_rcv'), 'rb') as msg_rcv_input:
        msg_data = msg_rcv_input.read()
    with pytest.raises(E_msg):
        msg = cam_message.CamMessageS2aDeploy(msg_data)
        msg.validate(uuid)

    with open(get_path('unexpected_status_type.msg_rcv'),
              'rb') as msg_rcv_input:
        msg_data = msg_rcv_input.read()
    with pytest.raises(E_msg):
        msg = cam_message.CamMessageS2aDeploy(msg_data)
        msg.validate(uuid)
        msg.status()
