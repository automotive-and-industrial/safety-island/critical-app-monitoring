#
# Critical Application Monitoring (CAM)
#
# SPDX-FileCopyrightText: <text>Copyright 2023 Arm Limited
# and/or its affiliates <open-source-office@arm.com></text>
#
# SPDX-License-Identifier: BSD-3-Clause
#

import filecmp
from from_root import from_here
import os
import pytest
import sys

from cam_tool import cam_csc

E_csc = cam_csc.CamStreamConfigException


def get_path(name):
    return from_here('fixture', name)


def invalid_csc_file(name):
    with pytest.raises(E_csc):
        obj = cam_csc.CamStreamConfig(get_path(name))


def invalid_metadata_setting(obj, uuid, name):
    with pytest.raises(E_csc):
        obj.set_metadata(uuid, name)


def invalid_state_control_setting(obj, timeout_i2s, timeout_s2e):
    with pytest.raises(E_csc):
        obj.set_state_control(timeout_i2s, timeout_s2e)


def invalid_event_alarm_adding(obj, timeout, event_id, mode):
    with pytest.raises(E_csc):
        obj.add_event_alarm(timeout, event_id, mode)


def test_new_object():
    # Empty filename
    obj = cam_csc.CamStreamConfig()
    assert obj is not None

    # Invalid filename
    with pytest.raises(FileNotFoundError):
        obj = cam_csc.CamStreamConfig('/path/to/nothing')

    # Valid filename
    obj = cam_csc.CamStreamConfig(get_path('valid.csc.yml'))
    assert obj is not None


def test_invalid_file():
    # Empty file
    invalid_csc_file('empty_file.csc.yml')

    # Invalid file format
    invalid_csc_file('no_version.csc.yml')
    invalid_csc_file('no_meta.csc.yml')
    invalid_csc_file('no_meta_uuid.csc.yml')
    invalid_csc_file('no_meta_name.csc.yml')
    invalid_csc_file('no_alarms.csc.yml')

    # Invalid data
    invalid_csc_file('invalid_version.csc.yml')
    invalid_csc_file('invalid_uuid.csc.yml')
    invalid_csc_file('invalid_name.csc.yml')
    invalid_csc_file('invalid_timeout.csc.yml')
    invalid_csc_file('invalid_mode_id.csc.yml')
    invalid_csc_file('invalid_mode_name.csc.yml')


def test_data():
    data = cam_csc.CamStreamConfig(get_path('valid.csc.yml')).data

    assert data['cam_stream_conf_version'] == "1.0"

    assert data['metadata']['uuid'] == "84085ddc-bc10-11ed-9a44-7ef9696e9dd3"
    assert data['metadata']['name'] == "Stream name up to 63 characters long"

    assert data['state_control']['timeout_init_to_start'] == 100
    assert data['state_control']['timeout_start_to_event'] == 500

    assert data['event_alarms'][0]['timeout'] == 254
    assert data['event_alarms'][0]['event_id'] == 122
    assert data['event_alarms'][0]['mode'] == 2
    assert data['event_alarms'][1]['timeout'] == 212
    assert data['event_alarms'][1]['event_id'] == 232
    assert data['event_alarms'][1]['mode'] == "ondemand_from_app"
    assert data['event_alarms'][2]['timeout'] == 8172
    assert data['event_alarms'][2]['event_id'] == 122
    assert data['event_alarms'][2]['mode'] == 0


def test_invalid_metadata_setting():
    obj = cam_csc.CamStreamConfig()
    valid_uuid = "00010203-0405-0607-0809-0a0b0c0d0e0f"
    valid_name = "Stream name up to 63 characters long"

    invalid_type_uuid = (b'\x00\x01\x02\x03\x04\x05\x06\x07'
                         b'\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f')
    invalid_metadata_setting(obj, invalid_type_uuid, valid_name)

    invalid_type_name = 24
    invalid_metadata_setting(obj, valid_uuid, invalid_type_name)

    invalid_range_uuid = "00010203-0405-0607-0809-0a0b0c0d0e"
    invalid_metadata_setting(obj, invalid_range_uuid, valid_name)

    invalid_range_name = ("Stream name up to 63 characters long,"
                          " but it is 67 and overflow now")
    invalid_metadata_setting(obj, valid_uuid, invalid_range_name)


def test_invalid_state_control_setting():
    obj = cam_csc.CamStreamConfig()
    valid_timeout_i2s = 24
    valid_timeout_s2e = 24

    invalid_type_timeout_i2s = "24"
    invalid_state_control_setting(
            obj, invalid_type_timeout_i2s, valid_timeout_s2e)

    invalid_type_timeout_s2e = "24"
    invalid_state_control_setting(
            obj, valid_timeout_i2s, invalid_type_timeout_s2e)

    invalid_range_timeout_i2s = -1
    invalid_state_control_setting(
            obj, invalid_range_timeout_i2s, valid_timeout_s2e)

    invalid_range_timeout_s2e = 2**32
    invalid_state_control_setting(
            obj, valid_timeout_i2s, invalid_range_timeout_s2e)


def test_invalid_event_alarm_adding():
    obj = cam_csc.CamStreamConfig()
    valid_timeout = 24
    valid_event_id = 122
    valid_mode = 0

    invalid_type_event_id = "122"
    invalid_event_alarm_adding(
            obj, valid_timeout, invalid_type_event_id, valid_mode)

    invalid_type_mode = [""]
    invalid_event_alarm_adding(
            obj, valid_timeout, valid_event_id, invalid_type_mode)

    invalid_range_event_id = -1
    invalid_event_alarm_adding(
            obj, valid_timeout, invalid_range_event_id, valid_mode)

    invalid_range_mode = 5
    invalid_event_alarm_adding(
            obj, valid_timeout, valid_event_id, invalid_range_mode)


def test_invalid_file_saving():
    obj = cam_csc.CamStreamConfig()

    name = "Stream name up to 63 characters long"
    metadata = {"name": name}
    obj.data["metadata"] = metadata

    timeout_i2s = 100
    timeout_s2e = 500
    obj.set_state_control(timeout_i2s, timeout_s2e)

    obj.add_event_alarm(254, 122, 2)
    obj.add_event_alarm(212, 232, 1)
    obj.add_event_alarm(8172, 122, 0)

    with pytest.raises(cam_csc.CamStreamConfigException):
        obj.file_save("test_invalid_file_saving.csc")


def test_file_saving():
    obj = cam_csc.CamStreamConfig()

    uuid = "84085ddc-bc10-11ed-9a44-7ef9696e9dd3"
    name = "Stream name up to 63 characters long"
    obj.set_metadata(uuid, name)

    timeout_i2s = 100
    timeout_s2e = 500
    obj.set_state_control(timeout_i2s, timeout_s2e)

    obj.add_event_alarm(254, 122, 2)
    obj.add_event_alarm(212, 232, 1)
    obj.add_event_alarm(8172, 122, 0)

    output = "test_file_saving.csc"
    expect_output = get_path('expected_output.csc')

    obj.file_save(output)
    assert filecmp.cmp(output, expect_output)

    if os.path.exists(output):
        os.remove(output)
